FROM python:3.9-slim

WORKDIR /srv/app/

RUN apt-get update && apt-get install -y locales python3-dev default-libmysqlclient-dev python-dev libmariadb-dev-compat libmariadb-dev build-essential

COPY sud_education_vote/ ./sud_education_vote/
COPY pyproject.toml ./
COPY manage.py ./

RUN python -m pip install uwsgi flit \
  && FLIT_ROOT_INSTALL=1 flit install

ENTRYPOINT [ "uwsgi", "-i", "uwsgi.ini" ]
