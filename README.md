# Application de vote

Ce projet a pour objectif de gérer des scrutins, recueillir des votes et en afficher les résultats

## Développement

### Gestion des dépendences

Les dépendances sont listées dans `pyproject.toml`, dans la section `[project]` dans `dependencies` (si c'est un requirement de production), ou dans la section `[project.optional-dependencies]` dans `test` (si c'est un requirement uniquement pour le developpement ou les tests).


### Installation des dépendances

Lancer ceci (après avoir créé un environnement virtuel python et l'avoir activé) :

```bash
(venv-sud_education_vote) $ pip install flit
(venv-sud_education_vote) $ flit install
```

### Préparer la BDD

Le projet utilise le SGBD MariaDB (ou MySQL). Vous devez donc installer MariaDB, lancer le service, puis créer un utilisateur et une base de données :

```bash
$ mysql -uroot -p
[...]
MariaDB [(none)]> CREATE DATABASE sud_education_vote CHARACTER SET UTF8;
MariaDB [(none)]> CREATE USER sud_education_vote@localhost IDENTIFIED BY 'sud_education_vote';
MariaDB [(none)]> GRANT ALL PRIVILEGES ON sud_education_vote.* TO sud_education_vote@localhost;
```

Notez que la base de données doit bien être en UTF-8, cf la documentation de django : https://docs.djangoproject.com/en/dev/ref/databases/#encoding

Ensuite, lancez les migrations django :

```bash
(venv-sud_education_vote) $ ./manage.py migrate
```

Vous pouvez également créer un superuser si vous le souhaitez : 

```bash
(venv-sud_education_vote) $ ./manage.py createsuperuser
```

### Rassembler les statiques

Cette commande permet de rassembler tous les fichiers statiques dans un même dossier, de manière à le servir efficacement avec nginx ou apache

```bash
(venv-sud_education_vote) $ ./manage.py collectstatic
```

### Lancer le serveur

```bash
(venv-sud_education_vote) $ ./manage.py runserver
# Site accessible sous http://localhost:8000
```

### Lancer les tests

```bash
(venv-sud_education_vote) $ pytest sud_education_vote/
```

### Linter le code

black, isort et flake8 sont utilisés pour linter le code de manière à avoir un code homogène. Pour cela lancez la commande suivante :

```bash
(venv-sud_education_vote) $ ./lintall.sh
```

Si la commande ne donne aucune erreur votre code est propre !


## Variable d'environnement

Variables d'environnement nécessaires au projet en production

- **DEBUG** : Permet l'affichage des erreurs, vaut True par défaut, doit être passé à False en production
- **SECRET_KEY** : la clé secrète de Django, 64 caractères aléatoires est un bon choix
- **ALLOWED_HOSTS** : la liste des domaines autorisés à acceder à l'application, sous la forme : `example.com,www.example.com`
- **DB_NAME** : le nom de la base de données mariadb, si non spécifié `sud_education_vote` est utilisé
- **DB_USER** : le nom de l'utilisateur se connectant à la base de données mariadb, si non spécifié `sud_education_vote` est utilisé
- **DB_PASSWORD** : le mot de passe de l'utilisateur de la base de données mariadb, si non spécifié `sud_education_vote` est utilisé
- **DB_HOST** : le nom de domaine ou l'adresse IP de la base de données mariadb. Peut également être le chemin vers la socket mysql, voir : https://docs.djangoproject.com/en/3.2/ref/settings/#host
- **DB_PORT** : le port de la base de données mariadb, si non spécifié le port par défaut de mariadb est utilisé
- **STATIC_ROOT** : le chemin des fichiers statiques, si non spécifié `./static` est utilisé
- **MEDIA_ROOT** : le chemin des fichiers médias (fichiers uploadés par exemple), si non spécifié `./media` est utilisé
- **EMAIL_BACKEND** : la méthode django pour envoyer des mails, par défaut `django.core.mail.backends.console.EmailBackend` est utilisé ce qui permet de voir les mails envoyés dans la console django. En production il faudra sûrement utiliser `django.core.mail.backends.smtp.EmailBackend`
- **EMAIL_HOST** : le nom de domaine ou l'adresse IP du serveur mail SMTP
- **EMAIL_PORT** : le port du serveur mail SMTP, si non spécifié `587` est utilisé
- **EMAIL_HOST_USER** : l'utilisateur se connectant au serveur mail SMTP, si non spécifié `postmaster@sudeducation-vite.hashbang.fr` est utilisé
- **EMAIL_HOST_PASSWORD** : le mot de passe utilisé pour se connecter au serveur mail SMTP
- **EMAIL_USE_TLS** : `True` si le serveur mail SMTP supporte le TLS, `False` sinon. Le défaut est `True`
- **DJANGO_DEFAULT_FROM_EMAIL** : l'adresse mail envoyant les emails de l'application, si non spécifié `postmaster@sudeducation-vote.hashbang.fr` est utilisé
- **DJANGO_DEFAULT_FROM_EMAIL_NAME** : le nom de la personne envoyant les emails de l'application, si non spécifié `admin` est utilisé

