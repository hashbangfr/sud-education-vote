"""sud_education_vote URL Configuration

The `urlpatterns` list routes URLs to views. For more information please see:
    https://docs.djangoproject.com/en/3.2/topics/http/urls/
Examples:
Function views
    1. Add an import:  from my_app import views
    2. Add a URL to urlpatterns:  path('', views.home, name='home')
Class-based views
    1. Add an import:  from other_app.views import Home
    2. Add a URL to urlpatterns:  path('', Home.as_view(), name='home')
Including another URLconf
    1. Import the include() function: from django.urls import include, path
    2. Add a URL to urlpatterns:  path('blog/', include('blog.urls'))
"""
from django.contrib import admin
from django.contrib.auth.views import (
    LogoutView,
    PasswordChangeDoneView,
    PasswordChangeView,
    PasswordResetCompleteView,
    PasswordResetConfirmView,
    PasswordResetDoneView,
    PasswordResetView,
)
from django.urls import include, path

from .views import AccueilView, ConnexionView

urlpatterns = [
    path("", AccueilView.as_view(), name="accueil"),
    path(
        "connexion/",
        ConnexionView.as_view(),
        name="connexion",
    ),
    path(
        "connexion/password_reset/",
        PasswordResetView.as_view(template_name="reinitialisation_password.html"),
        name="password_reset",
    ),
    path(
        "connexion/password_reset_done/",
        PasswordResetDoneView.as_view(
            template_name="reinitialisation_password_done.html"
        ),
        name="password_reset_done",
    ),
    path(
        "connexion/password_reset/confirm/<uidb64>/<token>/",
        PasswordResetConfirmView.as_view(
            template_name="reinitialisation_password_confirm.html"
        ),
        name="password_reset_confirm",
    ),
    path(
        "connexion/password_reset/done/",
        PasswordResetCompleteView.as_view(
            template_name="reinitialisation_password_complete.html"
        ),
        name="password_reset_complete",
    ),
    path(
        "deconnexion/",
        LogoutView.as_view(template_name="deconnexion.html"),
        name="deconnexion",
    ),
    path("admin/", admin.site.urls),
    path("utilisateurs/", include("sud_education_vote.utilisateur.urls")),
    path("votes/", include("sud_education_vote.vote.urls")),
    path("logs/", include("sud_education_vote.log.urls")),
    path(
        "changement-mot-de-passe/",
        PasswordChangeView.as_view(template_name="changement_pwd.html"),
        name="password_change",
    ),
    path(
        "changement-mot-de-passe-success/",
        PasswordChangeDoneView.as_view(template_name="changement_pwd_succes.html"),
        name="password_change_done",
    ),
]
