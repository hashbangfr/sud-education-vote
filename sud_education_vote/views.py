from django.contrib.auth.mixins import LoginRequiredMixin
from django.contrib.auth.views import LoginView
from django.urls import reverse
from django.utils.http import url_has_allowed_host_and_scheme
from django.views.generic import TemplateView


class AccueilView(LoginRequiredMixin, TemplateView):
    template_name = "accueil.html"


class ConnexionView(LoginView):
    template_name = "connexion.html"
    default_url = ""

    def get_success_url(self):
        if self.request.user.is_superadmin:
            self.default_url = reverse("utilisateur:admins")
        elif self.request.user.is_admin:
            self.default_url = reverse("accueil")
        else:
            self.default_url = reverse("vote:page-vote")
        return super().get_success_url()

    def get_redirect_url(self):
        """Return the user-originating redirect URL if it's safe."""

        redirect_to = self.request.POST.get(
            self.redirect_field_name,
            self.request.GET.get(self.redirect_field_name, self.default_url),
        )
        url_is_safe = url_has_allowed_host_and_scheme(
            url=redirect_to,
            allowed_hosts=self.get_success_url_allowed_hosts(),
            require_https=self.request.is_secure(),
        )
        return redirect_to if url_is_safe else ""
