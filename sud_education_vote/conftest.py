import pytest
from pytest_factoryboy import register

from sud_education_vote.log.factories import LogFactory
from sud_education_vote.utilisateur.factories import (
    UTILISATEUR_PASSWORD,
    UtilisateurFactory,
)
from sud_education_vote.utilisateur.models import Utilisateur
from sud_education_vote.vote.factories import (
    PropositionFactory,
    ScrutinFactory,
    VoteFactory,
    VoteOppositionFactory,
    VotePriorisationFactory,
    VotePropositionFactory,
)

register(LogFactory)
register(UtilisateurFactory)
register(ScrutinFactory)
register(VoteFactory)
register(VoteOppositionFactory)
register(VotePriorisationFactory)
register(VotePropositionFactory)
register(PropositionFactory)


@pytest.fixture
def utilisateur_syndicat(utilisateur_factory):
    return utilisateur_factory(type_utilisateur=Utilisateur.SYNDICAT, nombre_mandat=40)


@pytest.fixture
def utilisateur_admin(utilisateur_factory):
    return utilisateur_factory(type_utilisateur=Utilisateur.ADMIN)


@pytest.fixture
def utilisateur_superadmin(utilisateur_factory):
    return utilisateur_factory(type_utilisateur=Utilisateur.SUPERADMIN)


@pytest.fixture
def client_log(client, utilisateur):
    client.login(email=utilisateur.email, password=UTILISATEUR_PASSWORD)
    return client


@pytest.fixture
def client_syndicat(client, utilisateur_syndicat):
    client.login(email=utilisateur_syndicat.email, password=UTILISATEUR_PASSWORD)
    return client


@pytest.fixture
def client_admin(client, utilisateur_admin):
    client.login(email=utilisateur_admin.email, password=UTILISATEUR_PASSWORD)
    return client


@pytest.fixture
def client_superadmin(client, utilisateur_superadmin):
    client.login(email=utilisateur_superadmin.email, password=UTILISATEUR_PASSWORD)
    return client
