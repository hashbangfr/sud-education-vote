from django.contrib.auth.mixins import UserPassesTestMixin


class SuperAdminRequisMixin(UserPassesTestMixin):
    def test_func(self):
        if self.request.user.is_authenticated:
            return self.request.user.is_superadmin
        return False


class AdminRequisMixin(UserPassesTestMixin):
    def test_func(self):
        if self.request.user.is_authenticated:
            return self.request.user.is_admin
        return False


class SyndicatRequisMixin(UserPassesTestMixin):
    def test_func(self):
        if self.request.user.is_authenticated:
            return self.request.user.is_syndicat
        return False


class SyndicatOuAdminRequisMixin(UserPassesTestMixin):
    def test_func(self):
        if self.request.user.is_authenticated:
            return self.request.user.is_syndicat or self.request.user.is_admin
        return False
