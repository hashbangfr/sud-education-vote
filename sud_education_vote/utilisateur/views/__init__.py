from .gestion_admin_views import (
    AdminCreationView,
    AdminListView,
    AdminSupressionView,
    AdminUpdateView,
)
from .gestion_syndicat_views import (
    ImportMandatsView,
    SyndicatCreateView,
    SyndicatListView,
    SyndicatSuppressionView,
    SyndicatUpdateView,
)

__all__ = [
    "AdminListView",
    "AdminCreationView",
    "AdminSupressionView",
    "AdminUpdateView",
    "SyndicatListView",
    "SyndicatCreateView",
    "SyndicatUpdateView",
    "SyndicatSuppressionView",
    "ImportMandatsView",
]
