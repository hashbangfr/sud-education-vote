import codecs
import csv

from django.contrib import messages
from django.http import HttpResponseRedirect
from django.shortcuts import redirect, render
from django.urls import reverse
from django.views.generic import CreateView, DeleteView, ListView, UpdateView, View

from sud_education_vote.log.models import Log
from sud_education_vote.mixins import AdminRequisMixin

from ..forms import FichierImportMandatsForm, SyndicatForm
from ..models import Utilisateur


class SyndicatListView(AdminRequisMixin, ListView):
    template_name = "utilisateur/liste_syndicats.html"
    model = Utilisateur

    def get_queryset(self):
        queryset = super().get_queryset()
        queryset = queryset.filter(type_utilisateur=Utilisateur.SYNDICAT)
        return queryset


class SyndicatCreateView(AdminRequisMixin, CreateView):
    model = Utilisateur
    template_name = "utilisateur/creation_syndicat.html"
    form_class = SyndicatForm

    def get_form_kwargs(self):
        kwargs = super().get_form_kwargs()
        kwargs.update({"update": False, "user": None})
        return kwargs

    def get_success_url(self):
        return reverse("utilisateur:syndicats")

    def form_valid(self, form):
        self.object = form.save()
        self.object.type_utilisateur = Utilisateur.SYNDICAT
        self.object.save()
        Log.objects.create(
            log=f"L'admin {self.request.user} a créé le syndicat {self.object}"
        )
        return HttpResponseRedirect(self.get_success_url())


class SyndicatUpdateView(AdminRequisMixin, UpdateView):
    model = Utilisateur
    template_name = "utilisateur/edition_syndicat.html"
    form_class = SyndicatForm

    def get_form_kwargs(self):
        kwargs = super().get_form_kwargs()
        kwargs.update({"update": True, "user": self.get_object()})
        return kwargs

    def get_queryset(self):
        return super().get_queryset().filter(type_utilisateur=Utilisateur.SYNDICAT)

    def get_success_url(self):
        return reverse("utilisateur:syndicats")

    def form_valid(self, form):
        previous_instance = Utilisateur.objects.get(pk=self.object.pk)
        self.object = form.save()
        if previous_instance.nombre_mandat != self.object.nombre_mandat:
            Log.objects.create(
                log=(
                    f"L'admin {self.request.user} a modifié "
                    f"le nombre de mandats du syndicat {self.object} "
                    f"de {previous_instance.nombre_mandat} à "
                    f"{self.object.nombre_mandat}"
                )
            )
        if previous_instance.email != self.object.email:
            Log.objects.create(
                log=(
                    f"L'admin {self.request.user} a modifié "
                    f"le mail du syndicat {self.object} "
                    f"de {previous_instance.email} à {self.object.email}"
                )
            )
        return HttpResponseRedirect(self.get_success_url())


class SyndicatSuppressionView(AdminRequisMixin, DeleteView):
    model = Utilisateur
    template_name = "utilisateur/suppression_syndicat.html"

    """
    On s'assure que l'utilisateur choisi est bien un syndicat
    """

    def get_queryset(self):
        return super().get_queryset().filter(type_utilisateur=Utilisateur.SYNDICAT)

    def delete(self, request, *args, **kwargs):
        Log.objects.create(
            log=f"L'admin {self.request.user} a "
            f"supprimé le syndicat {self.get_object()}"
        )
        return super().delete(request, *args, **kwargs)

    def get_success_url(self):
        return reverse("utilisateur:syndicats")


class ImportMandatsView(AdminRequisMixin, View):
    form_class = FichierImportMandatsForm
    template_name = "utilisateur/import_mandats.html"

    def get(self, request, *args, **kwargs):
        form = self.form_class()
        return render(request, self.template_name, {"form": form})

    def post(self, request, *args, **kwargs):
        form = self.form_class(request.POST, request.FILES)
        fichier = request.FILES["fichier"]
        if fichier.content_type == "text/csv":
            reader = csv.reader((codecs.iterdecode(fichier, "utf-8")))
            syndicats_erreur = []
            error = False
            for row in reader:
                if row[0] != "Syndicats":
                    syndicat = Utilisateur.objects.filter(
                        type_utilisateur=Utilisateur.SYNDICAT, username=row[0]
                    ).first()
                    if not syndicat or not row[1].isnumeric():
                        error = True
                        syndicats_erreur.append(row[0])
                    else:
                        syndicat.nombre_mandat = row[1]
                        syndicat.save()
            if error:
                messages.add_message(
                    self.request,
                    messages.WARNING,
                    (
                        f"Les lignes suivantes n'ont pas pu être ajoutées : "
                        f"{', '.join(syndicats_erreur)}. Vérifiez l'orthographe du nom"
                        f" du syndicat ou le nombre de mandats."
                    ),
                )
            return redirect(reverse("utilisateur:syndicats"))
        else:
            messages.add_message(
                self.request,
                messages.ERROR,
                ("Le fichier téléversé n'est pas au bon format"),
            )
            return render(request, self.template_name, {"form": form})
