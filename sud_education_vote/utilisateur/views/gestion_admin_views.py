from django.http import HttpResponseRedirect
from django.urls import reverse
from django.views.generic import CreateView, DeleteView, ListView, UpdateView

from sud_education_vote.log.models import Log
from sud_education_vote.mixins import SuperAdminRequisMixin

from ..forms import AdminCreationForm, AdminUpdateForm
from ..models import Utilisateur


class AdminListView(SuperAdminRequisMixin, ListView):
    template_name = "utilisateur/liste_admins.html"
    model = Utilisateur

    def get_queryset(self):
        queryset = super().get_queryset()
        queryset = queryset.filter(type_utilisateur=Utilisateur.ADMIN)
        return queryset


class AdminCreationView(SuperAdminRequisMixin, CreateView):
    model = Utilisateur
    form_class = AdminCreationForm
    template_name = "utilisateur/creation_admin.html"

    def form_valid(self, form):
        self.object = form.save()
        raw_password = self.object.password
        self.object.set_password(raw_password)
        self.object.type_utilisateur = Utilisateur.ADMIN
        self.object.save()
        Log.objects.create(log=f"Le super admin a créé l'admin {self.object}")
        return HttpResponseRedirect(self.get_success_url())

    def get_success_url(self):
        return reverse("utilisateur:admins")


class AdminUpdateView(SuperAdminRequisMixin, UpdateView):
    model = Utilisateur
    form_class = AdminUpdateForm
    template_name = "utilisateur/edition_admin.html"

    """
    On s'assure que l'utilisateur choisi est bien un admin
    """

    def get_queryset(self):
        return super().get_queryset().filter(type_utilisateur=Utilisateur.ADMIN)

    def get_form_kwargs(self):
        kwargs = super().get_form_kwargs()
        kwargs.update({"user": self.get_object(), "update": True})
        return kwargs

    def get_success_url(self):
        return reverse("utilisateur:admins")


class AdminSupressionView(SuperAdminRequisMixin, DeleteView):
    model = Utilisateur
    template_name = "utilisateur/supression_admin.html"

    """
    On s'assure que l'utilisateur choisi est bien un admin
    """

    def get_queryset(self):
        return super().get_queryset().filter(type_utilisateur=Utilisateur.ADMIN)

    def delete(self, request, *args, **kwargs):
        Log.objects.create(log=f"Le super admin a supprimé l'admin {self.get_object()}")
        return super().delete(request, *args, **kwargs)

    def get_success_url(self):
        return reverse("utilisateur:admins")
