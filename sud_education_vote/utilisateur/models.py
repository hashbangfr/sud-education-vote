from django.contrib.auth.base_user import BaseUserManager
from django.contrib.auth.models import AbstractUser
from django.db import models
from django.utils.translation import gettext_lazy as _


class UtilisateurManager(BaseUserManager):
    """
    Manager d'utilisateur pour l'utilisateur surchargé qui utilisera
    le mail à la place de l'username pour l'authentification
    """

    def create_user(self, email, password, **extra_fields):
        """
        Créer et sauvegarder un utilisateur a partir d'un mail et d'un mot de passe
        """
        if not email:
            raise ValueError(_("L'email doit être saisi"))
        email = self.normalize_email(email)
        user = self.model(email=email, **extra_fields)
        user.set_password(password)
        user.save()
        return user

    def create_superuser(self, email, password, **extra_fields):
        """
        Créer un utilisateur superadmin avec un mail et un mot de passe
        """
        extra_fields.setdefault("is_staff", True)
        extra_fields.setdefault("is_superuser", True)
        extra_fields.setdefault("is_active", True)
        extra_fields.setdefault("type_utilisateur", "SUPERADMIN")

        if extra_fields.get("is_staff") is not True:
            raise ValueError(_("Le superadmin doit avoir is_staff=True."))
        if extra_fields.get("is_superuser") is not True:
            raise ValueError(_("Le superadmin doit avoir is_superuser=True."))
        return self.create_user(email, password, **extra_fields)


class Utilisateur(AbstractUser):
    password = models.CharField(_("password"), max_length=128, blank=True, null=True)
    username = models.CharField(
        _("Nom"), max_length=150, null=True, blank=True, unique=True
    )
    email = models.EmailField(_("Adresse mail"), unique=True)

    SUPERADMIN = "SUPERADMIN"
    ADMIN = "ADMIN"
    SYNDICAT = "SYNDICAT"
    TYPES = [
        (SUPERADMIN, _("Superadmin")),
        (ADMIN, _("Admin")),
        (SYNDICAT, _("Syndicat")),
    ]
    type_utilisateur = models.CharField(
        choices=TYPES, max_length=max([len(e[0]) for e in TYPES]), default=SYNDICAT
    )

    nombre_mandat = models.PositiveIntegerField(default=0)

    USERNAME_FIELD = "email"
    REQUIRED_FIELDS = []

    objects = UtilisateurManager()

    def __str__(self):
        if self.username:
            return self.username
        return self.email

    @property
    def is_syndicat(self):
        return self.type_utilisateur == Utilisateur.SYNDICAT

    @property
    def is_admin(self):
        return self.type_utilisateur == Utilisateur.ADMIN

    @property
    def is_superadmin(self):
        return self.type_utilisateur == Utilisateur.SUPERADMIN
