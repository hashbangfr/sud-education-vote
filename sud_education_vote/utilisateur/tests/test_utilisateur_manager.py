import pytest

from sud_education_vote.utilisateur.models import Utilisateur

pytestmark = pytest.mark.django_db


def test_instanciate():
    user1 = Utilisateur.objects.create(email="test1@example.com")
    user2 = Utilisateur.objects.create(email="test2@example.com", username="Test")
    assert f"{user1}" == "test1@example.com"
    assert f"{user2}" == "Test"


def test_management_create_superuser():
    assert Utilisateur.objects.count() == 0
    Utilisateur.objects.create_superuser(
        email="test@test.com",
        password="password",
        first_name="testi",
        last_name="test",
    )
    admin = Utilisateur.objects.first()
    assert admin.is_superuser
    assert admin.type_utilisateur == Utilisateur.SUPERADMIN


def test_management_create_superuser_wrong_is_staff():
    assert Utilisateur.objects.count() == 0
    with pytest.raises(ValueError) as e:
        Utilisateur.objects.create_superuser(
            email="test@test.com",
            password="password",
            first_name="testi",
            last_name="test",
            is_staff=False,
        )
    assert str(e.value) == "Le superadmin doit avoir is_staff=True."


def test_management_create_superuser_wrong_is_superuser():
    assert Utilisateur.objects.count() == 0
    with pytest.raises(ValueError) as e:
        Utilisateur.objects.create_superuser(
            email="test@test.com",
            password="password",
            first_name="testi",
            last_name="test",
            is_superuser=False,
        )
    assert str(e.value) == "Le superadmin doit avoir is_superuser=True."


def test_management_create_user_wrong_mail():
    assert Utilisateur.objects.count() == 0
    with pytest.raises(ValueError) as e:
        Utilisateur.objects.create_user(
            email=False,
            password="password",
            first_name="testi",
            last_name="test",
        )
    assert str(e.value) == "L'email doit être saisi"
