from os.path import abspath, dirname, join

import pytest
from django.core.files import File
from django.core.files.uploadedfile import SimpleUploadedFile
from django.urls import reverse

from sud_education_vote.utilisateur.models import Utilisateur

pytestmark = pytest.mark.django_db

FILES_DIR = join(dirname(abspath(__file__)), "files")


def test_import_page_redirection_si_non_connecte(client):
    url = reverse("utilisateur:import-mandats")
    response = client.get(url)
    assert response.status_code == 302
    assert response.url == f"{reverse('connexion')}?next={url}"


def test_import_page_pas_de_permission_syndicat(client_syndicat):
    response = client_syndicat.get(reverse("utilisateur:import-mandats"))
    assert response.status_code == 403


def test_import_page_get_permission_admin(client_admin):
    response = client_admin.get(reverse("utilisateur:import-mandats"))
    assert response.status_code == 200


def test_import_page_post_ok(client_admin, utilisateur_factory):
    syndicat = utilisateur_factory(
        username="Ain (01)", nombre_mandat=0, type_utilisateur=Utilisateur.SYNDICAT
    )
    syndicat2 = utilisateur_factory(
        username="Rhone (69)", nombre_mandat=0, type_utilisateur=Utilisateur.SYNDICAT
    )
    csv_file = File(open(join(FILES_DIR, "test_import.csv"), "rb"))
    upload_csv = SimpleUploadedFile(
        "test_import.csv", csv_file.read(), content_type="text/csv"
    )
    data = {"fichier": [upload_csv]}
    response = client_admin.post(
        reverse("utilisateur:import-mandats"), data, format="multipart"
    )
    assert response.status_code == 302
    syndicat.refresh_from_db()
    syndicat2.refresh_from_db()
    assert syndicat.nombre_mandat == 100
    assert syndicat2.nombre_mandat == 0


def test_import_page_post_not_right_file(client_admin, utilisateur_factory):
    syndicat = utilisateur_factory(
        username="Ain (01)", nombre_mandat=0, type_utilisateur=Utilisateur.SYNDICAT
    )
    syndicat2 = utilisateur_factory(
        username="Rhone (69)", nombre_mandat=0, type_utilisateur=Utilisateur.SYNDICAT
    )
    csv_file = File(open(join(FILES_DIR, "test_import.csv"), "rb"))
    upload_csv = SimpleUploadedFile(
        "test_import.csv", csv_file.read(), content_type="text"
    )
    data = {"fichier": [upload_csv]}
    response = client_admin.post(
        reverse("utilisateur:import-mandats"), data, format="multipart"
    )
    assert response.status_code == 200
    syndicat.refresh_from_db()
    syndicat2.refresh_from_db()
    assert syndicat.nombre_mandat == 0
    assert syndicat2.nombre_mandat == 0
