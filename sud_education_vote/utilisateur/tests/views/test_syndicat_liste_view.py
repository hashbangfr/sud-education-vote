import pytest
from django.urls import reverse
from pytest_django.asserts import assertQuerysetEqual

pytestmark = pytest.mark.django_db


def test_liste_syndicats_redirection_si_non_connecte(client):
    url_syndicats = reverse("utilisateur:syndicats")
    response = client.get(url_syndicats)
    assert response.status_code == 302
    assert response.url == f"{reverse('connexion')}?next={url_syndicats}"


def test_liste_syndicats_pas_de_permission_syndicat(client_syndicat):
    response = client_syndicat.get(reverse("utilisateur:syndicats"))
    assert response.status_code == 403


def test_liste_syndicats_pas_de_permission_superadmin(client_superadmin):
    response = client_superadmin.get(reverse("utilisateur:syndicats"))
    assert response.status_code == 403


def test_liste_syndicats_permission_admin(
    client_admin, utilisateur_syndicat, utilisateur_admin
):
    response = client_admin.get(reverse("utilisateur:syndicats"))
    assert response.status_code == 200
    assertQuerysetEqual(
        response.context.get("object_list"), [utilisateur_syndicat], ordered=False
    )


def test_liste_syndicats_ok_pour_admin_pas_de_syndicats(
    client_admin, utilisateur_admin
):
    response = client_admin.get(reverse("utilisateur:syndicats"))
    assert response.status_code == 200
    assert not response.context.get("object_list")
