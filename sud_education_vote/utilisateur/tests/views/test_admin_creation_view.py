import pytest
from django.contrib.auth import authenticate
from django.urls import reverse
from django.utils.translation import gettext_lazy as _

from sud_education_vote.log.models import Log
from sud_education_vote.utilisateur.models import Utilisateur

pytestmark = pytest.mark.django_db


def test_creation_admin_redirection_si_non_connecte(client):
    url_admin_creation = reverse("utilisateur:admin-creation")
    response = client.get(url_admin_creation)
    assert response.status_code == 302
    assert response.url == f"{reverse('connexion')}?next={url_admin_creation}"


def test_creation_admin_pas_de_permission_syndicat(client_syndicat):
    response = client_syndicat.get(reverse("utilisateur:admin-creation"))
    assert response.status_code == 403


def test_creation_admin_pas_de_permission_admin(client_admin):
    response = client_admin.get(reverse("utilisateur:admin-creation"))
    assert response.status_code == 403


def test_creation_admin_ok_pour_superadmin(client_superadmin):
    response = client_superadmin.get(reverse("utilisateur:admin-creation"))
    assert response.status_code == 200


def test_creation_admin_succes(client_superadmin):
    assert Utilisateur.objects.filter(type_utilisateur=Utilisateur.ADMIN).count() == 0
    response = client_superadmin.post(
        reverse("utilisateur:admin-creation"),
        data={
            "username": "Username",
            "email": "email_admin@test.com",
            "password": "password",
            "confirm_password": "password",
        },
    )
    assert response.status_code == 302
    assert response.url == reverse("utilisateur:admins")
    assert Utilisateur.objects.filter(type_utilisateur=Utilisateur.ADMIN).count() == 1
    admin = Utilisateur.objects.filter(type_utilisateur=Utilisateur.ADMIN).last()
    assert admin.username == "Username"
    assert admin.email == "email_admin@test.com"
    assert Log.objects.last().log == "Le super admin a créé l'admin Username"
    assert authenticate(username=admin.email, password="password") == admin


def test_creation_admin_echec_username_deja_pris(
    client_superadmin, utilisateur_syndicat
):
    assert Utilisateur.objects.filter(type_utilisateur=Utilisateur.ADMIN).count() == 0
    response = client_superadmin.post(
        reverse("utilisateur:admin-creation"),
        data={
            "username": utilisateur_syndicat.username,
            "email": "email_admin@test.com",
            "password": "password",
            "confirm_password": "password",
        },
    )
    assert response.status_code == 200
    assert Utilisateur.objects.filter(type_utilisateur=Utilisateur.ADMIN).count() == 0
    assert response.context.get("form").errors == {
        "username": [_("Il y a déjà un utilisateur avec ce nom d'utilisateur")]
    }


def test_creation_admin_echec_email_deja_pris(client_superadmin, utilisateur_syndicat):
    assert Utilisateur.objects.filter(type_utilisateur=Utilisateur.ADMIN).count() == 0
    response = client_superadmin.post(
        reverse("utilisateur:admin-creation"),
        data={
            "username": "username_admin",
            "email": utilisateur_syndicat.email,
            "password": "password",
            "confirm_password": "password",
        },
    )
    assert response.status_code == 200
    assert Utilisateur.objects.filter(type_utilisateur=Utilisateur.ADMIN).count() == 0
    assert response.context.get("form").errors == {
        "email": [_("Il y a déjà un utilisateur avec cet email")]
    }


def test_creation_admin_echec_mots_de_passe_differents(client_superadmin):
    assert Utilisateur.objects.filter(type_utilisateur=Utilisateur.ADMIN).count() == 0
    response = client_superadmin.post(
        reverse("utilisateur:admin-creation"),
        data={
            "username": "username_admin",
            "email": "email_admin@test.com",
            "password": "password",
            "confirm_password": "password2",
        },
    )
    assert response.status_code == 200
    assert Utilisateur.objects.filter(type_utilisateur=Utilisateur.ADMIN).count() == 0
    assert response.context.get("form").errors == {
        "confirm_password": [_("Les mots de passe doivent être identiques")]
    }
