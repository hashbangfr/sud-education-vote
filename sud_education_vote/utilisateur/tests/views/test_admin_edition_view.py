import pytest
from django.urls import reverse

from sud_education_vote.utilisateur.forms import AdminUpdateForm
from sud_education_vote.utilisateur.models import Utilisateur

pytestmark = pytest.mark.django_db


def test_edition_admin_redirection_si_non_connecte(client, utilisateur_admin):
    url_admin_edition = reverse(
        "utilisateur:admin-edition", kwargs={"pk": utilisateur_admin.pk}
    )
    response = client.get(url_admin_edition)
    assert response.status_code == 302
    assert response.url == f"{reverse('connexion')}?next={url_admin_edition}"


def test_edition_admin_pas_de_permission_syndicat(client_syndicat, utilisateur_admin):
    response = client_syndicat.get(
        reverse("utilisateur:admin-edition", kwargs={"pk": utilisateur_admin.pk})
    )
    assert response.status_code == 403


def test_edition_admin_pas_de_permission_admin(client_admin, utilisateur_admin):
    response = client_admin.get(
        reverse("utilisateur:admin-edition", kwargs={"pk": utilisateur_admin.pk})
    )
    assert response.status_code == 403


def test_edition_admin_ok_pour_superadmin(client_superadmin, utilisateur_admin):
    response = client_superadmin.get(
        reverse("utilisateur:admin-edition", kwargs={"pk": utilisateur_admin.pk})
    )
    assert response.status_code == 200
    assert isinstance(response.context["form"], AdminUpdateForm)


def test_edition_admin_pas_de_admin(client_superadmin, utilisateur_admin):
    response = client_superadmin.get(
        reverse("utilisateur:admin-edition", kwargs={"pk": utilisateur_admin.pk + 1})
    )
    assert response.status_code == 404


def test_edition_admin_ok(client_superadmin, utilisateur_admin, utilisateur_factory):
    admin = utilisateur_factory(type_utilisateur=Utilisateur.ADMIN)
    response = client_superadmin.post(
        reverse("utilisateur:admin-edition", kwargs={"pk": admin.pk}),
        data={
            "username": "Username",
            "email": admin.email,
        },
    )
    assert response.status_code == 302
    assert response.url == reverse("utilisateur:admins")
