import pytest
from django.urls import reverse
from pytest_django.asserts import assertQuerysetEqual

pytestmark = pytest.mark.django_db


def test_liste_admin_redirection_si_non_connecte(client):
    url_admins = reverse("utilisateur:admins")
    response = client.get(url_admins)
    assert response.status_code == 302
    assert response.url == f"{reverse('connexion')}?next={url_admins}"


def test_liste_admin_pas_de_permission_syndicat(client_syndicat):
    response = client_syndicat.get(reverse("utilisateur:admins"))
    assert response.status_code == 403


def test_liste_admin_pas_de_permission_admin(client_admin):
    response = client_admin.get(reverse("utilisateur:admins"))
    assert response.status_code == 403


def test_liste_admin_ok_pour_superadmin(
    client_superadmin, utilisateur_syndicat, utilisateur_admin
):
    response = client_superadmin.get(reverse("utilisateur:admins"))
    assert response.status_code == 200
    assertQuerysetEqual(
        response.context.get("object_list"), [utilisateur_admin], ordered=False
    )


def test_liste_admin_ok_pour_superadmin_pas_d_admin(
    client_superadmin, utilisateur_syndicat
):
    response = client_superadmin.get(reverse("utilisateur:admins"))
    assert response.status_code == 200
    assert not response.context.get("object_list")
