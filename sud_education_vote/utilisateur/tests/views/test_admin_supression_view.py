import pytest
from django.urls import reverse

from sud_education_vote.log.models import Log
from sud_education_vote.utilisateur.models import Utilisateur

pytestmark = pytest.mark.django_db


def test_supression_admin_redirection_si_non_connecte(client, utilisateur_admin):
    url_admin_supression = reverse(
        "utilisateur:admin-supression", kwargs={"pk": utilisateur_admin.pk}
    )
    response = client.get(url_admin_supression)
    assert response.status_code == 302
    assert response.url == f"{reverse('connexion')}?next={url_admin_supression}"


def test_supression_admin_pas_de_permission_syndicat(
    client_syndicat, utilisateur_admin
):
    response = client_syndicat.get(
        reverse("utilisateur:admin-supression", kwargs={"pk": utilisateur_admin.pk})
    )
    assert response.status_code == 403


def test_supression_admin_pas_de_permission_admin(client_admin, utilisateur_admin):
    response = client_admin.get(
        reverse("utilisateur:admin-supression", kwargs={"pk": utilisateur_admin.pk})
    )
    assert response.status_code == 403


def test_supression_admin_ok_pour_superadmin(client_superadmin, utilisateur_admin):
    response = client_superadmin.get(
        reverse("utilisateur:admin-supression", kwargs={"pk": utilisateur_admin.pk})
    )
    assert response.status_code == 200


def test_supression_admin_succes(client_superadmin, utilisateur_admin):
    response = client_superadmin.post(
        reverse("utilisateur:admin-supression", kwargs={"pk": utilisateur_admin.pk})
    )
    assert response.status_code == 302
    assert response.url == reverse("utilisateur:admins")
    assert not Utilisateur.objects.filter(username=utilisateur_admin).exists()
    assert (
        Log.objects.last().log
        == f"Le super admin a supprimé l'admin {utilisateur_admin.username}"
    )


def test_supression_admin_si_non_admin(client_superadmin, utilisateur_syndicat):
    response = client_superadmin.post(
        reverse("utilisateur:admin-supression", kwargs={"pk": utilisateur_syndicat.pk})
    )
    assert response.status_code == 404
    assert Utilisateur.objects.filter(username=utilisateur_syndicat).exists()
