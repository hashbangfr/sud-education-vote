import pytest
from django.urls import reverse

from sud_education_vote.log.models import Log
from sud_education_vote.utilisateur.models import Utilisateur

pytestmark = pytest.mark.django_db


def test_suppression_syndicat_redirection_si_non_connecte(client, utilisateur_syndicat):
    url_syndicat_suppression = reverse(
        "utilisateur:syndicat-suppression", kwargs={"pk": utilisateur_syndicat.pk}
    )
    response = client.get(url_syndicat_suppression)
    assert response.status_code == 302
    assert response.url == f"{reverse('connexion')}?next={url_syndicat_suppression}"


def test_suppression_syndicat_redirection_si_non_autorise(
    client_syndicat, utilisateur_syndicat
):
    response = client_syndicat.get(
        reverse(
            "utilisateur:syndicat-suppression", kwargs={"pk": utilisateur_syndicat.pk}
        )
    )
    assert response.status_code == 403


def test_suppression_syndicat_succes(
    client_admin, utilisateur_admin, utilisateur_syndicat
):
    response = client_admin.post(
        reverse(
            "utilisateur:syndicat-suppression", kwargs={"pk": utilisateur_syndicat.pk}
        )
    )
    assert response.status_code == 302
    assert response.url == reverse("utilisateur:syndicats")
    assert not Utilisateur.objects.filter(username=utilisateur_syndicat).exists()
    assert (
        Log.objects.last().log == f"L'admin {utilisateur_admin} a supprimé"
        f" le syndicat {utilisateur_syndicat.username}"
    )
