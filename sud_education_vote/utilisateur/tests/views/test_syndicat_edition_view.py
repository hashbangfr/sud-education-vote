import pytest
from django.urls import reverse

from sud_education_vote.log.models import Log
from sud_education_vote.utilisateur.forms import SyndicatForm
from sud_education_vote.utilisateur.models import Utilisateur

pytestmark = pytest.mark.django_db


def test_edition_syndicat_redirection_si_non_connecte(client, utilisateur_syndicat):
    url_admin_edition = reverse(
        "utilisateur:syndicat-edition", kwargs={"pk": utilisateur_syndicat.pk}
    )
    response = client.get(url_admin_edition)
    assert response.status_code == 302
    assert response.url == f"{reverse('connexion')}?next={url_admin_edition}"


def test_edition_syndicat_pas_de_permission_syndicat(
    client_syndicat, utilisateur_syndicat
):
    response = client_syndicat.get(
        reverse("utilisateur:syndicat-edition", kwargs={"pk": utilisateur_syndicat.pk})
    )
    assert response.status_code == 403


def test_edition_syndicat_pas_de_permission_superadmin(
    client_superadmin, utilisateur_syndicat
):
    response = client_superadmin.get(
        reverse("utilisateur:syndicat-edition", kwargs={"pk": utilisateur_syndicat.pk})
    )
    assert response.status_code == 403


def test_edition_syndicat_ok_pour_admin(client_admin, utilisateur_syndicat):
    response = client_admin.get(
        reverse("utilisateur:syndicat-edition", kwargs={"pk": utilisateur_syndicat.pk})
    )
    assert response.status_code == 200
    assert isinstance(response.context["form"], SyndicatForm)


def test_edition_syndicat_pas_de_syndicat(client_admin, utilisateur_admin):
    response = client_admin.get(
        reverse("utilisateur:syndicat-edition", kwargs={"pk": utilisateur_admin.pk})
    )
    assert response.status_code == 404


def test_edition_syndicat_nombre_mandat(
    client_admin, utilisateur_admin, utilisateur_factory
):
    syndicat = utilisateur_factory(
        nombre_mandat=10, type_utilisateur=Utilisateur.SYNDICAT
    )
    response = client_admin.post(
        reverse("utilisateur:syndicat-edition", kwargs={"pk": syndicat.pk}),
        data={
            "email": syndicat.email,
            "nombre_mandat": 15,
        },
    )
    assert response.status_code == 302
    assert response.url == reverse("utilisateur:syndicats")
    syndicat.refresh_from_db()
    assert syndicat.nombre_mandat == 15
    assert Log.objects.last().log == (
        f"L'admin {utilisateur_admin} a modifié "
        f"le nombre de mandats du syndicat {syndicat} "
        f"de {10} à {syndicat.nombre_mandat}"
    )


def test_edition_syndicat_email(client_admin, utilisateur_admin, utilisateur_factory):
    syndicat = utilisateur_factory(
        nombre_mandat=10, type_utilisateur=Utilisateur.SYNDICAT
    )
    response = client_admin.post(
        reverse("utilisateur:syndicat-edition", kwargs={"pk": syndicat.pk}),
        data={
            "email": "newmail@test.com",
            "nombre_mandat": syndicat.nombre_mandat,
        },
    )
    assert response.status_code == 302
    assert response.url == reverse("utilisateur:syndicats")
    previous_mail = syndicat.email
    syndicat.refresh_from_db()
    assert syndicat.nombre_mandat == 10
    assert syndicat.email == "newmail@test.com"
    assert Log.objects.last().log == (
        f"L'admin {utilisateur_admin} a modifié "
        f"le mail du syndicat {syndicat} "
        f"de {previous_mail} à {syndicat.email}"
    )


def test_edition_syndicat_email_manquant(
    client_admin, utilisateur_admin, utilisateur_factory
):
    syndicat = utilisateur_factory(
        nombre_mandat=10, type_utilisateur=Utilisateur.SYNDICAT
    )
    response = client_admin.post(
        reverse("utilisateur:syndicat-edition", kwargs={"pk": syndicat.pk}),
        data={
            "nombre_mandat": syndicat.nombre_mandat,
        },
    )
    assert response.status_code == 200
    assert response.context.get("form").errors == {
        "email": ["Ce champ est obligatoire."]
    }
