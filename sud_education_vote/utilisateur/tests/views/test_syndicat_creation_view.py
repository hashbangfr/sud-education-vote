import pytest
from django.contrib.auth import authenticate
from django.urls import reverse
from django.utils.translation import gettext_lazy as _

from sud_education_vote.log.models import Log
from sud_education_vote.utilisateur.forms import SyndicatForm
from sud_education_vote.utilisateur.models import Utilisateur

pytestmark = pytest.mark.django_db


def test_creation_syndicat_redirection_si_non_connecte(client):
    url_admin_creation = reverse("utilisateur:syndicat-creation")
    response = client.get(url_admin_creation)
    assert response.status_code == 302
    assert response.url == f"{reverse('connexion')}?next={url_admin_creation}"


def test_creation_syndicat_pas_de_permission_syndicat(client_syndicat):
    response = client_syndicat.get(reverse("utilisateur:syndicat-creation"))
    assert response.status_code == 403


def test_creation_syndicat_pas_de_permission_superadmin(client_superadmin):
    response = client_superadmin.get(reverse("utilisateur:syndicat-creation"))
    assert response.status_code == 403


def test_creation_admin_ok_pour_admin(client_admin):
    response = client_admin.get(reverse("utilisateur:syndicat-creation"))
    assert response.status_code == 200
    assert isinstance(response.context["form"], SyndicatForm)


def test_creation_syndicat_succes(client_admin, utilisateur_admin):
    assert (
        Utilisateur.objects.filter(type_utilisateur=Utilisateur.SYNDICAT).count() == 0
    )
    response = client_admin.post(
        reverse("utilisateur:syndicat-creation"),
        data={
            "username": "Username",
            "email": "email_syndicat@test.com",
            "password": "password",
            "confirm_password": "password",
            "nombre_mandat": 10,
        },
    )
    assert response.status_code == 302
    assert response.url == reverse("utilisateur:syndicats")
    assert (
        Utilisateur.objects.filter(type_utilisateur=Utilisateur.SYNDICAT).count() == 1
    )
    syndicat = Utilisateur.objects.filter(type_utilisateur=Utilisateur.SYNDICAT).last()
    assert syndicat.username == "Username"
    assert syndicat.email == "email_syndicat@test.com"
    assert Log.objects.last().log == (
        f"L'admin {utilisateur_admin} a créé le syndicat {syndicat}"
    )
    assert authenticate(username=syndicat.email, password="password") == syndicat


def test_creation_syndicat_echec_username_deja_pris(client_admin, utilisateur_admin):
    assert (
        Utilisateur.objects.filter(type_utilisateur=Utilisateur.SYNDICAT).count() == 0
    )
    response = client_admin.post(
        reverse("utilisateur:syndicat-creation"),
        data={
            "username": utilisateur_admin.username,
            "email": "email_admin@test.com",
            "password": "password",
            "confirm_password": "password",
            "nombre_mandat": 10,
        },
    )
    assert response.status_code == 200
    assert (
        Utilisateur.objects.filter(type_utilisateur=Utilisateur.SYNDICAT).count() == 0
    )
    assert response.context.get("form").errors == {
        "username": [_("Il y a déjà un utilisateur avec ce nom d'utilisateur")]
    }


def test_creation_syndicat_echec_email_deja_pris(client_admin, utilisateur_admin):
    assert (
        Utilisateur.objects.filter(type_utilisateur=Utilisateur.SYNDICAT).count() == 0
    )
    response = client_admin.post(
        reverse("utilisateur:syndicat-creation"),
        data={
            "username": "username_admin",
            "email": utilisateur_admin.email,
            "password": "password",
            "confirm_password": "password",
            "nombre_mandat": 10,
        },
    )
    assert response.status_code == 200
    assert (
        Utilisateur.objects.filter(type_utilisateur=Utilisateur.SYNDICAT).count() == 0
    )
    assert response.context.get("form").errors == {
        "email": [_("Il y a déjà un utilisateur avec cet email")]
    }


def test_creation_syndicat_echec_mots_de_passe_differents(client_admin):
    assert (
        Utilisateur.objects.filter(type_utilisateur=Utilisateur.SYNDICAT).count() == 0
    )
    response = client_admin.post(
        reverse("utilisateur:syndicat-creation"),
        data={
            "username": "username_admin",
            "email": "email_admin@test.com",
            "password": "password",
            "confirm_password": "password2",
            "nombre_mandat": 10,
        },
    )
    assert response.status_code == 200
    assert (
        Utilisateur.objects.filter(type_utilisateur=Utilisateur.SYNDICAT).count() == 0
    )
    assert response.context.get("form").errors == {
        "confirm_password": [_("Les mots de passe doivent être identiques")]
    }
