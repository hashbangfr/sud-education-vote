from factory import Sequence, post_generation
from factory.django import DjangoModelFactory

from .models import Utilisateur

UTILISATEUR_PASSWORD = "hackme"


class UtilisateurFactory(DjangoModelFactory):
    username = Sequence(lambda n: "username{0}".format(n))
    email = Sequence(lambda n: "email{0}@test.com".format(n))

    @post_generation
    def set_password(self, create, extracted, **kwargs):
        self.set_password(UTILISATEUR_PASSWORD)
        self.save()

    class Meta:
        model = Utilisateur
