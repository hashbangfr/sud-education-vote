from django.urls import path

from .views import (
    AdminCreationView,
    AdminListView,
    AdminSupressionView,
    AdminUpdateView,
    ImportMandatsView,
    SyndicatCreateView,
    SyndicatListView,
    SyndicatSuppressionView,
    SyndicatUpdateView,
)

app_name = "utilisateur"

urlpatterns = [
    path("admins/", AdminListView.as_view(), name="admins"),
    path("admins/creation/", AdminCreationView.as_view(), name="admin-creation"),
    path(
        "admins/<int:pk>/edition/",
        AdminUpdateView.as_view(),
        name="admin-edition",
    ),
    path(
        "admins/<int:pk>/suppression/",
        AdminSupressionView.as_view(),
        name="admin-supression",
    ),
    path("syndicats/", SyndicatListView.as_view(), name="syndicats"),
    path("syndicats/creation/", SyndicatCreateView.as_view(), name="syndicat-creation"),
    path(
        "syndicats/<int:pk>/edition/",
        SyndicatUpdateView.as_view(),
        name="syndicat-edition",
    ),
    path(
        "syndicats/<int:pk>/suppression/",
        SyndicatSuppressionView.as_view(),
        name="syndicat-suppression",
    ),
    path("import-mandats/", ImportMandatsView.as_view(), name="import-mandats"),
]
