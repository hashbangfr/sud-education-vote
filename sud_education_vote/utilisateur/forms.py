from django import forms
from django.core.exceptions import ValidationError
from django.utils.translation import gettext_lazy as _

from .models import Utilisateur


class AdminCreationForm(forms.ModelForm):
    password = forms.CharField(
        max_length=255, label=_("Mot de passe"), widget=forms.PasswordInput()
    )
    confirm_password = forms.CharField(
        max_length=255, label=_("Confirm du mot de passe"), widget=forms.PasswordInput()
    )

    def clean_username(self):
        username = self.cleaned_data["username"]
        utilisateurs = Utilisateur.objects.filter(username=username)
        if utilisateurs.exists():
            raise ValidationError(
                "Il y a déjà un utilisateur avec ce nom d'utilisateur"
            )
        return username

    def clean_email(self):
        email = self.cleaned_data["email"]
        utilisateurs = Utilisateur.objects.filter(email=email)
        if utilisateurs.exists():
            raise ValidationError("Il y a déjà un utilisateur avec cet email")
        return email

    def clean(self):
        cleaned_data = super().clean()
        confirm_password = cleaned_data.get("confirm_password")
        if confirm_password != cleaned_data.get("password"):
            self.add_error(
                "confirm_password", _("Les mots de passe doivent être identiques")
            )
        return cleaned_data

    class Meta:
        model = Utilisateur
        fields = [
            "username",
            "email",
            "password",
        ]


class BaseForm(forms.ModelForm):
    password = forms.CharField(
        max_length=255, label=_("Mot de passe"), widget=forms.PasswordInput()
    )
    confirm_password = forms.CharField(
        max_length=255,
        label=_("Confirmation du mot de passe"),
        widget=forms.PasswordInput(),
    )

    _password = None

    def clean_username(self):
        username = self.cleaned_data["username"]
        utilisateurs = Utilisateur.objects.filter(username=username)
        if self.user:
            utilisateurs = utilisateurs.exclude(pk=self.user.pk)
        if utilisateurs.exists():
            raise ValidationError(
                "Il y a déjà un utilisateur avec ce nom d'utilisateur"
            )
        return username

    def clean_email(self):
        email = self.cleaned_data["email"]
        utilisateurs = Utilisateur.objects.filter(email=email)
        if self.user:
            utilisateurs = utilisateurs.exclude(pk=self.user.pk)
        if utilisateurs.exists():
            raise ValidationError("Il y a déjà un utilisateur avec cet email")
        return email

    def clean(self):
        cleaned_data = super().clean()
        confirm_password = cleaned_data.get("confirm_password")
        if confirm_password != cleaned_data.get("password"):
            self.add_error(
                "confirm_password", _("Les mots de passe doivent être identiques")
            )
        return cleaned_data

    def __init__(self, user, update, **kwargs):
        super().__init__(**kwargs)
        self.user = user
        if update:
            self.fields["password"].required = False
            self.fields["confirm_password"].required = False
            self._password = self.instance.password

    def save(self, commit=True):
        if self.cleaned_data.get("password"):
            self.instance.set_password(self.cleaned_data.get("password"))
        else:
            self.instance.password = self._password
        return super().save(commit=commit)

    class Meta:
        model = Utilisateur
        fields = []


class AdminUpdateForm(BaseForm):
    class Meta:
        model = Utilisateur
        fields = [
            "username",
            "email",
            "password",
        ]


class SyndicatForm(BaseForm):
    password = forms.CharField(
        max_length=255,
        label=_("Mot de passe (facultatif)"),
        widget=forms.PasswordInput(),
        required=False,
    )
    confirm_password = forms.CharField(
        max_length=255,
        label=_("Confirmation du mot de passe (facultatif)"),
        widget=forms.PasswordInput(),
        required=False,
    )

    class Meta:
        model = Utilisateur
        fields = [
            "username",
            "email",
            "nombre_mandat",
            "password",
        ]


class FichierImportMandatsForm(forms.Form):
    fichier = forms.FileField(label="Fichier d'import (.csv)")
