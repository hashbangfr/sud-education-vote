import pytest
from django.utils.translation import gettext_lazy as _

from sud_education_vote.vote.models import Scrutin, Vote

pytestmark = pytest.mark.django_db


def test_scrutin_type_display_delegue(scrutin_factory):
    scrutin = scrutin_factory(type_scrutin=Scrutin.DELEGUE)
    assert scrutin.type_scrutin_display == "Majorité simple des délégué⋅es"


def test_scrutin_type_display_majorite(scrutin_factory):
    scrutin = scrutin_factory(type_scrutin=Scrutin.MAJORITE)
    assert scrutin.type_scrutin_display == "Majorité des syndicats et mandats"


def test_scrutin_type_display_majorite_syndicats(scrutin_factory):
    scrutin = scrutin_factory(type_scrutin=Scrutin.MAJORITE_SYNDICATS)
    assert scrutin.type_scrutin_display == "Majorité simple des syndicats"


def test_scrutin_type_display_majorite_syndicats_opposition(scrutin_factory):
    scrutin = scrutin_factory(type_scrutin=Scrutin.MAJORITE_SYNDICATS_OPPOSITION)
    assert scrutin.type_scrutin_display == "Majorité des syndicats - Opposition"


def test_scrutin_type_display_majorite_syndicats_piorisation(scrutin_factory):
    scrutin = scrutin_factory(type_scrutin=Scrutin.MAJORITE_SYNDICATS_PRIORISATION)
    assert scrutin.type_scrutin_display == "Majorité des syndicats - Priorisation"


def test_scrutin_type_display_majorite_deux_tiers(scrutin_factory):
    scrutin = scrutin_factory(type_scrutin=Scrutin.MAJORITE_DEUX_TIERS)
    display = scrutin.type_scrutin_display
    assert display == "Majorité des 2/3 des syndicats et mandats"


def test_calcul_resultat_scrutin_delegue_adoption(scrutin_factory, vote_factory):
    scrutin = scrutin_factory(ouvert=True, type_scrutin=Scrutin.DELEGUE)
    vote_factory(
        scrutin=scrutin, mandats_pour=70, mandats_contre=20, mandats_abstention=10
    )
    scrutin.calcul_resultat()
    assert scrutin.adoption


def test_resultat_scrutin_pas_de_vote(scrutin_factory):
    scrutin = scrutin_factory(ouvert=False, type_scrutin=Scrutin.MAJORITE)
    assert scrutin.resultat == _("Pas de votes")


def test_calcul_resultat_scrutin_delegue_rejet(scrutin_factory, vote_factory):
    scrutin = scrutin_factory(ouvert=True, type_scrutin=Scrutin.DELEGUE)
    vote_factory(
        scrutin=scrutin, mandats_pour=20, mandats_contre=70, mandats_abstention=10
    )
    scrutin.calcul_resultat()
    assert not scrutin.adoption


def test_calcul_resultat_scrutin_delegue_egalite_rejet(scrutin_factory, vote_factory):
    scrutin = scrutin_factory(ouvert=True, type_scrutin=Scrutin.DELEGUE)
    vote_factory(
        scrutin=scrutin, mandats_pour=50, mandats_contre=50, mandats_abstention=0
    )
    scrutin.calcul_resultat()
    assert not scrutin.adoption


def test_calcul_resultat_scrutin_majorite_adoption(scrutin_factory, vote_factory):
    scrutin = scrutin_factory(ouvert=True, type_scrutin=Scrutin.MAJORITE)
    vote_factory(
        scrutin=scrutin,
        mandats_pour=70,
        mandats_contre=20,
        mandats_abstention=10,
        vote_utilisateur=Vote.POUR,
    )
    vote_factory(
        scrutin=scrutin,
        mandats_pour=70,
        mandats_contre=20,
        mandats_abstention=10,
        vote_utilisateur=Vote.POUR,
    )
    vote_factory(
        scrutin=scrutin,
        mandats_pour=70,
        mandats_contre=20,
        mandats_abstention=10,
        vote_utilisateur=Vote.POUR,
    )
    vote_factory(
        scrutin=scrutin,
        mandats_pour=70,
        mandats_contre=20,
        mandats_abstention=10,
        vote_utilisateur=Vote.POUR,
    )
    vote_factory(
        scrutin=scrutin,
        mandats_pour=70,
        mandats_contre=20,
        mandats_abstention=10,
        vote_utilisateur=Vote.CONTRE,
    )
    vote_factory(
        scrutin=scrutin,
        mandats_pour=70,
        mandats_contre=20,
        mandats_abstention=10,
        vote_utilisateur=Vote.CONTRE,
    )
    vote_factory(
        scrutin=scrutin,
        mandats_pour=0,
        mandats_contre=0,
        mandats_abstention=1,
        vote_utilisateur=Vote.ABSTENTION,
    )
    vote_factory(
        scrutin=scrutin,
        mandats_pour=0,
        mandats_contre=0,
        mandats_nppv=1,
        vote_utilisateur=Vote.NPPV,
    )
    scrutin.ouvert = False
    scrutin.save()
    scrutin.calcul_resultat()
    assert scrutin.adoption
    assert scrutin.resultat == _("Adopté")
    assert scrutin.resultat_syndicats == "P: 4\nC: 2\nA: 1\nN: 1"
    assert scrutin.resultat_mandats == "P: 420\nC: 120\nA: 61\nN: 1"


def test_calcul_resultat_scrutin_majorite_rejet_maj_syndicat_pour_maj_mandats_contre(
    scrutin_factory, vote_factory
):
    scrutin = scrutin_factory(ouvert=True, type_scrutin=Scrutin.MAJORITE)
    vote_factory(
        scrutin=scrutin,
        mandats_pour=70,
        mandats_contre=20,
        mandats_abstention=10,
        vote_utilisateur=Vote.POUR,
    )
    vote_factory(
        scrutin=scrutin,
        mandats_pour=70,
        mandats_contre=20,
        mandats_abstention=10,
        vote_utilisateur=Vote.POUR,
    )
    vote_factory(
        scrutin=scrutin,
        mandats_pour=70,
        mandats_contre=20,
        mandats_abstention=10,
        vote_utilisateur=Vote.POUR,
    )
    vote_factory(
        scrutin=scrutin,
        mandats_pour=70,
        mandats_contre=20,
        mandats_abstention=10,
        vote_utilisateur=Vote.POUR,
    )
    vote_factory(
        scrutin=scrutin,
        mandats_pour=70,
        mandats_contre=20,
        mandats_abstention=10,
        vote_utilisateur=Vote.CONTRE,
    )
    vote_factory(
        scrutin=scrutin,
        mandats_pour=70,
        mandats_contre=200,
        mandats_abstention=100,
        vote_utilisateur=Vote.CONTRE,
    )
    scrutin.ouvert = False
    scrutin.save()
    scrutin.calcul_resultat()
    assert not scrutin.adoption
    assert scrutin.resultat == _("Rejeté")


def test_calcul_resultat_scrutin_majorite_rejet_maj_syndicat_contre_maj_mandats_pour(
    scrutin_factory, vote_factory
):
    scrutin = scrutin_factory(ouvert=True, type_scrutin=Scrutin.MAJORITE)
    vote_factory(
        scrutin=scrutin,
        mandats_pour=70,
        mandats_contre=20,
        mandats_abstention=10,
        vote_utilisateur=Vote.CONTRE,
    )
    vote_factory(
        scrutin=scrutin,
        mandats_pour=70,
        mandats_contre=20,
        mandats_abstention=10,
        vote_utilisateur=Vote.CONTRE,
    )
    vote_factory(
        scrutin=scrutin,
        mandats_pour=70,
        mandats_contre=20,
        mandats_abstention=10,
        vote_utilisateur=Vote.CONTRE,
    )
    vote_factory(
        scrutin=scrutin,
        mandats_pour=70,
        mandats_contre=20,
        mandats_abstention=10,
        vote_utilisateur=Vote.CONTRE,
    )
    vote_factory(
        scrutin=scrutin,
        mandats_pour=70,
        mandats_contre=20,
        mandats_abstention=10,
        vote_utilisateur=Vote.CONTRE,
    )
    vote_factory(
        scrutin=scrutin,
        mandats_pour=70,
        mandats_contre=200,
        mandats_abstention=100,
        vote_utilisateur=Vote.POUR,
    )
    scrutin.ouvert = False
    scrutin.save()
    scrutin.calcul_resultat()
    assert not scrutin.adoption
    assert scrutin.resultat == _("Rejeté")


def test_calcul_resultat_scrutin_majorite_deux_tiers_adoption(
    scrutin_factory, vote_factory
):
    scrutin = scrutin_factory(ouvert=True, type_scrutin=Scrutin.MAJORITE_DEUX_TIERS)
    vote_factory(
        scrutin=scrutin,
        mandats_pour=100,
        mandats_contre=10,
        mandats_abstention=10,
        vote_utilisateur=Vote.POUR,
    )
    vote_factory(
        scrutin=scrutin,
        mandats_pour=100,
        mandats_contre=10,
        mandats_abstention=10,
        vote_utilisateur=Vote.POUR,
    )
    vote_factory(
        scrutin=scrutin,
        mandats_pour=100,
        mandats_contre=10,
        mandats_abstention=10,
        vote_utilisateur=Vote.POUR,
    )
    vote_factory(
        scrutin=scrutin,
        mandats_pour=100,
        mandats_contre=10,
        mandats_abstention=10,
        vote_utilisateur=Vote.POUR,
    )
    vote_factory(
        scrutin=scrutin,
        mandats_pour=20,
        mandats_contre=60,
        mandats_abstention=10,
        vote_utilisateur=Vote.CONTRE,
    )
    vote_factory(
        scrutin=scrutin,
        mandats_pour=20,
        mandats_contre=60,
        mandats_abstention=10,
        vote_utilisateur=Vote.CONTRE,
    )
    scrutin.ouvert = False
    scrutin.save()
    scrutin.calcul_resultat()
    assert scrutin.adoption
    assert scrutin.resultat == _("Adopté")


def test_calcul_resultat_deux_tiers_rejete_maj_syndicat_pour_maj_mandat_contre(
    scrutin_factory, vote_factory
):
    scrutin = scrutin_factory(ouvert=True, type_scrutin=Scrutin.MAJORITE_DEUX_TIERS)
    vote_factory(
        scrutin=scrutin,
        mandats_pour=90,
        mandats_contre=10,
        mandats_abstention=10,
        vote_utilisateur=Vote.POUR,
    )
    vote_factory(
        scrutin=scrutin,
        mandats_pour=90,
        mandats_contre=10,
        mandats_abstention=10,
        vote_utilisateur=Vote.POUR,
    )
    vote_factory(
        scrutin=scrutin,
        mandats_pour=90,
        mandats_contre=10,
        mandats_abstention=10,
        vote_utilisateur=Vote.POUR,
    )
    vote_factory(
        scrutin=scrutin,
        mandats_pour=90,
        mandats_contre=40,
        mandats_abstention=10,
        vote_utilisateur=Vote.POUR,
    )
    vote_factory(
        scrutin=scrutin,
        mandats_pour=20,
        mandats_contre=60,
        mandats_abstention=10,
        vote_utilisateur=Vote.CONTRE,
    )
    vote_factory(
        scrutin=scrutin,
        mandats_pour=20,
        mandats_contre=60,
        mandats_abstention=10,
        vote_utilisateur=Vote.CONTRE,
    )
    scrutin.ouvert = False
    scrutin.save()
    scrutin.calcul_resultat()
    assert not scrutin.adoption
    assert scrutin.resultat == _("Rejeté")


def test_calcul_resultat_deux_tiers_rejete_maj_syndicat_contre_maj_mandat_pour(
    scrutin_factory, vote_factory
):
    scrutin = scrutin_factory(ouvert=True, type_scrutin=Scrutin.MAJORITE_DEUX_TIERS)
    vote_factory(
        scrutin=scrutin,
        mandats_pour=300,
        mandats_contre=10,
        mandats_abstention=10,
        vote_utilisateur=Vote.POUR,
    )
    vote_factory(
        scrutin=scrutin,
        mandats_pour=100,
        mandats_contre=10,
        mandats_abstention=10,
        vote_utilisateur=Vote.POUR,
    )
    vote_factory(
        scrutin=scrutin,
        mandats_pour=100,
        mandats_contre=10,
        mandats_abstention=10,
        vote_utilisateur=Vote.POUR,
    )
    vote_factory(
        scrutin=scrutin,
        mandats_pour=40,
        mandats_contre=60,
        mandats_abstention=10,
        vote_utilisateur=Vote.CONTRE,
    )
    vote_factory(
        scrutin=scrutin,
        mandats_pour=20,
        mandats_contre=60,
        mandats_abstention=10,
        vote_utilisateur=Vote.CONTRE,
    )
    vote_factory(
        scrutin=scrutin,
        mandats_pour=20,
        mandats_contre=60,
        mandats_abstention=10,
        vote_utilisateur=Vote.CONTRE,
    )
    scrutin.ouvert = False
    scrutin.save()
    scrutin.calcul_resultat()
    assert not scrutin.adoption
    assert scrutin.resultat == _("Rejeté")


def test_calcul_resultat_scrutin_majorite_syndicats_adoption(
    scrutin_factory, vote_factory
):
    scrutin = scrutin_factory(ouvert=True, type_scrutin=Scrutin.MAJORITE_SYNDICATS)
    vote_factory(
        scrutin=scrutin,
        vote_utilisateur=Vote.POUR,
    )
    vote_factory(
        scrutin=scrutin,
        vote_utilisateur=Vote.POUR,
    )
    vote_factory(
        scrutin=scrutin,
        vote_utilisateur=Vote.POUR,
    )
    vote_factory(
        scrutin=scrutin,
        vote_utilisateur=Vote.POUR,
    )
    vote_factory(
        scrutin=scrutin,
        vote_utilisateur=Vote.CONTRE,
    )
    vote_factory(
        scrutin=scrutin,
        vote_utilisateur=Vote.CONTRE,
    )
    vote_factory(
        scrutin=scrutin,
        vote_utilisateur=Vote.ABSTENTION,
    )
    vote_factory(
        scrutin=scrutin,
        vote_utilisateur=Vote.NPPV,
    )
    scrutin.ouvert = False
    scrutin.save()
    scrutin.calcul_resultat()
    assert scrutin.adoption
    assert scrutin.resultat == _("Adopté")
    assert scrutin.resultat_syndicats == "P: 4\nC: 2\nA: 1\nN: 1"


def test_calcul_resultat_scrutin_majorite_syndicats_rejet(
    scrutin_factory, vote_factory
):
    scrutin = scrutin_factory(ouvert=True, type_scrutin=Scrutin.MAJORITE_SYNDICATS)
    vote_factory(
        scrutin=scrutin,
        vote_utilisateur=Vote.POUR,
    )
    vote_factory(
        scrutin=scrutin,
        vote_utilisateur=Vote.POUR,
    )
    vote_factory(
        scrutin=scrutin,
        vote_utilisateur=Vote.CONTRE,
    )
    vote_factory(
        scrutin=scrutin,
        vote_utilisateur=Vote.CONTRE,
    )
    vote_factory(
        scrutin=scrutin,
        vote_utilisateur=Vote.CONTRE,
    )
    vote_factory(
        scrutin=scrutin,
        vote_utilisateur=Vote.CONTRE,
    )
    vote_factory(
        scrutin=scrutin,
        vote_utilisateur=Vote.ABSTENTION,
    )
    vote_factory(
        scrutin=scrutin,
        vote_utilisateur=Vote.NPPV,
    )
    scrutin.ouvert = False
    scrutin.save()
    scrutin.calcul_resultat()
    assert not scrutin.adoption
    assert scrutin.resultat == _("Rejeté")
    assert scrutin.resultat_syndicats == "P: 2\nC: 4\nA: 1\nN: 1"


def test_calcul_resultat_scrutin_majorite_syndicats_opposition(
    scrutin_factory, proposition_factory, vote_opposition_factory
):
    scrutin = scrutin_factory(
        ouvert=True, type_scrutin=Scrutin.MAJORITE_SYNDICATS_OPPOSITION
    )
    proposition1 = proposition_factory(text="prop1", scrutin=scrutin)
    proposition2 = proposition_factory(text="prop2", scrutin=scrutin)
    vote_opposition_factory(
        scrutin=scrutin,
        selected_proposition=proposition1,
    )
    vote_opposition_factory(
        scrutin=scrutin,
        selected_proposition=proposition1,
    )
    vote_opposition_factory(
        scrutin=scrutin,
        selected_proposition=proposition2,
    )
    scrutin.ouvert = False
    scrutin.save()
    assert scrutin.resultat == _("prop1")
    assert (
        scrutin.resultat_syndicats_opposition
        == "prop1 : 2\nprop2 : 1\nAbstention : 0\nNppv : 0\n"
    )


def test_calcul_resultat_scrutin_majorite_syndicats_priorisation(
    scrutin_factory,
    proposition_factory,
    vote_priorisation_factory,
    vote_proposition_factory,
):
    scrutin = scrutin_factory(
        ouvert=True, type_scrutin=Scrutin.MAJORITE_SYNDICATS_PRIORISATION
    )
    proposition1 = proposition_factory(text="prop1", scrutin=scrutin)
    proposition2 = proposition_factory(text="prop2", scrutin=scrutin)
    vote1 = vote_priorisation_factory(
        scrutin=scrutin,
    )
    vote_proposition_factory(proposition=proposition1, vote=vote1, score=1)
    vote_proposition_factory(proposition=proposition2, vote=vote1, score=2)
    vote2 = vote_priorisation_factory(
        scrutin=scrutin,
    )
    vote_proposition_factory(proposition=proposition1, vote=vote2, score=1)
    vote_proposition_factory(proposition=proposition2, vote=vote2, score=2)
    vote_priorisation_factory(scrutin=scrutin, abstention=True)
    scrutin.ouvert = False
    scrutin.save()
    assert scrutin.resultat == _("prop2")
    assert (
        scrutin.resultat_syndicats_priorisation
        == "prop2 : 4\nprop1 : 2\nAbstention : 1\nNppv : 0\n"
    )
