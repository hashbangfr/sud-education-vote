import pytest
from django.urls import reverse

from sud_education_vote.log.models import Log
from sud_education_vote.vote.forms import VoteDelegueCreationForm
from sud_education_vote.vote.models import Scrutin, Vote

pytestmark = pytest.mark.django_db


def test_creation_vote_delegue_scrutin_redirection_si_non_connecte(
    client, scrutin_factory
):
    scrutin = scrutin_factory(ouvert=True, type_scrutin=Scrutin.DELEGUE)
    url_vote_scrutin = reverse("vote:scrutin-delegue", kwargs={"pk": scrutin.pk})
    response = client.get(url_vote_scrutin)
    assert response.status_code == 302
    assert response.url == f"{reverse('connexion')}?next={url_vote_scrutin}"


def test_creation_vote_delegue_scrutin_pas_de_permission_syndicat(
    client_syndicat, scrutin_factory
):
    scrutin = scrutin_factory(ouvert=True, type_scrutin=Scrutin.DELEGUE)
    response = client_syndicat.get(
        reverse("vote:scrutin-delegue", kwargs={"pk": scrutin.pk})
    )
    assert response.status_code == 403


def test_creation_vote_delegue_scrutin_pas_de_permission_superadmin(
    client_superadmin, scrutin_factory
):
    scrutin = scrutin_factory(ouvert=True, type_scrutin=Scrutin.DELEGUE)
    response = client_superadmin.get(
        reverse("vote:scrutin-delegue", kwargs={"pk": scrutin.pk})
    )
    assert response.status_code == 403


def test_creation_vote_delegue_scrutin_ok_pour_admin(client_admin, scrutin_factory):
    scrutin = scrutin_factory(ouvert=True, type_scrutin=Scrutin.DELEGUE)
    response = client_admin.get(
        reverse("vote:scrutin-delegue", kwargs={"pk": scrutin.pk})
    )
    assert response.status_code == 200
    assert isinstance(response.context["form"], VoteDelegueCreationForm)


def test_creation_vote_delegue_scrutin_succes_adopte(
    client_admin, utilisateur_admin, scrutin_factory
):
    scrutin = scrutin_factory(ouvert=True, type_scrutin=Scrutin.DELEGUE)
    assert (
        Scrutin.objects.filter(type_scrutin=Scrutin.DELEGUE, ouvert=True).count() == 1
    )
    response = client_admin.post(
        reverse("vote:scrutin-delegue", kwargs={"pk": scrutin.pk}),
        data={
            "mandats_pour": 61,
            "mandats_contre": 10,
            "mandats_abstention": 10,
            "mandats_nppv": 20,
            "utilisateur": utilisateur_admin.pk,
            "scrutin": scrutin.pk,
        },
    )
    assert response.status_code == 302
    assert response.url == reverse("vote:scrutins")
    assert (
        Scrutin.objects.filter(type_scrutin=Scrutin.DELEGUE, ouvert=True).count() == 0
    )
    assert Vote.objects.count() == 1
    scrutin.refresh_from_db()
    assert not scrutin.ouvert
    assert scrutin.adoption
    vote = Vote.objects.last()
    assert vote.mandats_pour == 61
    assert vote.mandats_contre == 10
    assert vote.mandats_abstention == 10
    assert vote.mandats_nppv == 20
    assert vote.utilisateur == utilisateur_admin
    assert vote.scrutin == scrutin
    assert Log.objects.filter(
        log=(
            f"L'admin {utilisateur_admin} a rentré les votes suivant "
            f"pour le scrutin {scrutin} par delegue: "
            f"{vote.mandats_pour} votes 'Pour', "
            f"{vote.mandats_contre} votes 'Contre', "
            f"{vote.mandats_abstention} votes 'Abstention', "
            f"et {vote.mandats_nppv} votes 'Ne se prononce pas'."
        )
    ).exists()
    assert Log.objects.filter(
        log=(f"L'admin {utilisateur_admin} a fermé " f"le scrutin {scrutin}")
    ).exists()
    assert Log.objects.filter(
        log=(f"Le scrutin {scrutin} voit sa résolution adoptée")
    ).exists()


def test_creation_vote_delegue_scrutin_succes_rejete_50_50(
    client_admin, utilisateur_admin, scrutin_factory
):
    scrutin = scrutin_factory(ouvert=True, type_scrutin=Scrutin.DELEGUE)
    assert (
        Scrutin.objects.filter(type_scrutin=Scrutin.DELEGUE, ouvert=True).count() == 1
    )
    response = client_admin.post(
        reverse("vote:scrutin-delegue", kwargs={"pk": scrutin.pk}),
        data={
            "mandats_pour": 20,
            "mandats_contre": 10,
            "mandats_abstention": 10,
            "mandats_nppv": 0,
            "utilisateur": utilisateur_admin.pk,
            "scrutin": scrutin.pk,
        },
    )
    assert response.status_code == 302
    assert response.url == reverse("vote:scrutins")
    assert (
        Scrutin.objects.filter(type_scrutin=Scrutin.DELEGUE, ouvert=True).count() == 0
    )
    assert Vote.objects.count() == 1
    scrutin.refresh_from_db()
    assert not scrutin.ouvert
    assert not scrutin.adoption
    vote = Vote.objects.last()
    assert vote.mandats_pour == 20
    assert vote.mandats_contre == 10
    assert vote.mandats_abstention == 10
    assert vote.mandats_nppv == 0
    assert vote.utilisateur == utilisateur_admin
    assert vote.scrutin == scrutin
    assert Log.objects.filter(
        log=(
            f"L'admin {utilisateur_admin} a rentré les votes suivant "
            f"pour le scrutin {scrutin} par delegue: "
            f"{vote.mandats_pour} votes 'Pour', "
            f"{vote.mandats_contre} votes 'Contre', "
            f"{vote.mandats_abstention} votes 'Abstention', "
            f"et {vote.mandats_nppv} votes 'Ne se prononce pas'."
        )
    ).exists()
    assert Log.objects.filter(
        log=(f"L'admin {utilisateur_admin} a fermé " f"le scrutin {scrutin}")
    ).exists()
    assert Log.objects.filter(
        log=(f"Le scrutin {scrutin} voit sa résolution rejetée")
    ).exists()


def test_creation_vote_delegue_scrutin_succes_rejete(
    client_admin, utilisateur_admin, scrutin_factory
):
    scrutin = scrutin_factory(ouvert=True, type_scrutin=Scrutin.DELEGUE)
    assert (
        Scrutin.objects.filter(type_scrutin=Scrutin.DELEGUE, ouvert=True).count() == 1
    )
    response = client_admin.post(
        reverse("vote:scrutin-delegue", kwargs={"pk": scrutin.pk}),
        data={
            "mandats_pour": 20,
            "mandats_contre": 40,
            "mandats_abstention": 10,
            "mandats_nppv": 20,
            "utilisateur": utilisateur_admin.pk,
            "scrutin": scrutin.pk,
        },
    )
    assert response.status_code == 302
    assert response.url == reverse("vote:scrutins")
    assert (
        Scrutin.objects.filter(type_scrutin=Scrutin.DELEGUE, ouvert=True).count() == 0
    )
    assert Vote.objects.count() == 1
    scrutin.refresh_from_db()
    assert not scrutin.ouvert
    assert not scrutin.adoption
    vote = Vote.objects.last()
    assert vote.mandats_pour == 20
    assert vote.mandats_contre == 40
    assert vote.mandats_abstention == 10
    assert vote.mandats_nppv == 20
    assert vote.utilisateur == utilisateur_admin
    assert vote.scrutin == scrutin
    assert Log.objects.filter(
        log=(
            f"L'admin {utilisateur_admin} a rentré les votes suivant "
            f"pour le scrutin {scrutin} par delegue: "
            f"{vote.mandats_pour} votes 'Pour', "
            f"{vote.mandats_contre} votes 'Contre', "
            f"{vote.mandats_abstention} votes 'Abstention', "
            f"et {vote.mandats_nppv} votes 'Ne se prononce pas'."
        )
    ).exists()
    assert Log.objects.filter(
        log=(f"L'admin {utilisateur_admin} a fermé " f"le scrutin {scrutin}")
    ).exists()
    assert Log.objects.filter(
        log=(f"Le scrutin {scrutin} voit sa résolution rejetée")
    ).exists()


def test_creation_vote_delegue_scrutin_errors(
    client_admin, utilisateur_admin, scrutin_factory
):
    scrutin = scrutin_factory(ouvert=True, type_scrutin=Scrutin.DELEGUE)
    assert (
        Scrutin.objects.filter(type_scrutin=Scrutin.DELEGUE, ouvert=True).count() == 1
    )
    response = client_admin.post(
        reverse("vote:scrutin-delegue", kwargs={"pk": scrutin.pk}),
        data={},
    )
    assert response.status_code == 200
    assert (
        Scrutin.objects.filter(type_scrutin=Scrutin.DELEGUE, ouvert=True).count() == 1
    )
    assert response.context.get("form").errors == {
        "mandats_abstention": ["Ce champ est obligatoire."],
        "mandats_contre": ["Ce champ est obligatoire."],
        "mandats_nppv": ["Ce champ est obligatoire."],
        "mandats_pour": ["Ce champ est obligatoire."],
        "scrutin": ["Ce champ est obligatoire."],
        "utilisateur": ["Ce champ est obligatoire."],
    }
