import pytest
from django.urls import reverse

from sud_education_vote.log.models import Log
from sud_education_vote.vote.forms import (
    VoteMajoriteSyndicatsOppositionUpdateForm,
    VoteMajoriteSyndicatsPriorisationUpdateForm,
    VoteMajoriteSyndicatsUpdateForm,
    VoteUpdateForm,
)
from sud_education_vote.vote.models import Scrutin, Vote

pytestmark = pytest.mark.django_db


def test_update_vote_redirection_vote_simple(
    client_syndicat,
    scrutin_factory,
    vote_factory,
    utilisateur_syndicat,
):
    scrutin = scrutin_factory(type_scrutin=Scrutin.MAJORITE)
    vote = vote_factory(
        scrutin=scrutin,
        vote_utilisateur=Vote.POUR,
        utilisateur=utilisateur_syndicat,
        mandats_pour=60,
        mandats_contre=30,
        mandats_abstention=10,
        mandats_nppv=5,
    )

    response = client_syndicat.get(
        reverse("vote:vote-modification", kwargs={"scrutin": scrutin.pk, "pk": vote.pk})
    )
    assert response.status_code == 302
    assert response.url == reverse(
        "vote:vote-simple-modification", kwargs={"scrutin": scrutin.pk, "pk": vote.pk}
    )


def test_update_vote_redirection_vote_opposition(
    client_syndicat,
    scrutin_factory,
    vote_opposition_factory,
    proposition_factory,
    utilisateur_syndicat,
):
    scrutin_opposition = scrutin_factory(
        ouvert=False, type_scrutin=Scrutin.MAJORITE_SYNDICATS_OPPOSITION
    )
    proposition1 = proposition_factory(text="prop1", scrutin=scrutin_opposition)
    vote_opposition = vote_opposition_factory(
        scrutin=scrutin_opposition,
        selected_proposition=proposition1,
        utilisateur=utilisateur_syndicat,
    )

    response = client_syndicat.get(
        reverse(
            "vote:vote-modification",
            kwargs={"scrutin": scrutin_opposition.pk, "pk": vote_opposition.pk},
        )
    )
    assert response.status_code == 302
    assert response.url == reverse(
        "vote:vote-opposition-modification",
        kwargs={"scrutin": scrutin_opposition.pk, "pk": vote_opposition.pk},
    )


def test_update_vote_redirection_vote_priorisation(
    client_syndicat,
    scrutin_factory,
    vote_priorisation_factory,
    vote_proposition_factory,
    proposition_factory,
    utilisateur_syndicat,
):
    scrutin_priorisation = scrutin_factory(
        ouvert=False, type_scrutin=Scrutin.MAJORITE_SYNDICATS_PRIORISATION
    )
    proposition2 = proposition_factory(text="prop2", scrutin=scrutin_priorisation)
    proposition3 = proposition_factory(text="prop3", scrutin=scrutin_priorisation)
    vote_priorisation = vote_priorisation_factory(
        scrutin=scrutin_priorisation,
        utilisateur=utilisateur_syndicat,
    )
    vote_proposition_factory(proposition=proposition2, vote=vote_priorisation, score=1)
    vote_proposition_factory(proposition=proposition3, vote=vote_priorisation, score=2)

    response = client_syndicat.get(
        reverse(
            "vote:vote-modification",
            kwargs={"scrutin": scrutin_priorisation.pk, "pk": vote_priorisation.pk},
        )
    )
    assert response.status_code == 302
    assert response.url == reverse(
        "vote:vote-priorisation-modification",
        kwargs={"scrutin": scrutin_priorisation.pk, "pk": vote_priorisation.pk},
    )


def test_update_vote_redirection_si_non_connecte(
    client, scrutin_factory, vote_factory, utilisateur_syndicat
):
    scrutin = scrutin_factory(ouvert=True, type_scrutin=Scrutin.MAJORITE)
    utilisateur_syndicat.nombre_mandat = 40
    utilisateur_syndicat.save()
    vote = vote_factory(
        scrutin=scrutin,
        utilisateur=utilisateur_syndicat,
        vote_utilisateur="POUR",
        mandats_pour=30,
        mandats_contre=5,
        mandats_abstention=3,
        mandats_nppv=2,
    )
    url_page_vote = reverse(
        "vote:vote-simple-modification", kwargs={"scrutin": scrutin.pk, "pk": vote.pk}
    )
    response = client.get(url_page_vote)
    assert response.status_code == 302
    assert response.url == f"{reverse('connexion')}?next={url_page_vote}"


def test_update_vote_scrutin_superadmin_not_ok(
    client_superadmin, scrutin_factory, vote_factory, utilisateur_syndicat
):
    scrutin = scrutin_factory(ouvert=True, type_scrutin=Scrutin.MAJORITE)
    utilisateur_syndicat.nombre_mandat = 40
    utilisateur_syndicat.save()
    vote = vote_factory(
        scrutin=scrutin,
        utilisateur=utilisateur_syndicat,
        vote_utilisateur="POUR",
        mandats_pour=30,
        mandats_contre=5,
        mandats_abstention=3,
        mandats_nppv=2,
    )
    response = client_superadmin.get(
        reverse(
            "vote:vote-simple-modification",
            kwargs={"scrutin": scrutin.pk, "pk": vote.pk},
        )
    )
    assert response.status_code == 302


def test_update_vote_scrutin_ok_pour_syndicat(
    client_syndicat, scrutin_factory, vote_factory, utilisateur_syndicat
):
    scrutin = scrutin_factory(ouvert=True, type_scrutin=Scrutin.MAJORITE)
    utilisateur_syndicat.nombre_mandat = 40
    utilisateur_syndicat.save()
    vote = vote_factory(
        scrutin=scrutin,
        utilisateur=utilisateur_syndicat,
        vote_utilisateur="POUR",
        mandats_pour=30,
        mandats_contre=5,
        mandats_abstention=3,
        mandats_nppv=2,
    )
    response = client_syndicat.get(
        reverse(
            "vote:vote-simple-modification",
            kwargs={"scrutin": scrutin.pk, "pk": vote.pk},
        )
    )
    assert response.status_code == 200
    assert response.context.get("scrutin")
    assert isinstance(response.context.get("form"), VoteUpdateForm)


def test_update_vote_scrutin_majorite_syndicat_ok_pour_syndicat(
    client_syndicat, scrutin_factory, vote_factory, utilisateur_syndicat
):
    scrutin = scrutin_factory(ouvert=True, type_scrutin=Scrutin.MAJORITE_SYNDICATS)
    vote = vote_factory(
        scrutin=scrutin,
        utilisateur=utilisateur_syndicat,
        vote_utilisateur="POUR",
    )
    response = client_syndicat.get(
        reverse(
            "vote:vote-simple-modification",
            kwargs={"scrutin": scrutin.pk, "pk": vote.pk},
        )
    )
    assert response.status_code == 200
    assert response.context.get("scrutin")
    assert isinstance(response.context.get("form"), VoteMajoriteSyndicatsUpdateForm)


def test_post_update_vote_scrutin_ok_pour_syndicat(
    client_syndicat, scrutin_factory, vote_factory, utilisateur_syndicat
):
    scrutin = scrutin_factory(ouvert=True, type_scrutin=Scrutin.MAJORITE)
    utilisateur_syndicat.nombre_mandat = 40
    utilisateur_syndicat.save()
    vote = vote_factory(
        scrutin=scrutin,
        utilisateur=utilisateur_syndicat,
        vote_utilisateur=Vote.POUR,
        mandats_pour=30,
        mandats_contre=5,
        mandats_abstention=3,
        mandats_nppv=2,
    )
    scrutin.calcul_resultat()
    assert scrutin.adoption
    response = client_syndicat.post(
        reverse(
            "vote:vote-simple-modification",
            kwargs={"scrutin": scrutin.pk, "pk": vote.pk},
        ),
        {
            "vote_utilisateur": Vote.CONTRE,
            "mandats_pour": 2,
            "mandats_contre": 30,
            "mandats_abstention": 4,
            "mandats_nppv": 4,
            "scrutin": scrutin.pk,
            "utilisateur": utilisateur_syndicat.pk,
        },
    )
    assert response.status_code == 302
    scrutin.refresh_from_db()
    assert not scrutin.adoption
    assert Log.objects.count() == 2


def test_post_update_vote_scrutin_majorite_syndicat_ok_pour_syndicat(
    client_syndicat, scrutin_factory, vote_factory, utilisateur_syndicat
):
    scrutin = scrutin_factory(ouvert=True, type_scrutin=Scrutin.MAJORITE_SYNDICATS)
    utilisateur_syndicat.nombre_mandat = 40
    utilisateur_syndicat.save()
    vote = vote_factory(
        scrutin=scrutin,
        utilisateur=utilisateur_syndicat,
        vote_utilisateur=Vote.POUR,
    )
    scrutin.calcul_resultat()
    assert scrutin.adoption
    response = client_syndicat.post(
        reverse(
            "vote:vote-simple-modification",
            kwargs={"scrutin": scrutin.pk, "pk": vote.pk},
        ),
        {
            "vote_utilisateur": Vote.CONTRE,
            "scrutin": scrutin.pk,
            "utilisateur": utilisateur_syndicat.pk,
        },
    )
    assert response.status_code == 302
    scrutin.refresh_from_db()
    assert not scrutin.adoption
    assert Log.objects.count() == 2


def test_post_update_vote_scrutin_not_ok_pour_syndicat_scrutin_ferme(
    client_syndicat, scrutin_factory, vote_factory, utilisateur_syndicat
):
    scrutin = scrutin_factory(ouvert=True, type_scrutin=Scrutin.MAJORITE)
    utilisateur_syndicat.nombre_mandat = 40
    utilisateur_syndicat.save()
    vote = vote_factory(
        scrutin=scrutin,
        utilisateur=utilisateur_syndicat,
        vote_utilisateur=Vote.POUR,
        mandats_pour=30,
        mandats_contre=5,
        mandats_abstention=3,
        mandats_nppv=2,
    )
    scrutin.calcul_resultat()
    assert scrutin.adoption
    scrutin.ouvert = False
    scrutin.save()
    response = client_syndicat.post(
        reverse(
            "vote:vote-simple-modification",
            kwargs={"scrutin": scrutin.pk, "pk": vote.pk},
        ),
        {
            "vote_utilisateur": Vote.CONTRE,
            "mandats_pour": 2,
            "mandats_contre": 30,
            "mandats_abstention": 4,
            "mandats_nppv": 4,
            "scrutin": scrutin.pk,
            "utilisateur": utilisateur_syndicat.pk,
        },
    )
    assert response.status_code == 302


def test_update_vote_scrutin_ok_pour_admin(
    client_admin, scrutin_factory, vote_factory, utilisateur_syndicat
):
    scrutin = scrutin_factory(ouvert=True, type_scrutin=Scrutin.MAJORITE)
    utilisateur_syndicat.nombre_mandat = 40
    utilisateur_syndicat.save()
    vote = vote_factory(
        scrutin=scrutin,
        utilisateur=utilisateur_syndicat,
        vote_utilisateur="POUR",
        mandats_pour=30,
        mandats_contre=5,
        mandats_abstention=3,
        mandats_nppv=2,
    )
    response = client_admin.get(
        reverse(
            "vote:vote-simple-modification",
            kwargs={"scrutin": scrutin.pk, "pk": vote.pk},
        )
    )
    assert response.status_code == 200
    assert response.context.get("scrutin")
    assert isinstance(response.context.get("form"), VoteUpdateForm)


def test_post_update_vote_scrutin_ok_pour_admin(
    client_admin, scrutin_factory, vote_factory, utilisateur_syndicat
):
    scrutin = scrutin_factory(ouvert=True, type_scrutin=Scrutin.MAJORITE)
    utilisateur_syndicat.nombre_mandat = 40
    utilisateur_syndicat.save()
    vote = vote_factory(
        scrutin=scrutin,
        utilisateur=utilisateur_syndicat,
        vote_utilisateur=Vote.POUR,
        mandats_pour=30,
        mandats_contre=5,
        mandats_abstention=3,
        mandats_nppv=2,
    )
    scrutin.calcul_resultat()
    assert scrutin.adoption
    response = client_admin.post(
        reverse(
            "vote:vote-simple-modification",
            kwargs={"scrutin": scrutin.pk, "pk": vote.pk},
        ),
        {
            "vote_utilisateur": Vote.CONTRE,
            "mandats_pour": 2,
            "mandats_contre": 30,
            "mandats_abstention": 4,
            "mandats_nppv": 4,
            "scrutin": scrutin.pk,
            "utilisateur": utilisateur_syndicat.pk,
        },
    )
    assert response.status_code == 302
    scrutin.refresh_from_db()
    assert not scrutin.adoption
    assert Log.objects.count() == 2


def test_update_vote_scrutin_opposition_ok_pour_admin(
    client_admin,
    scrutin_factory,
    vote_opposition_factory,
    proposition_factory,
    utilisateur_syndicat,
):
    scrutin = scrutin_factory(
        ouvert=True, type_scrutin=Scrutin.MAJORITE_SYNDICATS_OPPOSITION
    )
    proposition = proposition_factory(text="prop1", scrutin=scrutin)
    vote = vote_opposition_factory(
        scrutin=scrutin,
        selected_proposition=proposition,
        utilisateur=utilisateur_syndicat,
    )
    response = client_admin.get(
        reverse(
            "vote:vote-opposition-modification",
            kwargs={"scrutin": scrutin.pk, "pk": vote.pk},
        )
    )
    assert response.status_code == 200
    assert response.context.get("scrutin")
    assert isinstance(
        response.context.get("form"), VoteMajoriteSyndicatsOppositionUpdateForm
    )


def test_post_update_vote_scrutin_opposition_ok_pour_admin(
    client_admin,
    scrutin_factory,
    vote_opposition_factory,
    proposition_factory,
    utilisateur_syndicat,
):
    scrutin = scrutin_factory(
        ouvert=True, type_scrutin=Scrutin.MAJORITE_SYNDICATS_OPPOSITION
    )
    proposition = proposition_factory(text="prop1", scrutin=scrutin)
    proposition2 = proposition_factory(text="prop2", scrutin=scrutin)
    vote = vote_opposition_factory(
        scrutin=scrutin,
        selected_proposition=proposition,
        utilisateur=utilisateur_syndicat,
    )
    scrutin.ouvert = False
    scrutin.save()
    assert scrutin.resultat == "prop1"
    response = client_admin.post(
        reverse(
            "vote:vote-opposition-modification",
            kwargs={"scrutin": scrutin.pk, "pk": vote.pk},
        ),
        {
            "selected_proposition": proposition2.id,
            "scrutin": scrutin.pk,
            "utilisateur": utilisateur_syndicat.pk,
        },
    )
    assert response.status_code == 302
    scrutin.ouvert = False
    scrutin.save()
    scrutin.refresh_from_db()
    assert scrutin.resultat == "prop2"


def test_post_update_vote_scrutin_opposition_ok_pour_syndicat(
    client_syndicat,
    scrutin_factory,
    vote_opposition_factory,
    proposition_factory,
    utilisateur_syndicat,
):
    scrutin = scrutin_factory(
        ouvert=True, type_scrutin=Scrutin.MAJORITE_SYNDICATS_OPPOSITION
    )
    proposition = proposition_factory(text="prop1", scrutin=scrutin)
    proposition2 = proposition_factory(text="prop2", scrutin=scrutin)
    vote = vote_opposition_factory(
        scrutin=scrutin,
        selected_proposition=proposition,
        utilisateur=utilisateur_syndicat,
    )
    scrutin.ouvert = False
    scrutin.save()
    assert scrutin.resultat == "prop1"
    scrutin.ouvert = True
    scrutin.save()
    response = client_syndicat.post(
        reverse(
            "vote:vote-opposition-modification",
            kwargs={"scrutin": scrutin.pk, "pk": vote.pk},
        ),
        {
            "selected_proposition": proposition2.id,
            "scrutin": scrutin.pk,
            "utilisateur": utilisateur_syndicat.pk,
        },
    )
    assert response.status_code == 302
    scrutin.ouvert = False
    scrutin.save()
    scrutin.refresh_from_db()
    assert scrutin.resultat == "prop2"


def test_update_vote_scrutin_priorisation_ok_pour_admin(
    client_admin,
    scrutin_factory,
    vote_priorisation_factory,
    vote_proposition_factory,
    proposition_factory,
    utilisateur_syndicat,
):
    scrutin = scrutin_factory(
        ouvert=True, type_scrutin=Scrutin.MAJORITE_SYNDICATS_PRIORISATION
    )
    proposition1 = proposition_factory(text="prop1", scrutin=scrutin)
    proposition2 = proposition_factory(text="prop2", scrutin=scrutin)
    vote = vote_priorisation_factory(
        scrutin=scrutin,
        utilisateur=utilisateur_syndicat,
    )
    vote_proposition_factory(proposition=proposition1, vote=vote, score=1)
    vote_proposition_factory(proposition=proposition2, vote=vote, score=2)
    response = client_admin.get(
        reverse(
            "vote:vote-priorisation-modification",
            kwargs={"scrutin": scrutin.pk, "pk": vote.pk},
        )
    )
    assert response.status_code == 200
    assert response.context.get("scrutin")
    assert isinstance(
        response.context.get("form"), VoteMajoriteSyndicatsPriorisationUpdateForm
    )


def test_post_update_vote_scrutin_priorisation_ok_pour_admin(
    client_admin,
    scrutin_factory,
    vote_priorisation_factory,
    vote_proposition_factory,
    proposition_factory,
    utilisateur_syndicat,
):
    scrutin = scrutin_factory(
        ouvert=True, type_scrutin=Scrutin.MAJORITE_SYNDICATS_PRIORISATION
    )
    proposition1 = proposition_factory(text="prop1", scrutin=scrutin)
    proposition2 = proposition_factory(text="prop2", scrutin=scrutin)
    vote = vote_priorisation_factory(
        scrutin=scrutin,
        utilisateur=utilisateur_syndicat,
    )
    vote_proposition_factory(proposition=proposition1, vote=vote, score=1)
    vote_proposition_factory(proposition=proposition2, vote=vote, score=2)
    scrutin.ouvert = False
    scrutin.save()
    assert scrutin.resultat == "prop2"
    response = client_admin.post(
        reverse(
            "vote:vote-priorisation-modification",
            kwargs={"scrutin": scrutin.pk, "pk": vote.pk},
        ),
        {
            f"proposition-{proposition1.id}": 2,
            f"proposition-{proposition2.id}": 1,
            "abstention": False,
            "nppv": False,
            "scrutin": scrutin.pk,
            "utilisateur": utilisateur_syndicat.pk,
        },
    )
    assert response.status_code == 302
    scrutin.ouvert = False
    scrutin.save()
    scrutin.refresh_from_db()
    assert scrutin.resultat == "prop1"


def test_post_update_vote_scrutin_priorisation_ok_pour_syndicat(
    client_syndicat,
    scrutin_factory,
    vote_priorisation_factory,
    vote_proposition_factory,
    proposition_factory,
    utilisateur_syndicat,
):
    scrutin = scrutin_factory(
        ouvert=True, type_scrutin=Scrutin.MAJORITE_SYNDICATS_PRIORISATION
    )
    proposition1 = proposition_factory(text="prop1", scrutin=scrutin)
    proposition2 = proposition_factory(text="prop2", scrutin=scrutin)
    vote = vote_priorisation_factory(
        scrutin=scrutin,
        utilisateur=utilisateur_syndicat,
    )
    vote_proposition_factory(proposition=proposition1, vote=vote, score=1)
    vote_proposition_factory(proposition=proposition2, vote=vote, score=2)
    response = client_syndicat.post(
        reverse(
            "vote:vote-priorisation-modification",
            kwargs={"scrutin": scrutin.pk, "pk": vote.pk},
        ),
        {
            f"proposition-{proposition1.id}": 2,
            f"proposition-{proposition2.id}": 1,
            "abstention": False,
            "nppv": False,
            "scrutin": scrutin.pk,
            "utilisateur": utilisateur_syndicat.pk,
        },
    )
    assert response.status_code == 302
