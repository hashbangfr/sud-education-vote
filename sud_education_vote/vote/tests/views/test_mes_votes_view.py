import pytest
from django.urls import reverse

from sud_education_vote.vote.models import Scrutin

pytestmark = pytest.mark.django_db


def test_mes_votes_redirection_si_non_connecte(client, scrutin_factory):
    url_page_vote = reverse("vote:mes-votes")
    response = client.get(url_page_vote)
    assert response.status_code == 302
    assert response.url == f"{reverse('connexion')}?next={url_page_vote}"


def test_mes_votes_pas_de_permission_superadmin(client_superadmin):
    response = client_superadmin.get(reverse("vote:mes-votes"))
    assert response.status_code == 403


def test_mes_votes_pas_de_permission_pour_admin(client_admin):
    response = client_admin.get(reverse("vote:mes-votes"))
    assert response.status_code == 403


def test_mes_votes_ok_pour_syndicat_pas_de_scrutin_ouvert(
    client_syndicat, scrutin_factory, vote_factory, utilisateur_syndicat
):
    scrutin = scrutin_factory(ouvert=False, type_scrutin=Scrutin.MAJORITE)
    vote = vote_factory(utilisateur=utilisateur_syndicat, scrutin=scrutin)

    scrutin_delegue = scrutin_factory(ouvert=False, type_scrutin=Scrutin.DELEGUE)

    response = client_syndicat.get(reverse("vote:mes-votes"))
    assert response.status_code == 200
    assert response.context.get("votes_syndicat") == [
        {
            "scrutin": scrutin_delegue,
        },
        {"scrutin": scrutin, "vote": vote},
    ]


def test_mes_votes_ok_pour_syndicat_tout_type_scrutin(
    client_syndicat,
    scrutin_factory,
    vote_factory,
    utilisateur_syndicat,
    proposition_factory,
    vote_opposition_factory,
    vote_proposition_factory,
    vote_priorisation_factory,
):
    scrutin = scrutin_factory(ouvert=False, type_scrutin=Scrutin.MAJORITE)
    vote = vote_factory(utilisateur=utilisateur_syndicat, scrutin=scrutin)

    scrutin_opposition = scrutin_factory(
        ouvert=False, type_scrutin=Scrutin.MAJORITE_SYNDICATS_OPPOSITION
    )
    proposition1 = proposition_factory(text="prop1", scrutin=scrutin_opposition)
    vote_opposition = vote_opposition_factory(
        scrutin=scrutin_opposition,
        selected_proposition=proposition1,
        utilisateur=utilisateur_syndicat,
    )

    scrutin_priorisation = scrutin_factory(
        ouvert=False, type_scrutin=Scrutin.MAJORITE_SYNDICATS_PRIORISATION
    )
    proposition2 = proposition_factory(text="prop2", scrutin=scrutin_priorisation)
    proposition3 = proposition_factory(text="prop3", scrutin=scrutin_priorisation)
    vote_priorisation = vote_priorisation_factory(
        scrutin=scrutin_priorisation,
        utilisateur=utilisateur_syndicat,
    )
    vote_proposition_factory(proposition=proposition2, vote=vote_priorisation, score=1)
    vote_proposition_factory(proposition=proposition3, vote=vote_priorisation, score=2)

    response = client_syndicat.get(reverse("vote:mes-votes"))

    assert response.status_code == 200
    assert response.context.get("votes_syndicat") == [
        {"scrutin": scrutin_priorisation, "vote": vote_priorisation},
        {"scrutin": scrutin_opposition, "vote": vote_opposition},
        {"scrutin": scrutin, "vote": vote},
    ]
