import pytest
from django.urls import reverse

from sud_education_vote.log.models import Log
from sud_education_vote.vote.forms import (
    VoteCreationForm,
    VoteMajoriteSyndicatsCreationForm,
)
from sud_education_vote.vote.models import Scrutin, Vote

pytestmark = pytest.mark.django_db


def test_page_vote_redirection_si_non_connecte(client, scrutin_factory):
    scrutin_factory(ouvert=True, type_scrutin=Scrutin.MAJORITE)
    url_page_vote = reverse("vote:page-vote")
    response = client.get(url_page_vote)
    assert response.status_code == 302
    assert response.url == f"{reverse('connexion')}?next={url_page_vote}"


def test_page_vote_scrutin_pas_de_permission_superadmin(client_superadmin):
    response = client_superadmin.get(reverse("vote:page-vote"))
    assert response.status_code == 403


def test_page_vote_scrutin_ok_pour_syndicat_scrutin_majorite(
    client_syndicat, scrutin_factory
):
    scrutin_factory(type_scrutin=Scrutin.MAJORITE_SYNDICATS)
    response = client_syndicat.get(reverse("vote:page-vote"))
    assert response.status_code == 302
    assert response.url == reverse("vote:page-vote-simple")


def test_page_vote_scrutin_ok_pour_syndicat_scrutin_majorite_opposition(
    client_syndicat, scrutin_factory
):
    scrutin_factory(type_scrutin=Scrutin.MAJORITE_SYNDICATS_OPPOSITION)
    response = client_syndicat.get(reverse("vote:page-vote"))
    assert response.status_code == 302
    assert response.url == reverse("vote:page-vote-opposition")


def test_page_vote_scrutin_ok_pour_syndicat_scrutin_majorite_priorisation(
    client_syndicat, scrutin_factory
):
    scrutin_factory(type_scrutin=Scrutin.MAJORITE_SYNDICATS_PRIORISATION)
    response = client_syndicat.get(reverse("vote:page-vote"))
    assert response.status_code == 302
    assert response.url == reverse("vote:page-vote-priorisation")


def test_page_vote_scrutin_ok_pour_syndicat_pas_de_scrutin_ouvert(
    client_syndicat, scrutin_factory
):
    scrutin_factory(ouvert=False, type_scrutin=Scrutin.MAJORITE)
    response = client_syndicat.get(reverse("vote:page-vote-simple"))
    assert response.status_code == 200
    assert response.context.get("scrutin") is None


def test_page_vote_scrutin_ok_pour_syndicat_scrutin_ouvert_deja_vote(
    client_syndicat, utilisateur_syndicat, scrutin_factory, vote_factory
):
    scrutin = scrutin_factory(ouvert=True, type_scrutin=Scrutin.MAJORITE)
    vote_factory(utilisateur=utilisateur_syndicat, scrutin=scrutin)
    response = client_syndicat.get(reverse("vote:page-vote-simple"))
    assert response.status_code == 200
    assert response.context.get("scrutin")
    assert response.context.get("deja_vote")


def test_page_vote_scrutin_ok_pour_syndicat_scrutin_ouvert(
    client_syndicat, utilisateur_syndicat, scrutin_factory
):
    scrutin_factory(ouvert=True, type_scrutin=Scrutin.MAJORITE)
    response = client_syndicat.get(reverse("vote:page-vote-simple"))
    assert response.status_code == 200
    assert response.context.get("scrutin")
    assert not response.context.get("deja_vote")
    assert isinstance(response.context.get("form"), VoteCreationForm)


def test_page_vote_scrutin_ok_pour_syndicat_scrutin_maj_syndicats_ouvert(
    client_syndicat, utilisateur_syndicat, scrutin_factory
):
    scrutin_factory(ouvert=True, type_scrutin=Scrutin.MAJORITE_SYNDICATS)
    response = client_syndicat.get(reverse("vote:page-vote-simple"))
    assert response.status_code == 200
    assert response.context.get("scrutin")
    assert response.context.get("majorite_syndicats")
    assert not response.context.get("deja_vote")
    assert isinstance(response.context.get("form"), VoteMajoriteSyndicatsCreationForm)


def test_vote_page_vote_scrutin_ok_pour_syndicat_scrutin_ouvert(
    client_syndicat, utilisateur_syndicat, scrutin_factory
):
    scrutin = scrutin_factory(ouvert=True, type_scrutin=Scrutin.MAJORITE)
    assert Vote.objects.count() == 0
    response = client_syndicat.post(
        reverse("vote:page-vote-simple"),
        {
            "vote_utilisateur": Vote.POUR,
            "mandats_pour": 30,
            "mandats_contre": 4,
            "mandats_abstention": 4,
            "mandats_nppv": 2,
            "scrutin": scrutin.pk,
            "utilisateur": utilisateur_syndicat.pk,
        },
    )
    assert response.status_code == 302
    assert response.url == reverse("vote:page-vote")
    assert Vote.objects.count() == 1
    vote = Vote.objects.last()
    assert vote.vote_utilisateur == Vote.POUR
    assert vote.mandats_pour == 30
    assert vote.mandats_contre == 4
    assert vote.mandats_abstention == 4
    assert vote.mandats_nppv == 2
    assert vote.utilisateur == utilisateur_syndicat
    assert vote.scrutin == scrutin
    assert Log.objects.last().log == (
        f"Le syndicat {utilisateur_syndicat} a rentré les votes suivants "
        f"pour le scrutin {scrutin} : "
        f"{vote.mandats_pour} votes 'Pour', "
        f"{vote.mandats_contre} votes 'Contre', "
        f"{vote.mandats_abstention} votes 'Abstention', "
        f"et {vote.mandats_nppv} votes 'Ne se prononce pas'."
    )


def test_vote_page_vote_scrutin_erreur_trop_de_mandat(
    client_syndicat, utilisateur_syndicat, scrutin_factory
):
    scrutin = scrutin_factory(ouvert=True, type_scrutin=Scrutin.MAJORITE)
    assert Vote.objects.count() == 0
    response = client_syndicat.post(
        reverse("vote:page-vote-simple"),
        {
            "vote_utilisateur": Vote.POUR,
            "mandats_pour": 31,
            "mandats_contre": 4,
            "mandats_abstention": 4,
            "mandats_nppv": 2,
            "scrutin": scrutin.pk,
            "utilisateur": utilisateur_syndicat.pk,
        },
    )
    assert response.status_code == 200
    assert response.context.get("form").errors == {
        "__all__": ["Vous ne pouvez attribuer plus de mandats que vous n'en possedez"]
    }


def test_vote_page_vote_scrutin_erreur_pas_assez_de_mandat(
    client_syndicat, utilisateur_syndicat, scrutin_factory
):
    scrutin = scrutin_factory(ouvert=True, type_scrutin=Scrutin.MAJORITE)
    assert Vote.objects.count() == 0
    response = client_syndicat.post(
        reverse("vote:page-vote-simple"),
        {
            "vote_utilisateur": Vote.POUR,
            "mandats_pour": 29,
            "mandats_contre": 4,
            "mandats_abstention": 4,
            "mandats_nppv": 2,
            "scrutin": scrutin.pk,
            "utilisateur": utilisateur_syndicat.pk,
        },
    )
    assert response.status_code == 200
    assert response.context.get("form").errors == {
        "__all__": ["Vous devez attribuer l'ensemble de vos mandats"]
    }


def test_vote_page_vote_scrutin_erreur_deja_vote(
    client_syndicat, utilisateur_syndicat, scrutin_factory, vote_factory
):
    scrutin = scrutin_factory(ouvert=True, type_scrutin=Scrutin.MAJORITE)
    vote_factory(
        scrutin=scrutin,
        utilisateur=utilisateur_syndicat,
        vote_utilisateur=Vote.POUR,
        mandats_pour=30,
        mandats_contre=4,
        mandats_abstention=4,
        mandats_nppv=2,
    )
    assert Vote.objects.count() == 1
    response = client_syndicat.post(
        reverse("vote:page-vote-simple"),
        {
            "vote_utilisateur": Vote.POUR,
            "mandats_pour": 30,
            "mandats_contre": 4,
            "mandats_abstention": 4,
            "mandats_nppv": 2,
            "scrutin": scrutin.pk,
            "utilisateur": utilisateur_syndicat.pk,
        },
    )
    assert Vote.objects.count() == 1
    assert response.status_code == 200
    messages = response.context.get("messages")
    assert messages
    assert len(messages) == 1
    assert str(messages._loaded_data[0]) == "Vous avez déjà voté pour ce scrutin"


def test_vote_page_vote_scrutin_erreur_scrutin_ferme(
    client_syndicat, utilisateur_syndicat, scrutin_factory
):
    scrutin = scrutin_factory(ouvert=False, type_scrutin=Scrutin.MAJORITE)
    assert Vote.objects.count() == 0
    response = client_syndicat.post(
        reverse("vote:page-vote-simple"),
        {
            "vote_utilisateur": Vote.POUR,
            "mandats_pour": 30,
            "mandats_contre": 4,
            "mandats_abstention": 4,
            "mandats_nppv": 2,
            "scrutin": scrutin.pk,
            "utilisateur": utilisateur_syndicat.pk,
        },
    )
    assert Vote.objects.count() == 0
    assert response.status_code == 200
    messages = response.context.get("messages")
    assert messages
    assert len(messages) == 1
    assert (
        str(messages._loaded_data[0])
        == "Ce scrutin est fermé, votre vote n'a pas été pris en compte"
    )


def test_vote_page_vote_scrutin_majorite_syndicats_ok(
    client_syndicat, utilisateur_syndicat, scrutin_factory
):
    scrutin = scrutin_factory(ouvert=True, type_scrutin=Scrutin.MAJORITE_SYNDICATS)
    assert Vote.objects.count() == 0
    response = client_syndicat.post(
        reverse("vote:page-vote-simple"),
        {
            "vote_utilisateur": Vote.POUR,
            "scrutin": scrutin.pk,
            "utilisateur": utilisateur_syndicat.pk,
        },
    )
    assert response.status_code == 302
    assert response.url == reverse("vote:page-vote")
    assert Vote.objects.count() == 1
    vote = Vote.objects.last()
    assert vote.vote_utilisateur == Vote.POUR
    assert vote.mandats_pour == 0
    assert vote.mandats_contre == 0
    assert vote.mandats_abstention == 0
    assert vote.mandats_nppv == 0
    assert vote.utilisateur == utilisateur_syndicat
    assert vote.scrutin == scrutin
    assert Log.objects.last().log == (
        f"Le syndicat {utilisateur_syndicat} a rentré le vote suivant "
        f"pour le scrutin {scrutin} : "
        f"{vote.vote_utilisateur}"
    )


def test_vote_page_vote_scrutin_majorite_syndicats_erreur_deja_vote(
    client_syndicat, utilisateur_syndicat, scrutin_factory, vote_factory
):
    scrutin = scrutin_factory(ouvert=True, type_scrutin=Scrutin.MAJORITE_SYNDICATS)
    vote_factory(
        scrutin=scrutin,
        utilisateur=utilisateur_syndicat,
        vote_utilisateur=Vote.POUR,
    )
    assert Vote.objects.count() == 1
    response = client_syndicat.post(
        reverse("vote:page-vote-simple"),
        {
            "vote_utilisateur": Vote.POUR,
            "scrutin": scrutin.pk,
            "utilisateur": utilisateur_syndicat.pk,
        },
    )
    assert Vote.objects.count() == 1
    assert response.status_code == 200
    messages = response.context.get("messages")
    assert messages
    assert len(messages) == 1
    assert str(messages._loaded_data[0]) == "Vous avez déjà voté pour ce scrutin"


def test_vote_page_vote_scrutin_majorite_syndicats_erreur_scrutin_ferme(
    client_syndicat, utilisateur_syndicat, scrutin_factory
):
    scrutin = scrutin_factory(ouvert=False, type_scrutin=Scrutin.MAJORITE_SYNDICATS)
    assert Vote.objects.count() == 0
    response = client_syndicat.post(
        reverse("vote:page-vote-simple"),
        {
            "vote_utilisateur": Vote.POUR,
            "scrutin": scrutin.pk,
            "utilisateur": utilisateur_syndicat.pk,
        },
    )
    assert Vote.objects.count() == 0
    assert response.status_code == 200
