import pytest
from django.urls import reverse

from sud_education_vote.vote.models import Scrutin, Vote

pytestmark = pytest.mark.django_db


def test_export_txt_mes_votes_redirection_si_non_connecte(client):
    url_export = reverse("vote:mes-votes-export")
    response = client.get(url_export)
    assert response.status_code == 302
    assert response.url == f"{reverse('connexion')}?next={url_export}"


def test_export_txt_mes_votes_ok_superadmin(client_superadmin):
    response = client_superadmin.get(reverse("vote:mes-votes-export"))
    assert response.status_code == 403


def test_export_txt_mes_votes_ok_syndicat(
    client_syndicat,
    scrutin_factory,
    vote_factory,
    utilisateur_syndicat,
    proposition_factory,
    vote_opposition_factory,
    vote_proposition_factory,
    vote_priorisation_factory,
):
    scrutin = scrutin_factory(type_scrutin=Scrutin.MAJORITE)
    vote = vote_factory(
        scrutin=scrutin,
        vote_utilisateur=Vote.POUR,
        utilisateur=utilisateur_syndicat,
        mandats_pour=60,
        mandats_contre=30,
        mandats_abstention=10,
        mandats_nppv=5,
    )
    scrutin_opposition = scrutin_factory(
        ouvert=False, type_scrutin=Scrutin.MAJORITE_SYNDICATS_OPPOSITION
    )
    proposition1 = proposition_factory(text="prop1", scrutin=scrutin_opposition)
    vote_opposition = vote_opposition_factory(
        scrutin=scrutin_opposition,
        selected_proposition=proposition1,
        utilisateur=utilisateur_syndicat,
    )

    scrutin_priorisation = scrutin_factory(
        ouvert=False, type_scrutin=Scrutin.MAJORITE_SYNDICATS_PRIORISATION
    )
    proposition2 = proposition_factory(text="prop2", scrutin=scrutin_priorisation)
    proposition3 = proposition_factory(text="prop3", scrutin=scrutin_priorisation)
    vote_priorisation = vote_priorisation_factory(
        scrutin=scrutin_priorisation,
        utilisateur=utilisateur_syndicat,
    )
    vote_proposition_factory(proposition=proposition2, vote=vote_priorisation, score=1)
    vote_proposition_factory(proposition=proposition3, vote=vote_priorisation, score=2)
    response = client_syndicat.get(reverse("vote:mes-votes-export"))
    assert response.status_code == 200
    assert response.content.decode("UTF-8") == (
        f"{scrutin_priorisation.intitule}, {scrutin_priorisation.type_scrutin_display},"
        f" {scrutin_priorisation.resultat}, "
        f"vote syndicat: {vote_priorisation.resultat}\n"
        f"{scrutin_opposition.intitule}, {scrutin_opposition.type_scrutin_display},"
        f" {scrutin_opposition.resultat}, vote syndicat: {vote_opposition.resultat}\n"
        f"{scrutin.intitule}, {scrutin.type_scrutin_display}, {scrutin.resultat}, "
        f"vote syndicat: {vote.vote_utilisateur}, mandats pour: {vote.mandats_pour}, "
        f"mandats contre: {vote.mandats_contre}, "
        f"mandats abstention: {vote.mandats_abstention}, "
        f"mandats nppv: {vote.mandats_nppv}\n"
    )


def test_export_txt_mes_votes_ok_admin(client_admin):
    response = client_admin.get(reverse("vote:mes-votes-export"))
    assert response.status_code == 403
