import pytest
from django.urls import reverse

from sud_education_vote.log.models import Log
from sud_education_vote.vote.models import Scrutin

pytestmark = pytest.mark.django_db


def test_fermeture_scrutin_redirection_si_non_connecte(client, scrutin):
    url_admin_supression = reverse("vote:scrutin-fermeture", kwargs={"pk": scrutin.pk})
    response = client.get(url_admin_supression)
    assert response.status_code == 302
    assert response.url == f"{reverse('connexion')}?next={url_admin_supression}"


def test_fermeture_scrutin_pas_de_permission_syndicat(client_syndicat, scrutin):
    response = client_syndicat.get(
        reverse("vote:scrutin-fermeture", kwargs={"pk": scrutin.pk})
    )
    assert response.status_code == 403


def test_fermeture_scrutin_pas_de_permission_superadmin(client_superadmin, scrutin):
    response = client_superadmin.get(
        reverse("vote:scrutin-fermeture", kwargs={"pk": scrutin.pk})
    )
    assert response.status_code == 403


def test_fermeture_scrutin_succes(client_admin, utilisateur_admin, scrutin):
    scrutin.type_scrutin = Scrutin.MAJORITE
    scrutin.save()
    assert Scrutin.objects.filter(ouvert=True).exists()
    response = client_admin.post(
        reverse("vote:scrutin-fermeture", kwargs={"pk": scrutin.pk})
    )
    assert response.status_code == 302
    assert response.url == reverse("vote:scrutins")
    assert Log.objects.filter(
        log=f"L'admin {utilisateur_admin} a fermé le scrutin {scrutin}"
    ).exists()
    assert Log.objects.filter(
        log=f"Le scrutin {scrutin} voit sa résolution "
        f"{'adoptée' if scrutin.adoption else 'rejetée'}"
    ).exists()


def test_fermeture_scrutin_opposition_succes(
    client_admin, utilisateur_admin, proposition_factory, scrutin
):
    scrutin.type_scrutin = Scrutin.MAJORITE_SYNDICATS_OPPOSITION
    proposition_factory(scrutin=scrutin, text="prop1")
    scrutin.save()
    assert Scrutin.objects.filter(ouvert=True).exists()
    response = client_admin.post(
        reverse("vote:scrutin-fermeture", kwargs={"pk": scrutin.pk})
    )
    assert response.status_code == 302
    assert response.url == reverse("vote:scrutins")
    assert Log.objects.filter(
        log=f"L'admin {utilisateur_admin} a fermé le scrutin {scrutin}"
    ).exists()
    assert Log.objects.filter(
        log=f"Le scrutin {scrutin} obtient le " f"résultat suivant : Pas de votes"
    ).exists()


def test_fermeture_scrutin_majorite_opposition_succes(
    client_admin, utilisateur_admin, scrutin_factory
):
    scrutin = scrutin_factory(
        ouvert=True, type_scrutin=Scrutin.MAJORITE_SYNDICATS_OPPOSITION
    )
    response = client_admin.post(
        reverse("vote:scrutin-fermeture", kwargs={"pk": scrutin.pk})
    )
    assert response.status_code == 302
    assert response.url == reverse("vote:scrutins")
    assert Log.objects.filter(
        log=f"L'admin {utilisateur_admin} a fermé le scrutin {scrutin}"
    ).exists()
