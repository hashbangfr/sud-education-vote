import pytest
from django.urls import reverse

from sud_education_vote.log.models import Log
from sud_education_vote.vote.models import (
    Scrutin,
    Vote,
    VoteOpposition,
    VotePriorisation,
    VoteProposition,
)

pytestmark = pytest.mark.django_db


def test_reset_not_ok_admin(client_admin):
    response = client_admin.get(reverse("vote:reinitialisation"))
    assert response.status_code == 403


def test_reset_not_ok_syndicat(client_syndicat):
    response = client_syndicat.get(reverse("vote:reinitialisation"))
    assert response.status_code == 403


def test_get_page_reset_ok_superadmin(
    client_superadmin,
):
    response = client_superadmin.get(reverse("vote:reinitialisation"))
    assert response.status_code == 200


def test_post_page_reset_archive_ok_superadmin(
    client_superadmin,
    scrutin_factory,
    vote_factory,
    vote_opposition_factory,
    vote_priorisation_factory,
    log_factory,
):
    scrutin_factory(
        type_scrutin=Scrutin.MAJORITE,
    )
    scrutin_factory(
        type_scrutin=Scrutin.MAJORITE_SYNDICATS,
    )
    scrutin_factory(
        type_scrutin=Scrutin.MAJORITE_SYNDICATS_OPPOSITION,
    )
    scrutin_factory(
        type_scrutin=Scrutin.MAJORITE_SYNDICATS_PRIORISATION,
    )
    vote_factory()
    vote_opposition_factory()
    vote_priorisation_factory()
    log_factory()
    response = client_superadmin.post(
        reverse("vote:reinitialisation"),
        {"archive_souhaitee": True},
    )
    assert response.status_code == 200
    assert Scrutin.objects.all().count() == 0
    assert Vote.objects.all().count() == 0
    assert VoteOpposition.objects.all().count() == 0
    assert VotePriorisation.objects.all().count() == 0
    assert VoteProposition.objects.all().count() == 0
    assert Log.objects.all().count() == 0


def test_post_page_reset_sans_archive_ok_superadmin(
    client_superadmin,
    scrutin_factory,
    vote_factory,
    vote_opposition_factory,
    vote_priorisation_factory,
    proposition_factory,
    vote_proposition_factory,
    log_factory,
):
    assert Scrutin.objects.all().count() == 0
    scrutin_factory(
        type_scrutin=Scrutin.MAJORITE,
    )
    scrutin_factory(
        type_scrutin=Scrutin.MAJORITE_SYNDICATS,
    )
    scrutin_opposition = scrutin_factory(
        type_scrutin=Scrutin.MAJORITE_SYNDICATS_OPPOSITION,
    )
    proposition_factory(scrutin=scrutin_opposition)
    scrutin_factory(
        type_scrutin=Scrutin.MAJORITE_SYNDICATS_PRIORISATION,
    )
    vote_factory()
    vote_opposition_factory()
    vote_priorisation = vote_priorisation_factory()
    vote_proposition_factory(vote=vote_priorisation)
    log_factory()
    response = client_superadmin.post(
        reverse("vote:reinitialisation"),
        {"archive_souhaitee": False},
    )
    assert response.status_code == 302
    assert Scrutin.objects.all().count() == 0
    assert Vote.objects.all().count() == 0
    assert VoteOpposition.objects.all().count() == 0
    assert VotePriorisation.objects.all().count() == 0
    assert VoteProposition.objects.all().count() == 0
    assert Log.objects.all().count() == 0
