import pytest
from django.urls import reverse

from sud_education_vote.vote.models import Scrutin

pytestmark = pytest.mark.django_db


def test_detail_scrutins_redirection_si_non_connecte(client, scrutin):
    url_scrutin = reverse("vote:scrutin-detail", kwargs={"pk": scrutin.pk})
    response = client.get(url_scrutin)
    assert response.status_code == 302
    assert response.url == f"{reverse('connexion')}?next={url_scrutin}"


def test_detail_scrutins_ok_syndicat(client_syndicat, scrutin):
    response = client_syndicat.get(
        reverse("vote:scrutin-detail", kwargs={"pk": scrutin.pk})
    )
    assert response.status_code == 200


def test_detail_scrutins_pas_de_permission_superadmin(client_superadmin, scrutin):
    response = client_superadmin.get(
        reverse("vote:scrutin-detail", kwargs={"pk": scrutin.pk})
    )
    assert response.status_code == 403


def test_detail_scrutins_ok_admin(client_admin, scrutin):
    scrutin.type_scrutin = Scrutin.MAJORITE
    scrutin.save()
    response = client_admin.get(
        reverse("vote:scrutin-detail", kwargs={"pk": scrutin.pk})
    )
    assert response.status_code == 200


def test_detail_scrutin_majorite_syndicat_ok_admin(client_admin, scrutin):
    scrutin.type_scrutin = Scrutin.MAJORITE_SYNDICATS
    scrutin.save()
    response = client_admin.get(
        reverse("vote:scrutin-detail", kwargs={"pk": scrutin.pk})
    )
    assert response.status_code == 200
    assert response.context.get("majorite_syndicats")


def test_detail_scrutin_opposition_ok_admin(client_admin, scrutin):
    scrutin.type_scrutin = Scrutin.MAJORITE_SYNDICATS_OPPOSITION
    scrutin.save()
    response = client_admin.get(
        reverse("vote:scrutin-detail", kwargs={"pk": scrutin.pk})
    )
    assert response.status_code == 200


def test_detail_scrutin_priorisation_ok_admin(client_admin, scrutin):
    scrutin.type_scrutin = Scrutin.MAJORITE_SYNDICATS_PRIORISATION
    scrutin.save()
    response = client_admin.get(
        reverse("vote:scrutin-detail", kwargs={"pk": scrutin.pk})
    )
    assert response.status_code == 200
