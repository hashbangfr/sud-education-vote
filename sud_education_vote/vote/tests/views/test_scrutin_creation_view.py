import pytest
from django.contrib.messages import get_messages
from django.urls import reverse
from django.utils.translation import gettext_lazy as _

from sud_education_vote.log.models import Log
from sud_education_vote.vote.forms import ScrutinCreationForm
from sud_education_vote.vote.models import Proposition, Scrutin

pytestmark = pytest.mark.django_db


def test_creation_scrutin_redirection_si_non_connecte(client):
    url_scrutin_creation = reverse("vote:scrutin-creation")
    response = client.get(url_scrutin_creation)
    assert response.status_code == 302
    assert response.url == f"{reverse('connexion')}?next={url_scrutin_creation}"


def test_creation_scrutin_pas_de_permission_syndicat(client_syndicat):
    response = client_syndicat.get(reverse("vote:scrutin-creation"))
    assert response.status_code == 403


def test_creation_scrutin_pas_de_permission_superadmin(client_superadmin):
    response = client_superadmin.get(reverse("vote:scrutin-creation"))
    assert response.status_code == 403


def test_creation_scrutin_ok_pour_admin(client_admin):
    response = client_admin.get(reverse("vote:scrutin-creation"))
    assert response.status_code == 200
    assert isinstance(response.context["form"], ScrutinCreationForm)


def test_creation_scrutin_admin_deja_un_scrutin_ouvert(client_admin, scrutin):
    response = client_admin.get(reverse("vote:scrutin-creation"))
    assert response.status_code == 302
    assert response.url == reverse("vote:scrutins")
    messages = [m.message for m in get_messages(response.wsgi_request)]
    assert _("Vous ne pouvez ouvrir plus d'un scrutin à la fois!") in messages


def test_creation_scrutin_succes(client_admin, utilisateur_admin):
    assert Scrutin.objects.count() == 0
    response = client_admin.post(
        reverse("vote:scrutin-creation"),
        data={
            "intitule": "Scrutin",
            "type_scrutin": Scrutin.MAJORITE,
        },
    )
    assert response.status_code == 302
    assert response.url == reverse("vote:scrutins")
    assert Scrutin.objects.count() == 1
    scrutin = Scrutin.objects.last()
    assert scrutin.intitule == "Scrutin"
    assert scrutin.type_scrutin == Scrutin.MAJORITE
    assert scrutin.ouvert
    assert scrutin.createur == utilisateur_admin
    assert Log.objects.last().log == (
        f"L'admin {utilisateur_admin} a créé le scrutin {scrutin} "
        f"de type Majorité des syndicats et mandats"
    )


def test_creation_scrutin_delegue_succes(client_admin, utilisateur_admin):
    assert Scrutin.objects.count() == 0
    response = client_admin.post(
        reverse("vote:scrutin-creation"),
        data={
            "intitule": "Scrutin",
            "type_scrutin": Scrutin.DELEGUE,
        },
    )
    assert response.status_code == 302
    assert Scrutin.objects.count() == 1
    scrutin = Scrutin.objects.last()
    assert response.url == reverse("vote:scrutin-delegue", kwargs={"pk": scrutin.pk})

    assert scrutin.intitule == "Scrutin"
    assert scrutin.type_scrutin == Scrutin.DELEGUE
    assert scrutin.ouvert
    assert scrutin.createur == utilisateur_admin
    assert Log.objects.last().log == (
        f"L'admin {utilisateur_admin} a créé le scrutin {scrutin} "
        f"de type Majorité simple des délégué⋅es"
    )


def test_creation_scrutin_majorite_syndicats_succes(client_admin, utilisateur_admin):
    assert Scrutin.objects.count() == 0
    response = client_admin.post(
        reverse("vote:scrutin-creation"),
        data={
            "intitule": "Scrutin",
            "type_scrutin": Scrutin.MAJORITE_SYNDICATS,
        },
    )
    assert response.status_code == 302
    assert response.url == reverse("vote:scrutins")
    assert Scrutin.objects.count() == 1
    scrutin = Scrutin.objects.last()
    assert scrutin.intitule == "Scrutin"
    assert scrutin.type_scrutin == Scrutin.MAJORITE_SYNDICATS
    assert scrutin.ouvert
    assert scrutin.createur == utilisateur_admin
    assert Log.objects.last().log == (
        f"L'admin {utilisateur_admin} a créé le scrutin {scrutin} "
        f"de type Majorité simple des syndicats"
    )


def test_creation_scrutin_majorite_syndicats_priorisation_succes(
    client_admin, utilisateur_admin
):
    assert Scrutin.objects.count() == 0
    response = client_admin.post(
        reverse("vote:scrutin-creation"),
        data={
            "intitule": "Scrutin",
            "type_scrutin": Scrutin.MAJORITE_SYNDICATS_PRIORISATION,
        },
    )
    assert response.status_code == 302
    assert Scrutin.objects.count() == 1
    scrutin = Scrutin.objects.last()
    assert response.url == reverse(
        "vote:scrutin-propositions", kwargs={"pk": scrutin.pk}
    )
    assert scrutin.intitule == "Scrutin"
    assert scrutin.type_scrutin == Scrutin.MAJORITE_SYNDICATS_PRIORISATION
    assert scrutin.ouvert
    assert scrutin.createur == utilisateur_admin
    response = client_admin.post(
        reverse("vote:scrutin-propositions", kwargs={"pk": scrutin.pk}),
        data={"proposition": ["proposition1", "proposition2"]},
    )
    assert response.status_code == 302
    assert response.url == reverse("vote:scrutins")
    assert Proposition.objects.count() == 2
    assert Log.objects.last().log == (
        f"L'admin {utilisateur_admin} a créé le scrutin {scrutin} "
        f"de type Majorité des syndicats - Priorisation"
    )


def test_creation_scrutin_majorite_syndicats_opposition_succes(
    client_admin, utilisateur_admin
):
    assert Scrutin.objects.count() == 0
    response = client_admin.post(
        reverse("vote:scrutin-creation"),
        data={
            "intitule": "Scrutin",
            "type_scrutin": Scrutin.MAJORITE_SYNDICATS_OPPOSITION,
        },
    )
    assert response.status_code == 302
    assert Scrutin.objects.count() == 1
    scrutin = Scrutin.objects.last()
    assert response.url == reverse(
        "vote:scrutin-propositions", kwargs={"pk": scrutin.pk}
    )
    assert scrutin.intitule == "Scrutin"
    assert scrutin.type_scrutin == Scrutin.MAJORITE_SYNDICATS_OPPOSITION
    assert scrutin.ouvert
    assert scrutin.createur == utilisateur_admin
    response = client_admin.post(
        reverse("vote:scrutin-propositions", kwargs={"pk": scrutin.pk}),
        data={"proposition": ["proposition1", "proposition2"]},
    )
    assert response.status_code == 302
    assert response.url == reverse("vote:scrutins")
    assert Proposition.objects.count() == 2
    assert Log.objects.last().log == (
        f"L'admin {utilisateur_admin} a créé le scrutin {scrutin} "
        f"de type Majorité des syndicats - Opposition"
    )
