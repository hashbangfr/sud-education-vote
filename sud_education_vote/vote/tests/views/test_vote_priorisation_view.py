import pytest
from django.urls import reverse

from sud_education_vote.log.models import Log
from sud_education_vote.vote.models import Scrutin, VotePriorisation, VoteProposition

pytestmark = pytest.mark.django_db


def test_vote_page_vote_scrutin_priorisation_ok(
    client_syndicat, utilisateur_syndicat, scrutin_factory, proposition_factory
):
    scrutin = scrutin_factory(
        ouvert=True, type_scrutin=Scrutin.MAJORITE_SYNDICATS_PRIORISATION
    )
    proposition1 = proposition_factory(scrutin=scrutin, text="prop1")
    proposition2 = proposition_factory(scrutin=scrutin, text="prop2")
    assert VotePriorisation.objects.count() == 0
    assert VoteProposition.objects.count() == 0
    response = client_syndicat.post(
        reverse("vote:page-vote-priorisation"),
        {
            f"proposition-{proposition1.id}": 1,
            f"proposition-{proposition2.id}": 2,
            "abstention": False,
            "nppv": False,
            "scrutin": scrutin.pk,
            "utilisateur": utilisateur_syndicat.pk,
        },
    )
    assert response.status_code == 302
    assert response.url == reverse("vote:page-vote")
    assert VotePriorisation.objects.count() == 1
    vote = VotePriorisation.objects.last()
    assert vote.utilisateur == utilisateur_syndicat
    assert vote.scrutin == scrutin
    assert VoteProposition.objects.count() == 2
    assert Log.objects.last().log == (
        f"Le syndicat {utilisateur_syndicat} a rentré le vote suivant "
        f"pour le scrutin {scrutin} : {vote.resultat}"
    )


def test_vote_page_vote_scrutin_priorisation_abstention_ok(
    client_syndicat, utilisateur_syndicat, scrutin_factory, proposition_factory
):
    scrutin = scrutin_factory(
        ouvert=True, type_scrutin=Scrutin.MAJORITE_SYNDICATS_PRIORISATION
    )
    proposition1 = proposition_factory(scrutin=scrutin, text="prop1")
    proposition2 = proposition_factory(scrutin=scrutin, text="prop2")
    assert VotePriorisation.objects.count() == 0
    assert VoteProposition.objects.count() == 0
    response = client_syndicat.post(
        reverse("vote:page-vote-priorisation"),
        {
            f"proposition-{proposition1.id}": 0,
            f"proposition-{proposition2.id}": 0,
            "abstention": True,
            "nppv": False,
            "scrutin": scrutin.pk,
            "utilisateur": utilisateur_syndicat.pk,
        },
    )
    assert response.status_code == 302
    assert response.url == reverse("vote:page-vote")
    assert VotePriorisation.objects.count() == 1
    vote = VotePriorisation.objects.last()
    assert vote.utilisateur == utilisateur_syndicat
    assert vote.scrutin == scrutin
    assert vote.abstention
    assert VoteProposition.objects.count() == 0
    assert Log.objects.last().log == (
        f"Le syndicat {utilisateur_syndicat} a rentré le vote suivant "
        f"pour le scrutin {scrutin} : {vote.resultat}"
    )


def test_vote_page_vote_scrutin_priorisation_nppv_ok(
    client_syndicat, utilisateur_syndicat, scrutin_factory, proposition_factory
):
    scrutin = scrutin_factory(
        ouvert=True, type_scrutin=Scrutin.MAJORITE_SYNDICATS_PRIORISATION
    )
    proposition1 = proposition_factory(scrutin=scrutin, text="prop1")
    proposition2 = proposition_factory(scrutin=scrutin, text="prop2")
    assert VotePriorisation.objects.count() == 0
    assert VoteProposition.objects.count() == 0
    response = client_syndicat.post(
        reverse("vote:page-vote-priorisation"),
        {
            f"proposition-{proposition1.id}": 0,
            f"proposition-{proposition2.id}": 0,
            "abstention": False,
            "nppv": True,
            "scrutin": scrutin.pk,
            "utilisateur": utilisateur_syndicat.pk,
        },
    )
    assert response.status_code == 302
    assert response.url == reverse("vote:page-vote")
    assert VotePriorisation.objects.count() == 1
    vote = VotePriorisation.objects.last()
    assert vote.utilisateur == utilisateur_syndicat
    assert vote.scrutin == scrutin
    assert vote.nppv
    assert VoteProposition.objects.count() == 0
    assert Log.objects.last().log == (
        f"Le syndicat {utilisateur_syndicat} a rentré le vote suivant "
        f"pour le scrutin {scrutin} : {vote.resultat}"
    )


def test_vote_page_vote_scrutin_priorisation_ferme(
    client_syndicat, utilisateur_syndicat, scrutin_factory, proposition_factory
):
    scrutin = scrutin_factory(
        ouvert=False, type_scrutin=Scrutin.MAJORITE_SYNDICATS_PRIORISATION
    )
    proposition1 = proposition_factory(scrutin=scrutin, text="prop1")
    proposition2 = proposition_factory(scrutin=scrutin, text="prop2")
    assert VotePriorisation.objects.count() == 0
    response = client_syndicat.post(
        reverse("vote:page-vote-priorisation"),
        {
            f"proposition-{proposition1.id}": 1,
            f"proposition-{proposition2.id}": 2,
            "abstention": False,
            "nppv": False,
            "scrutin": scrutin.pk,
            "utilisateur": utilisateur_syndicat.pk,
        },
    )
    assert VotePriorisation.objects.count() == 0
    assert response.status_code == 200
    messages = response.context.get("messages")
    assert messages
    assert len(messages) == 1
    assert (
        str(messages._loaded_data[0])
        == "Ce scrutin est fermé, votre vote n'a pas été pris en compte"
    )


def test_vote_page_vote_scrutin_priorisation_deja_vote(
    client_syndicat, utilisateur_syndicat, scrutin_factory, proposition_factory
):
    scrutin = scrutin_factory(
        ouvert=True, type_scrutin=Scrutin.MAJORITE_SYNDICATS_PRIORISATION
    )
    proposition1 = proposition_factory(scrutin=scrutin, text="prop1")
    proposition2 = proposition_factory(scrutin=scrutin, text="prop2")
    client_syndicat.post(
        reverse("vote:page-vote-priorisation"),
        {
            f"proposition-{proposition1.id}": 1,
            f"proposition-{proposition2.id}": 2,
            "abstention": False,
            "nppv": False,
            "scrutin": scrutin.pk,
            "utilisateur": utilisateur_syndicat.pk,
        },
    )
    assert VotePriorisation.objects.count() == 1
    assert VoteProposition.objects.count() == 2
    response = client_syndicat.post(
        reverse("vote:page-vote-priorisation"),
        {
            f"proposition-{proposition1.id}": 1,
            f"proposition-{proposition2.id}": 2,
            "abstention": False,
            "nppv": False,
            "scrutin": scrutin.pk,
            "utilisateur": utilisateur_syndicat.pk,
        },
    )
    assert VotePriorisation.objects.count() == 1
    assert VoteProposition.objects.count() == 2
    assert response.status_code == 200
    messages = response.context.get("messages")
    assert messages
