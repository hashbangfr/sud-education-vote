import pytest
from django.urls import reverse
from pytest_django.asserts import assertQuerysetEqual

from sud_education_vote.vote.models import Scrutin

pytestmark = pytest.mark.django_db


def test_liste_scrutins_redirection_si_non_connecte(client):
    url_scrutins = reverse("vote:scrutins")
    response = client.get(url_scrutins)
    assert response.status_code == 302
    assert response.url == f"{reverse('connexion')}?next={url_scrutins}"


def test_liste_scrutins_ok_syndicat(client_syndicat, scrutin):
    response = client_syndicat.get(reverse("vote:scrutins"))
    assert response.status_code == 200
    assert response.context.get("scrutin_actuel") is None


def test_liste_scrutins_pas_de_permission_superadmin(client_superadmin):
    response = client_superadmin.get(reverse("vote:scrutins"))
    assert response.status_code == 403


def test_liste_scrutins_ok_admin(client_admin, scrutin_factory):
    scrutin_actif = scrutin_factory(type_scrutin=Scrutin.MAJORITE)
    scrutin_1 = scrutin_factory(ouvert=False)
    scrutin_2 = scrutin_factory(ouvert=False)
    response = client_admin.get(reverse("vote:scrutins"))
    assert response.status_code == 200
    assertQuerysetEqual(
        response.context.get("object_list"), [scrutin_2, scrutin_1, scrutin_actif]
    )
    assert response.context.get("scrutin_actuel") == scrutin_actif


def test_liste_scrutins_ok_pour_admin_pas_de_scrutins(client_admin):
    response = client_admin.get(reverse("vote:scrutins"))
    assert response.status_code == 200
    assert not response.context.get("object_list")
    assert not response.context.get("scrutin_actuel")


def test_liste_scrutins_scrutin_opposition_ok_admin(client_admin, scrutin_factory):
    scrutin_factory(type_scrutin=Scrutin.MAJORITE_SYNDICATS_OPPOSITION)
    response = client_admin.get(reverse("vote:scrutins"))
    assert response.status_code == 200


def test_liste_scrutins_scrutin_priorisation_ok_admin(client_admin, scrutin_factory):
    scrutin_factory(type_scrutin=Scrutin.MAJORITE_SYNDICATS_PRIORISATION)
    response = client_admin.get(reverse("vote:scrutins"))
    assert response.status_code == 200
