import pytest
from django.urls import reverse

from sud_education_vote.vote.forms import AdminCreateVoteForm
from sud_education_vote.vote.models import Scrutin, Vote

pytestmark = pytest.mark.django_db


def test_get_page_selection_candidat_not_ok_pour_syndicat(
    client_syndicat, scrutin_factory
):
    scrutin_factory(ouvert=True, type_scrutin=Scrutin.MAJORITE)
    response = client_syndicat.get(reverse("vote:page-vote-admin"))
    assert response.status_code == 403


def test_get_page_selection_candidat_ok_pour_admin(client_admin, scrutin_factory):
    scrutin_factory(ouvert=True, type_scrutin=Scrutin.MAJORITE)
    response = client_admin.get(reverse("vote:page-vote-admin"))
    assert response.status_code == 200
    assert isinstance(response.context.get("form"), AdminCreateVoteForm)


def test_selection_candidat_not_ok_pas_de_scrutin_ouvert(client_admin):
    response = client_admin.get(reverse("vote:page-vote-admin"))
    assert response.status_code == 302
    assert response.url == reverse("vote:scrutins")


def test_creation_vote_simple_par_admin_ok(
    client_admin, scrutin_factory, utilisateur_syndicat, utilisateur_admin
):
    scrutin = scrutin_factory(ouvert=True, type_scrutin=Scrutin.MAJORITE)
    response = client_admin.post(
        reverse("vote:page-vote-admin"), {"syndicat": utilisateur_syndicat.pk}
    )
    assert len(scrutin.votes.all()) == 0
    assert response.status_code == 302
    assert response.url == reverse("vote:page-vote")
    response = client_admin.get(reverse("vote:page-vote-simple"))
    assert response.context.get("vote_admin")
    assert response.context.get("user") == utilisateur_syndicat
    assert response.context.get("admin") == utilisateur_admin
    response = client_admin.post(
        reverse("vote:page-vote-simple"),
        {
            "vote_utilisateur": Vote.POUR,
            "mandats_pour": 30,
            "mandats_contre": 4,
            "mandats_abstention": 4,
            "mandats_nppv": 2,
            "scrutin": scrutin.pk,
            "utilisateur": utilisateur_syndicat.pk,
        },
    )
    assert response.status_code == 302
    assert response.url == reverse("vote:scrutins")
    scrutin.refresh_from_db()
    assert len(scrutin.votes.all()) == 1
    assert scrutin.votes.first().utilisateur == utilisateur_syndicat


def test_creation_vote_opposition_par_admin_ok(
    client_admin,
    scrutin_factory,
    proposition_factory,
    utilisateur_syndicat,
    utilisateur_admin,
):
    scrutin = scrutin_factory(
        ouvert=True, type_scrutin=Scrutin.MAJORITE_SYNDICATS_OPPOSITION
    )
    proposition = proposition_factory()
    response = client_admin.post(
        reverse("vote:page-vote-admin"), {"syndicat": utilisateur_syndicat.pk}
    )
    assert len(scrutin.votes_opposition.all()) == 0
    assert response.status_code == 302
    assert response.url == reverse("vote:page-vote")
    response = client_admin.get(reverse("vote:page-vote-opposition"))
    assert response.context.get("vote_admin")
    assert response.context.get("user") == utilisateur_syndicat
    assert response.context.get("admin") == utilisateur_admin
    response = client_admin.post(
        reverse("vote:page-vote-opposition"),
        {
            "selected_proposition": proposition.id,
            "abstention": False,
            "nppv": False,
            "scrutin": scrutin.pk,
            "utilisateur": utilisateur_syndicat.pk,
        },
    )
    assert response.status_code == 302
    assert response.url == reverse("vote:scrutins")
    scrutin.refresh_from_db()
    assert len(scrutin.votes_opposition.all()) == 1
    assert scrutin.votes_opposition.first().utilisateur == utilisateur_syndicat


def test_creation_vote_priorisation_par_admin_ok(
    client_admin,
    scrutin_factory,
    proposition_factory,
    utilisateur_syndicat,
    utilisateur_admin,
):
    scrutin = scrutin_factory(
        ouvert=True, type_scrutin=Scrutin.MAJORITE_SYNDICATS_PRIORISATION
    )
    proposition = proposition_factory()
    response = client_admin.post(
        reverse("vote:page-vote-admin"), {"syndicat": utilisateur_syndicat.pk}
    )
    assert len(scrutin.votes_priorisation.all()) == 0
    assert response.status_code == 302
    assert response.url == reverse("vote:page-vote")
    response = client_admin.get(reverse("vote:page-vote-priorisation"))
    assert response.context.get("vote_admin")
    assert response.context.get("user") == utilisateur_syndicat
    assert response.context.get("admin") == utilisateur_admin
    response = client_admin.post(
        reverse("vote:page-vote-priorisation"),
        {
            "selected_proposition": proposition.id,
            "abstention": False,
            "nppv": False,
            "scrutin": scrutin.pk,
            "utilisateur": utilisateur_syndicat.pk,
        },
    )
    assert response.status_code == 302
    assert response.url == reverse("vote:scrutins")
    scrutin.refresh_from_db()
    assert len(scrutin.votes_priorisation.all()) == 1
    assert scrutin.votes_priorisation.first().utilisateur == utilisateur_syndicat
