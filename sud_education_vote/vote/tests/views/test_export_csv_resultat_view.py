import pytest
from django.urls import reverse

from sud_education_vote.vote.models import Scrutin, Vote

pytestmark = pytest.mark.django_db


def test_export_csv_scrutin_redirection_si_non_connecte(client, scrutin):
    url_scrutin = reverse("vote:scrutin-export", kwargs={"pk": scrutin.pk})
    response = client.get(url_scrutin)
    assert response.status_code == 302
    assert response.url == f"{reverse('connexion')}?next={url_scrutin}"


def test_export_csv_scrutin_scrutins_pas_de_permission_superadmin(
    client_superadmin, scrutin
):
    response = client_superadmin.get(
        reverse("vote:scrutin-export", kwargs={"pk": scrutin.pk})
    )
    assert response.status_code == 403


def test_export_csv_scrutin_scrutins_ok_syndicat(client_syndicat, scrutin):
    response = client_syndicat.get(
        reverse("vote:scrutin-export", kwargs={"pk": scrutin.pk})
    )
    assert response.status_code == 200


def test_export_csv_scrutin_scrutins_ok_admin(
    client_admin, scrutin_factory, vote_factory, utilisateur_syndicat
):
    scrutin = scrutin_factory(
        type_scrutin=Scrutin.MAJORITE,
        adoption=True,
    )
    vote_factory(
        scrutin=scrutin,
        utilisateur=utilisateur_syndicat,
        mandats_pour=60,
        mandats_contre=5,
        mandats_abstention=5,
        mandats_nppv=2,
        vote_utilisateur=Vote.POUR,
    )
    response = client_admin.get(
        reverse("vote:scrutin-export", kwargs={"pk": scrutin.pk})
    )
    first_line = (
        "Syndicat,Vote du syndicat,Mandats pour,Mandats "
        "contre,Mandats abstention,Mandats NPPV"
    )
    assert response.status_code == 200
    assert response._container == [
        b"",
        b"" + first_line.encode() + b"\r\n",
        b"" + utilisateur_syndicat.__str__().encode() + b",POUR,60,5,5,2\r\n",
        b"" + "Résolution adoptée".encode() + b"\r\n",
    ]


def test_export_csv_scrutin_majorite_syndicats_ok_syndicat(
    client_syndicat, scrutin_factory, vote_factory, utilisateur_syndicat
):
    scrutin = scrutin_factory(type_scrutin=Scrutin.MAJORITE_SYNDICATS)
    vote_factory(
        scrutin=scrutin,
        utilisateur=utilisateur_syndicat,
        vote_utilisateur=Vote.POUR,
    )
    response = client_syndicat.get(
        reverse("vote:scrutin-export", kwargs={"pk": scrutin.pk})
    )
    assert response.status_code == 200


def test_export_csv_scrutin_opposition_ok_syndicat(
    client_syndicat,
    proposition_factory,
    vote_opposition_factory,
    scrutin_factory,
    utilisateur_syndicat,
):
    scrutin = scrutin_factory(type_scrutin=Scrutin.MAJORITE_SYNDICATS_OPPOSITION)
    proposition = proposition_factory(scrutin=scrutin, text="prop1")
    vote_opposition_factory(
        scrutin=scrutin,
        utilisateur=utilisateur_syndicat,
        selected_proposition=proposition,
    )
    response = client_syndicat.get(
        reverse("vote:scrutin-export", kwargs={"pk": scrutin.pk})
    )
    assert response.status_code == 200


def test_export_csv_scrutin_priorisation_ok_syndicat(client_syndicat, scrutin_factory):
    scrutin = scrutin_factory(type_scrutin=Scrutin.MAJORITE_SYNDICATS_PRIORISATION)
    response = client_syndicat.get(
        reverse("vote:scrutin-export", kwargs={"pk": scrutin.pk})
    )
    assert response.status_code == 200


def test_export_csv_scrutins_ok_admin(
    client_admin, scrutin_factory, vote_factory, utilisateur_syndicat
):
    scrutin = scrutin_factory(
        type_scrutin=Scrutin.MAJORITE,
        adoption=True,
    )
    vote_factory(
        scrutin=scrutin,
        utilisateur=utilisateur_syndicat,
        mandats_pour=60,
        mandats_contre=5,
        mandats_abstention=5,
        mandats_nppv=2,
        vote_utilisateur=Vote.POUR,
    )
    response = client_admin.get(reverse("vote:scrutins-export"))
    assert response.status_code == 200


def test_export_csv_scrutins_ok_syndicat(
    client_syndicat, scrutin_factory, vote_factory, utilisateur_syndicat
):
    scrutin = scrutin_factory(type_scrutin=Scrutin.MAJORITE_SYNDICATS)
    vote_factory(
        scrutin=scrutin,
        utilisateur=utilisateur_syndicat,
        vote_utilisateur=Vote.POUR,
    )
    response = client_syndicat.get(reverse("vote:scrutins-export"))
    assert response.status_code == 200


def test_export_csv_scrutins_majorite_opposition(
    client_syndicat, scrutin_factory, vote_opposition_factory, utilisateur_syndicat
):
    scrutin = scrutin_factory(type_scrutin=Scrutin.MAJORITE_SYNDICATS_OPPOSITION)
    vote_opposition_factory(
        scrutin=scrutin, utilisateur=utilisateur_syndicat, abstention=True
    )
    response = client_syndicat.get(reverse("vote:scrutins-export"))
    assert response.status_code == 200
