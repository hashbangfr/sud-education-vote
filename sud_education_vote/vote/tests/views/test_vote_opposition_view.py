import pytest
from django.urls import reverse

from sud_education_vote.log.models import Log
from sud_education_vote.vote.models import Scrutin, VoteOpposition

pytestmark = pytest.mark.django_db


def test_vote_page_vote_scrutin_opposition(
    client_syndicat, utilisateur_syndicat, scrutin_factory, proposition_factory
):
    scrutin = scrutin_factory(
        ouvert=True, type_scrutin=Scrutin.MAJORITE_SYNDICATS_OPPOSITION
    )
    proposition1 = proposition_factory(scrutin=scrutin, text="prop1")
    proposition_factory(scrutin=scrutin, text="prop2")
    assert VoteOpposition.objects.count() == 0
    response = client_syndicat.post(
        reverse("vote:page-vote-opposition"),
        {
            "selected_proposition": proposition1.id,
            "abstention": False,
            "nppv": False,
            "scrutin": scrutin.pk,
            "utilisateur": utilisateur_syndicat.pk,
        },
    )
    assert response.status_code == 302
    assert response.url == reverse("vote:page-vote")
    assert VoteOpposition.objects.count() == 1
    vote = VoteOpposition.objects.last()
    assert vote.selected_proposition == proposition1
    assert vote.utilisateur == utilisateur_syndicat
    assert vote.scrutin == scrutin
    assert Log.objects.last().log == (
        f"Le syndicat {utilisateur_syndicat} a renseigné le vote "
        f"suivant pour le scrutin {scrutin} : "
        f"{vote.resultat} "
    )


def test_resultat_scrutin_opposition_egalite(
    scrutin_factory, proposition_factory, vote_opposition_factory
):
    scrutin = scrutin_factory(
        ouvert=True, type_scrutin=Scrutin.MAJORITE_SYNDICATS_OPPOSITION
    )
    proposition1 = proposition_factory(scrutin=scrutin, text="prop1")
    proposition2 = proposition_factory(scrutin=scrutin, text="prop2")
    vote_opposition_factory(selected_proposition=proposition1, scrutin=scrutin)
    vote_opposition_factory(selected_proposition=proposition2, scrutin=scrutin)
    scrutin = Scrutin.objects.first()
    scrutin.ouvert = False
    scrutin.save()
    assert scrutin.resultat == "Egalité entre plusieurs propositions : prop1, prop2"


def test_vote_page_vote_scrutin_opposition_ferme(
    client_syndicat, utilisateur_syndicat, scrutin_factory, proposition_factory
):
    scrutin = scrutin_factory(
        ouvert=False, type_scrutin=Scrutin.MAJORITE_SYNDICATS_OPPOSITION
    )
    proposition1 = proposition_factory(scrutin=scrutin, text="prop1")
    assert VoteOpposition.objects.count() == 0
    response = client_syndicat.post(
        reverse("vote:page-vote-opposition"),
        {
            "selected_proposition": proposition1.id,
            "abstention": False,
            "nppv": False,
            "scrutin": scrutin.pk,
            "utilisateur": utilisateur_syndicat.pk,
        },
    )
    assert VoteOpposition.objects.count() == 0
    assert response.status_code == 200
    messages = response.context.get("messages")
    assert messages
    assert len(messages) == 1
    assert (
        str(messages._loaded_data[0])
        == "Ce scrutin est fermé, votre vote n'a pas été pris en compte"
    )


def test_vote_page_vote_scrutin_opposition_deja_vote(
    client_syndicat,
    utilisateur_syndicat,
    scrutin_factory,
    proposition_factory,
    vote_opposition_factory,
):
    scrutin = scrutin_factory(
        ouvert=True, type_scrutin=Scrutin.MAJORITE_SYNDICATS_OPPOSITION
    )
    proposition1 = proposition_factory(scrutin=scrutin, text="prop1")
    vote_opposition_factory(
        scrutin=scrutin,
        selected_proposition=proposition1,
        utilisateur=utilisateur_syndicat,
    )
    response = client_syndicat.post(
        reverse("vote:page-vote-opposition"),
        {
            "selected_proposition": proposition1.id,
            "abstention": False,
            "nppv": False,
            "scrutin": scrutin.pk,
            "utilisateur": utilisateur_syndicat.pk,
        },
    )
    assert VoteOpposition.objects.count() == 1
    assert response.status_code == 200
