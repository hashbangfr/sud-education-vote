from django.urls import path

from .views import (
    AdminVoteCreateView,
    ExportAllCSVResultatView,
    ExportCSVResultatView,
    ExportTxTMesVotesView,
    MesVotesView,
    PropositionCreateView,
    RedirectVoteCreateView,
    RedirectVoteUpdateView,
    ScrutinCreateView,
    ScrutinDetailView,
    ScrutinFermetureView,
    ScrutinListView,
    SuperAdminResetView,
    VoteCreateView,
    VoteDelegueCreateView,
    VoteOppositionCreateView,
    VoteOppositionUpdateView,
    VotePriorisationCreateView,
    VotePriorisationUpdateView,
    VoteUpdateView,
)

app_name = "vote"

urlpatterns = [
    path("", RedirectVoteCreateView.as_view(), name="page-vote"),
    path("vote/", VoteCreateView.as_view(), name="page-vote-simple"),
    path(
        "vote-opposition/",
        VoteOppositionCreateView.as_view(),
        name="page-vote-opposition",
    ),
    path(
        "vote-priorisation/",
        VotePriorisationCreateView.as_view(),
        name="page-vote-priorisation",
    ),
    path("vote/admin/", AdminVoteCreateView.as_view(), name="page-vote-admin"),
    path("mes-votes/", MesVotesView.as_view(), name="mes-votes"),
    path("mes-votes/export/", ExportTxTMesVotesView.as_view(), name="mes-votes-export"),
    path("scrutins/", ScrutinListView.as_view(), name="scrutins"),
    path("scrutins/creation/", ScrutinCreateView.as_view(), name="scrutin-creation"),
    path(
        "scrutins/<int:pk>/",
        ScrutinDetailView.as_view(),
        name="scrutin-detail",
    ),
    path(
        "scrutins/<int:pk>/export/",
        ExportCSVResultatView.as_view(),
        name="scrutin-export",
    ),
    path(
        "scrutins/export/",
        ExportAllCSVResultatView.as_view(),
        name="scrutins-export",
    ),
    path(
        "scrutins/<int:scrutin>/vote/<int:pk>/update/",
        RedirectVoteUpdateView.as_view(),
        name="vote-modification",
    ),
    path(
        "scrutins/<int:scrutin>/vote-simple/<int:pk>/update/",
        VoteUpdateView.as_view(),
        name="vote-simple-modification",
    ),
    path(
        "scrutins/<int:scrutin>/vote-opposition/<int:pk>/update/",
        VoteOppositionUpdateView.as_view(),
        name="vote-opposition-modification",
    ),
    path(
        "scrutins/<int:scrutin>/vote-priorisation/<int:pk>/update/",
        VotePriorisationUpdateView.as_view(),
        name="vote-priorisation-modification",
    ),
    path(
        "scrutins/<int:pk>/fermeture/",
        ScrutinFermetureView.as_view(),
        name="scrutin-fermeture",
    ),
    path(
        "scrutins/<int:pk>/vote-delegue/",
        VoteDelegueCreateView.as_view(),
        name="scrutin-delegue",
    ),
    path(
        "scrutins/<int:pk>/add-propositions/",
        PropositionCreateView.as_view(),
        name="scrutin-propositions",
    ),
    path(
        "reinitialisation/",
        SuperAdminResetView.as_view(),
        name="reinitialisation",
    ),
]
