import csv
import io
import zipfile
from wsgiref.util import FileWrapper

from django.contrib import messages
from django.http import HttpResponseRedirect, StreamingHttpResponse
from django.urls import reverse
from django.views.generic import FormView

from sud_education_vote.log.models import Log
from sud_education_vote.mixins import SuperAdminRequisMixin
from sud_education_vote.vote.forms import ResetForm
from sud_education_vote.vote.models import Scrutin
from sud_education_vote.vote.views.gestion_scrutin import (
    get_writer_scrutin_opposition_priorisation,
    get_writer_scrutin_simple,
)


class SuperAdminResetView(SuperAdminRequisMixin, FormView):
    template_name = "vote/reset.html"
    form_class = ResetForm

    def get_fichiers_csv(self):
        csv_files = []
        scrutins = Scrutin.objects.all()
        for scrutin in scrutins:
            mem_file = io.StringIO()
            writer = csv.writer(mem_file)
            if scrutin.type_scrutin in [
                scrutin.MAJORITE_SYNDICATS_OPPOSITION,
                Scrutin.MAJORITE_SYNDICATS_PRIORISATION,
            ]:
                writer = get_writer_scrutin_opposition_priorisation(
                    self, scrutin, writer
                )
            else:
                writer = get_writer_scrutin_simple(self, scrutin, writer)
            mem_file.seek(0)
            csv_files.append(
                {
                    "scrutin_pk": scrutin.pk,
                    "scrutin_intitule": scrutin.intitule,
                    "csv_file": mem_file,
                }
            )
        return csv_files

    def get_success_url(self):
        return reverse("vote:reinitialisation")

    def form_valid(self, form):
        archive_souhaitee = form.cleaned_data["archive_souhaitee"]
        if archive_souhaitee:
            csv_datas = self.get_fichiers_csv()
            temp_file = io.BytesIO()
            with zipfile.ZipFile(
                temp_file, "w", zipfile.ZIP_DEFLATED
            ) as temp_file_opened:
                # export d'un fichier csv pour chaque scrutin
                for data in csv_datas:
                    data["csv_file"].seek(0)
                    temp_file_opened.writestr(
                        f"scrutin_{data['scrutin_intitule']}_{data['scrutin_pk']}.csv",
                        data["csv_file"].getvalue(),
                    )
                content = ""
                # export des logs
                for log in Log.objects.all():
                    date_str = log.date_log.strftime("%m/%d/%Y %H:%M:%S")
                    content = content + f"{date_str} {log.log}\n"
                temp_file_opened.writestr("journal.txt", content)
            temp_file.seek(0)
            # put them to streaming content response
            # within zip content_type
            response = StreamingHttpResponse(
                FileWrapper(temp_file),
                content_type="application/zip",
            )
            response[
                "Content-Disposition"
            ] = "attachment;filename=export_reinitialisation.zip"
            Scrutin.objects.all().delete()
            Log.objects.all().delete()
            messages.add_message(
                self.request,
                messages.SUCCESS,
                ("La réinitialisation a bien été effectuée !"),
            )
            return response
        Scrutin.objects.all().delete()
        Log.objects.all().delete()
        messages.add_message(
            self.request,
            messages.SUCCESS,
            ("La réinitialisation a bien été effectuée !"),
        )
        return HttpResponseRedirect(self.get_success_url())
