from django import forms
from django.contrib import messages
from django.db.models import Q
from django.http import HttpResponse, HttpResponseRedirect
from django.shortcuts import get_object_or_404
from django.urls import reverse
from django.utils.translation import gettext_lazy as _
from django.views.generic import (
    CreateView,
    FormView,
    RedirectView,
    TemplateView,
    UpdateView,
    View,
)

from sud_education_vote.log.models import Log
from sud_education_vote.mixins import (
    AdminRequisMixin,
    SyndicatOuAdminRequisMixin,
    SyndicatRequisMixin,
)
from sud_education_vote.utilisateur.models import Utilisateur

from ..forms import (
    AdminCreateVoteForm,
    VoteCreationForm,
    VoteDelegueCreationForm,
    VoteMajoriteSyndicatsCreationForm,
    VoteMajoriteSyndicatsOppositionCreationForm,
    VoteMajoriteSyndicatsOppositionUpdateForm,
    VoteMajoriteSyndicatsPriorisationCreationForm,
    VoteMajoriteSyndicatsPriorisationUpdateForm,
    VoteMajoriteSyndicatsUpdateForm,
    VoteUpdateForm,
)
from ..models import (
    Proposition,
    Scrutin,
    Vote,
    VoteOpposition,
    VotePriorisation,
    VoteProposition,
)


class VoteDelegueCreateView(AdminRequisMixin, CreateView):
    model = Vote
    template_name = "vote/creation_vote_delegue.html"
    form_class = VoteDelegueCreationForm
    scrutin = None

    def setup(self, request, *args, **kwargs):
        super().setup(request, *args, **kwargs)
        self.scrutin = get_object_or_404(
            Scrutin, pk=self.kwargs.get("pk"), type_scrutin=Scrutin.DELEGUE
        )

    def get_initial(self):
        initial = super().get_initial()
        initial.update({"scrutin": self.scrutin, "utilisateur": self.request.user})
        return initial

    def get_success_url(self):
        return reverse("vote:scrutins")

    def form_valid(self, form):
        response = super().form_valid(form)
        self.scrutin.ouvert = False
        self.scrutin.save()
        Log.objects.create(
            log=(
                f"L'admin {self.request.user} a rentré les votes suivant "
                f"pour le scrutin {self.scrutin} par delegue: "
                f"{self.object.mandats_pour} votes 'Pour', "
                f"{self.object.mandats_contre} votes 'Contre', "
                f"{self.object.mandats_abstention} votes 'Abstention', "
                f"et {self.object.mandats_nppv} votes 'Ne se prononce pas'."
            )
        )
        self.scrutin.calcul_resultat()
        Log.objects.create(
            log=f"L'admin {self.request.user} a fermé le scrutin {self.scrutin}"
        )
        Log.objects.create(
            log=(
                f"Le scrutin {self.scrutin} voit sa résolution "
                f"{'adoptée' if self.scrutin.adoption else 'rejetée'}"
            )
        )
        return response


class RedirectVoteCreateView(SyndicatOuAdminRequisMixin, RedirectView):
    def setup(self, request, *args, **kwargs):
        super().setup(request, *args, **kwargs)
        self.scrutin = (
            Scrutin.objects.filter(ouvert=True)
            .exclude(type_scrutin=Scrutin.DELEGUE)
            .first()
        )

    def get_redirect_url(self, *args, **kwargs):
        if (
            self.scrutin
            and self.scrutin.type_scrutin == Scrutin.MAJORITE_SYNDICATS_OPPOSITION
        ):
            return reverse("vote:page-vote-opposition")
        elif (
            self.scrutin
            and self.scrutin.type_scrutin == Scrutin.MAJORITE_SYNDICATS_PRIORISATION
        ):
            return reverse("vote:page-vote-priorisation")
        else:
            return reverse("vote:page-vote-simple")


class VoteCreateView(SyndicatOuAdminRequisMixin, CreateView):
    template_name = "vote/page_vote.html"
    model = Vote
    scrutin = None

    def get_form_class(self):
        if self.scrutin and self.scrutin.type_scrutin == Scrutin.MAJORITE_SYNDICATS:
            return VoteMajoriteSyndicatsCreationForm
        else:
            return VoteCreationForm

    def get_context_data(self, **kwargs):
        context = super().get_context_data(**kwargs)
        context["scrutin"] = self.scrutin
        if self.request.user.is_admin and self.syndicat_choisi:
            context["vote_admin"] = True
            context["user"] = self.syndicat_choisi
            context["admin"] = self.request.user
        if self.scrutin:
            context["deja_vote"] = Vote.objects.filter(
                scrutin=self.scrutin,
                utilisateur=self.request.user,
            ).exists()
            if context["deja_vote"]:
                context["vote_existant"] = Vote.objects.filter(
                    scrutin=self.scrutin,
                    utilisateur=self.request.user,
                ).first()
            if self.scrutin.type_scrutin == Scrutin.MAJORITE_SYNDICATS:
                context["majorite_syndicats"] = True
        return context

    def get_initial(self):
        initial = super().get_initial()
        if self.request.session.get("syndicat_choisi_id"):
            self.syndicat_choisi = get_object_or_404(
                Utilisateur, pk=self.request.session.get("syndicat_choisi_id")
            )
        if self.request.user.is_admin and self.syndicat_choisi:
            initial.update(
                {"scrutin": self.scrutin, "utilisateur": self.syndicat_choisi}
            )
        else:
            initial.update({"scrutin": self.scrutin, "utilisateur": self.request.user})
        return initial

    def get_success_url(self):
        if self.request.user.is_admin and self.syndicat_choisi:
            return reverse("vote:scrutins")
        else:
            return reverse("vote:page-vote")

    def form_invalid(self, form):
        if form.errors.get("scrutin"):
            messages.add_message(
                self.request,
                messages.ERROR,
                form.errors.get("scrutin")[0],
            )
        return super().form_invalid(form)

    def form_valid(self, form):
        response = super().form_valid(form)
        if self.request.user.is_admin and self.syndicat_choisi:
            user = (
                f"L'admin {self.request.user}, votant "
                f"pour le syndicat {self.syndicat_choisi.username}, "
            )
        else:
            user = f"Le syndicat {self.request.user}"
        if self.scrutin.type_scrutin == Scrutin.MAJORITE_SYNDICATS:
            Log.objects.create(
                log=(
                    f"{user} a rentré le vote suivant "
                    f"pour le scrutin {self.scrutin} : "
                    f"{self.object.vote_utilisateur}"
                )
            )
        else:
            Log.objects.create(
                log=(
                    f"{user} a rentré les votes suivants "
                    f"pour le scrutin {self.scrutin} : "
                    f"{self.object.mandats_pour} votes 'Pour', "
                    f"{self.object.mandats_contre} votes 'Contre', "
                    f"{self.object.mandats_abstention} votes 'Abstention', "
                    f"et {self.object.mandats_nppv} votes 'Ne se prononce pas'."
                )
            )
        messages.add_message(
            self.request,
            messages.SUCCESS,
            _("Votre vote a bien été pris en compte!"),
        )
        return response

    def setup(self, request, *args, **kwargs):
        super().setup(request, *args, **kwargs)
        self.scrutin = (
            Scrutin.objects.filter(ouvert=True)
            .exclude(type_scrutin=Scrutin.DELEGUE)
            .first()
        )


class VoteOppositionCreateView(SyndicatOuAdminRequisMixin, CreateView):
    template_name = "vote/page_vote.html"
    model = VoteOpposition

    def get_form_class(self):
        return VoteMajoriteSyndicatsOppositionCreationForm

    def get_context_data(self, **kwargs):
        context = super().get_context_data(**kwargs)
        context["scrutin"] = self.scrutin
        if self.request.user.is_admin and self.syndicat_choisi:
            context["vote_admin"] = True
            context["user"] = self.syndicat_choisi
            context["admin"] = self.request.user
        if self.scrutin:
            scrutin_propositions = Proposition.objects.filter(scrutin=self.scrutin)
            context["form"].fields[
                "selected_proposition"
            ].queryset = scrutin_propositions
            context["form"].fields["selected_proposition"].required = False
        context["majorite_syndicats"] = True

        context["opposition"] = True
        if self.scrutin:
            context["deja_vote"] = VoteOpposition.objects.filter(
                scrutin=self.scrutin,
                utilisateur=self.request.user,
            ).exists()
            if context["deja_vote"]:
                context["vote_existant"] = VoteOpposition.objects.filter(
                    scrutin=self.scrutin,
                    utilisateur=self.request.user,
                ).first()
        return context

    def get_initial(self):
        initial = super().get_initial()
        if self.request.session.get("syndicat_choisi_id"):
            self.syndicat_choisi = get_object_or_404(
                Utilisateur, pk=self.request.session.get("syndicat_choisi_id")
            )
        if self.request.user.is_admin and self.syndicat_choisi:
            initial.update(
                {"scrutin": self.scrutin, "utilisateur": self.syndicat_choisi}
            )
        else:
            initial.update({"scrutin": self.scrutin, "utilisateur": self.request.user})
        return initial

    def get_success_url(self):
        if self.request.user.is_admin and self.syndicat_choisi:
            return reverse("vote:scrutins")
        else:
            return reverse("vote:page-vote")

    def form_invalid(self, form):
        if form.errors.get("scrutin"):
            messages.add_message(
                self.request,
                messages.ERROR,
                form.errors.get("scrutin")[0],
            )
        return super().form_invalid(form)

    def form_valid(self, form):
        response = super().form_valid(form)
        if self.request.user.is_admin and self.syndicat_choisi:
            user = (
                f"L'admin {self.request.user}, votant "
                f"pour le syndicat {self.syndicat_choisi.username},"
            )
        else:
            user = f"Le syndicat {self.request.user}"
        Log.objects.create(
            log=(
                f"{user}"
                f" a renseigné le vote "
                f"suivant pour le scrutin {self.scrutin} : "
                f"{self.object.resultat} "
            )
        )
        messages.add_message(
            self.request,
            messages.SUCCESS,
            _("Votre vote a bien été pris en compte!"),
        )
        return response

    def setup(self, request, *args, **kwargs):
        super().setup(request, *args, **kwargs)
        """ TODO : getobjet or 400? """
        self.scrutin = (
            Scrutin.objects.filter(ouvert=True)
            .exclude(type_scrutin=Scrutin.DELEGUE)
            .first()
        )


class VotePriorisationCreateView(SyndicatOuAdminRequisMixin, CreateView):
    template_name = "vote/page_vote.html"
    model = VotePriorisation

    def get_form_class(self):
        return VoteMajoriteSyndicatsPriorisationCreationForm

    def get_context_data(self, **kwargs):
        context = super().get_context_data(**kwargs)
        if self.request.user.is_admin and self.syndicat_choisi:
            context["vote_admin"] = True
            context["user"] = self.syndicat_choisi
            context["admin"] = self.request.user
        context["scrutin"] = self.scrutin
        context["majorite_syndicats"] = True
        context["priorisation"] = True
        if self.scrutin:
            propositions = self.scrutin.propositions.all()
            for proposition in propositions:
                context["form"].fields[
                    f"proposition-{proposition.id}"
                ] = forms.IntegerField()
                context["form"].fields[
                    f"proposition-{proposition.id}"
                ].label = f"{proposition.text}"
                context["form"].fields[f"proposition-{proposition.id}"].widget.attrs[
                    "class"
                ] = "proposition"
                context["form"].fields[f"proposition-{proposition.id}"].widget.attrs[
                    "max"
                ] = propositions.count()
                context["form"].fields[f"proposition-{proposition.id}"].widget.attrs[
                    "min"
                ] = 1
                context["nombre_propositions"] = propositions.count()
                context["deja_vote"] = VotePriorisation.objects.filter(
                    scrutin=self.scrutin,
                    utilisateur=self.request.user,
                ).exists()
                if context["deja_vote"]:
                    context["vote_existant"] = VotePriorisation.objects.filter(
                        scrutin=self.scrutin,
                        utilisateur=self.request.user,
                    ).first()
        return context

    def get_initial(self):
        initial = super().get_initial()
        if self.request.session.get("syndicat_choisi_id"):
            self.syndicat_choisi = get_object_or_404(
                Utilisateur, pk=self.request.session.get("syndicat_choisi_id")
            )
        if self.request.user.is_admin and self.syndicat_choisi:
            initial.update(
                {"scrutin": self.scrutin, "utilisateur": self.syndicat_choisi}
            )
        else:
            initial.update({"scrutin": self.scrutin, "utilisateur": self.request.user})
        return initial

    def get_success_url(self):
        if self.request.user.is_admin and self.syndicat_choisi:
            return reverse("vote:scrutins")
        else:
            return reverse("vote:page-vote")

    def form_invalid(self, form):
        if form.errors.get("scrutin"):
            messages.add_message(
                self.request,
                messages.ERROR,
                form.errors.get("scrutin")[0],
            )
        return super().form_invalid(form)

    def form_valid(self, form):
        response = super().form_valid(form)
        abstention = form.cleaned_data["abstention"]
        nppv = form.cleaned_data["nppv"]
        if not abstention and not nppv:
            propositions = self.scrutin.propositions.all()
            for p in propositions:
                VoteProposition.objects.create(
                    proposition=p,
                    score=form.data[f"proposition-{p.id}"],
                    vote=self.object,
                )
        if self.request.user.is_admin and self.syndicat_choisi:
            user = (
                f"L'admin {self.request.user}, votant "
                f"pour le syndicat {self.syndicat_choisi.username},"
            )
        else:
            user = f"Le syndicat {self.request.user}"
        Log.objects.create(
            log=(
                f"{user}"
                f" a rentré le vote suivant "
                f"pour le scrutin {self.scrutin} : {self.object.resultat}"
            )
        )
        messages.add_message(
            self.request,
            messages.SUCCESS,
            _("Votre vote a bien été pris en compte!"),
        )
        return response

    def setup(self, request, *args, **kwargs):
        super().setup(request, *args, **kwargs)
        self.scrutin = (
            Scrutin.objects.filter(ouvert=True)
            .exclude(type_scrutin=Scrutin.DELEGUE)
            .first()
        )


class MesVotesView(SyndicatRequisMixin, TemplateView):
    template_name = "vote/mes_votes.html"

    def get_context_data(self, **kwargs):
        context = super().get_context_data(**kwargs)
        syndicat = self.request.user
        scrutins = Scrutin.objects.filter(
            Q(votes_priorisation__utilisateur=syndicat)
            | Q(votes_opposition__utilisateur=syndicat)
            | Q(votes__utilisateur=syndicat)
            | Q(type_scrutin=Scrutin.DELEGUE)
        ).order_by("-pk")
        votes_syndicat = []
        for scrutin in scrutins:
            data = {}
            if scrutin.type_scrutin == Scrutin.DELEGUE:
                data.update({"scrutin": scrutin})
            elif scrutin.type_scrutin == Scrutin.MAJORITE_SYNDICATS_OPPOSITION:
                vote = VoteOpposition.objects.filter(
                    scrutin=scrutin, utilisateur=syndicat
                ).first()
                if vote:
                    data.update({"scrutin": scrutin, "vote": vote})
            elif scrutin.type_scrutin == Scrutin.MAJORITE_SYNDICATS_PRIORISATION:
                vote = VotePriorisation.objects.filter(
                    scrutin=scrutin, utilisateur=syndicat
                ).first()
                if vote:
                    data.update({"scrutin": scrutin, "vote": vote})
            else:
                vote = Vote.objects.filter(
                    scrutin=scrutin, utilisateur=syndicat
                ).first()
                if vote:
                    data.update({"scrutin": scrutin, "vote": vote})
            votes_syndicat.append(data)
        context["votes_syndicat"] = votes_syndicat
        return context


class ExportTxTMesVotesView(SyndicatRequisMixin, View):
    def get(self, request, *args, **kwargs):
        content = ""
        syndicat = self.request.user
        scrutins = Scrutin.objects.filter(
            Q(votes_priorisation__utilisateur=syndicat)
            | Q(votes_opposition__utilisateur=syndicat)
            | Q(votes__utilisateur=syndicat)
            | Q(type_scrutin=Scrutin.DELEGUE)
        ).order_by("-pk")
        content = ""
        for scrutin in scrutins:
            ligne = f"{scrutin.intitule}, {scrutin.type_scrutin_display}"
            ligne = f"{ligne}, {scrutin.resultat}"
            if scrutin.type_scrutin != Scrutin.DELEGUE:
                if scrutin.type_scrutin == Scrutin.MAJORITE_SYNDICATS_OPPOSITION:
                    vote = VoteOpposition.objects.filter(
                        scrutin=scrutin, utilisateur=syndicat
                    ).first()
                    ligne = f"{ligne}, vote syndicat: {vote.resultat}"
                elif scrutin.type_scrutin == Scrutin.MAJORITE_SYNDICATS_PRIORISATION:
                    vote = VotePriorisation.objects.filter(
                        scrutin=scrutin, utilisateur=syndicat
                    ).first()
                    ligne = f"{ligne}, vote syndicat: {vote.resultat}"
                else:
                    vote = Vote.objects.filter(
                        scrutin=scrutin, utilisateur=syndicat
                    ).first()
                    ligne = f"{ligne}, vote syndicat: {vote.vote_utilisateur}"
                    if scrutin.type_scrutin != Scrutin.MAJORITE_SYNDICATS:
                        ligne = f"{ligne}, mandats pour: {vote.mandats_pour}"
                        ligne = f"{ligne}, mandats contre: {vote.mandats_contre}"
                        ligne = (
                            f"{ligne}, mandats abstention: {vote.mandats_abstention}"
                        )
                        ligne = f"{ligne}, mandats nppv: {vote.mandats_nppv}"
            content = f"{content}{ligne}\n"
        response = HttpResponse(
            content=content,
            content_type="text/plain",
            headers={"Content-Disposition": "attachment; filename=mesvotes.txt"},
        )
        return response


class RedirectVoteUpdateView(SyndicatOuAdminRequisMixin, RedirectView):
    def setup(self, request, *args, **kwargs):
        super().setup(request, *args, **kwargs)
        self.scrutin = get_object_or_404(Scrutin, pk=kwargs.get("scrutin"))

    def get_redirect_url(self, *args, **kwargs):
        if (
            self.scrutin
            and self.scrutin.type_scrutin == Scrutin.MAJORITE_SYNDICATS_OPPOSITION
        ):
            return reverse("vote:vote-opposition-modification", kwargs=kwargs)
        elif (
            self.scrutin
            and self.scrutin.type_scrutin == Scrutin.MAJORITE_SYNDICATS_PRIORISATION
        ):
            return reverse("vote:vote-priorisation-modification", kwargs=kwargs)
        else:
            return reverse("vote:vote-simple-modification", kwargs=kwargs)


def verifier_si_utilisateur_vote_ou_admin(self, request, *args, **kwargs):
    utilisateur = Utilisateur.objects.filter(pk=request.user.pk).first()
    if utilisateur:
        is_admin = utilisateur.is_admin
        is_owner = utilisateur == self.vote.utilisateur
        if not (is_admin or is_owner) or (not self.scrutin.ouvert and not is_admin):
            messages.add_message(
                self.request,
                messages.ERROR,
                _("Vous n'avez pas l'autorisation de modifier ce vote !"),
            )
            return False
    return True


class VoteUpdateView(SyndicatOuAdminRequisMixin, UpdateView):
    template_name = "vote/vote_modification.html"
    model = Vote
    scrutin = None

    def dispatch(self, request, *args, **kwargs):
        utilisateur_autorise = verifier_si_utilisateur_vote_ou_admin(
            self, request, *args, **kwargs
        )
        if not utilisateur_autorise:
            return HttpResponseRedirect(reverse("vote:scrutins"))
        return super().dispatch(request, *args, **kwargs)

    def get_form_class(self):
        if self.scrutin and self.scrutin.type_scrutin == Scrutin.MAJORITE_SYNDICATS:
            return VoteMajoriteSyndicatsUpdateForm
        else:
            return VoteUpdateForm

    def get_context_data(self, **kwargs):
        context = super().get_context_data(**kwargs)
        context["scrutin"] = self.scrutin
        if self.scrutin.type_scrutin == Scrutin.MAJORITE_SYNDICATS:
            context["majorite_syndicats"] = True
        return context

    def get_success_url(self):
        return reverse("vote:page-vote")

    def form_valid(self, form):
        response = super().form_valid(form)
        if not self.request.user.is_admin:
            user = f"Le syndicat {self.request.user} a modifié son vote "
        else:
            user = (
                f"L'admin {self.request.user} a modifié le vote "
                f"du syndicat {self.object.utilisateur} "
            )
        if self.scrutin.type_scrutin == Scrutin.MAJORITE_SYNDICATS:
            resultat = f"{self.object.vote_utilisateur}"
        else:
            resultat = (
                f"{self.object.mandats_pour} votes 'Pour', "
                f"{self.object.mandats_contre} votes 'Contre', "
                f"{self.object.mandats_abstention} votes 'Abstention', "
                f"et {self.object.mandats_nppv} votes 'Ne se prononce pas'."
            )
        Log.objects.create(
            log=(f"{user}" f"pour le scrutin {self.scrutin} : " f"{resultat}")
        )
        messages.add_message(
            self.request,
            messages.SUCCESS,
            _("Le vote a bien été pris en compte!"),
        )
        previous_result = self.scrutin.adoption
        self.scrutin.calcul_resultat()
        if previous_result != self.scrutin.adoption:
            Log.objects.create(
                log=(
                    f"Le scrutin {self.scrutin} voit sa résolution "
                    f"{'adoptée' if self.scrutin.adoption else 'rejetée'}"
                )
            )
        return response

    def setup(self, request, *args, **kwargs):
        super().setup(request, *args, **kwargs)
        self.scrutin = get_object_or_404(Scrutin, pk=kwargs.get("scrutin"))
        self.vote = get_object_or_404(Vote, pk=kwargs.get("pk"))


class VoteOppositionUpdateView(SyndicatOuAdminRequisMixin, UpdateView):
    template_name = "vote/vote_modification.html"
    model = VoteOpposition
    scrutin = None

    def dispatch(self, request, *args, **kwargs):
        utilisateur_autorise = verifier_si_utilisateur_vote_ou_admin(
            self, request, *args, **kwargs
        )
        if not utilisateur_autorise:
            return HttpResponseRedirect(reverse("vote:scrutins"))
        return super().dispatch(request, *args, **kwargs)

    def get_form_class(self):
        return VoteMajoriteSyndicatsOppositionUpdateForm

    def get_context_data(self, **kwargs):
        context = super().get_context_data(**kwargs)
        context["scrutin"] = self.scrutin
        if self.scrutin:
            scrutin_propositions = Proposition.objects.filter(scrutin=self.scrutin)
            context["form"].fields[
                "selected_proposition"
            ].queryset = scrutin_propositions
            context["form"].fields["selected_proposition"].required = False
        context["opposition"] = True
        context["majorite_syndicats"] = True
        return context

    def get_success_url(self):
        return reverse("vote:page-vote")

    def form_valid(self, form):
        response = super().form_valid(form)
        if not self.request.user.is_admin:
            Log.objects.create(
                log=(
                    f"Le syndicat {self.request.user} a modifié son vote "
                    f"pour le scrutin {self.scrutin} : "
                    f"{self.object.resultat}"
                )
            )
        else:
            Log.objects.create(
                log=(
                    f"L'admin {self.request.user} a modifié le vote "
                    f"du syndicat {self.object.utilisateur} "
                    f"pour le scrutin {self.scrutin} : "
                    f"{self.object.resultat}"
                )
            )
        messages.add_message(
            self.request,
            messages.SUCCESS,
            _("Votre vote a bien été pris en compte!"),
        )
        return response

    def setup(self, request, *args, **kwargs):
        super().setup(request, *args, **kwargs)
        self.scrutin = get_object_or_404(Scrutin, pk=kwargs.get("scrutin"))
        self.vote = get_object_or_404(VoteOpposition, pk=kwargs.get("pk"))


class VotePriorisationUpdateView(SyndicatOuAdminRequisMixin, UpdateView):
    template_name = "vote/vote_modification.html"
    model = VotePriorisation

    def dispatch(self, request, *args, **kwargs):
        utilisateur_autorise = verifier_si_utilisateur_vote_ou_admin(
            self, request, *args, **kwargs
        )
        if not utilisateur_autorise:
            return HttpResponseRedirect(reverse("vote:scrutins"))
        return super().dispatch(request, *args, **kwargs)

    def get_initial(self):
        propositions = self.scrutin.propositions.all()
        initial_values = {}
        for proposition in propositions:
            vote_proposition = VoteProposition.objects.filter(
                vote=self.object, proposition=proposition
            ).first()
            if vote_proposition:
                initial_values[f"proposition-{proposition.id}"] = vote_proposition.score
        return initial_values

    def get_form_class(self):
        return VoteMajoriteSyndicatsPriorisationUpdateForm

    def get_context_data(self, **kwargs):
        context = super().get_context_data(**kwargs)
        is_admin = self.request.user.is_admin
        is_owner = self.request.user.pk == self.object.utilisateur.pk
        if not (is_admin or is_owner) or (not self.scrutin.ouvert and not is_admin):
            messages.add_message(
                self.request,
                messages.ERROR,
                _("Vous n'avez pas l'autorisation de modifier ce vote !"),
            )
        context["scrutin"] = self.scrutin
        if self.scrutin:
            propositions = self.scrutin.propositions.all()
            context["nombre_propositions"] = propositions.count()
            for proposition in propositions:
                context["form"].fields[
                    f"proposition-{proposition.id}"
                ] = forms.IntegerField(label=f"{proposition.text}")
                context["form"].fields[f"proposition-{proposition.id}"].widget.attrs = {
                    "class": "proposition",
                    "max": propositions.count(),
                    "min": 1,
                }
        context["priorisation"] = True
        context["majorite_syndicats"] = True
        return context

    def get_success_url(self):
        return reverse("vote:page-vote")

    def form_valid(self, form):
        response = super().form_valid(form)
        """ On supprime les scores précédents attribués """
        votes_precedents = VoteProposition.objects.filter(
            vote__utilisateur=self.object.utilisateur, vote__scrutin=self.scrutin
        )
        votes_precedents.delete()
        abstention = form.cleaned_data["abstention"]
        nppv = form.cleaned_data["nppv"]
        if not abstention and not nppv:
            propositions = self.scrutin.propositions.all()
            for p in propositions:
                VoteProposition.objects.create(
                    proposition=p,
                    score=form.data[f"proposition-{p.id}"],
                    vote=self.object,
                )
        if not self.request.user.is_admin:
            Log.objects.create(
                log=(
                    f"Le syndicat {self.request.user} a modifié son vote "
                    f"pour le scrutin {self.scrutin} : "
                    f"{self.object.resultat}"
                )
            )
        else:
            Log.objects.create(
                log=(
                    f"L'admin {self.request.user} a modifié le vote "
                    f"du syndicat {self.object.utilisateur} "
                    f"pour le scrutin {self.scrutin} : "
                    f"{self.object.resultat}"
                )
            )
        messages.add_message(
            self.request,
            messages.SUCCESS,
            _("Votre vote a bien été pris en compte!"),
        )
        return response

    def setup(self, request, *args, **kwargs):
        super().setup(request, *args, **kwargs)
        self.scrutin = get_object_or_404(Scrutin, pk=kwargs.get("scrutin"))
        self.vote = get_object_or_404(VotePriorisation, pk=kwargs.get("pk"))


class AdminVoteCreateView(AdminRequisMixin, FormView):
    template_name = "vote/admin_creation_vote.html"
    form_class = AdminCreateVoteForm

    def dispatch(self, request, *args, **kwargs):
        if not Scrutin.objects.filter(ouvert=True).exists():
            messages.add_message(
                self.request,
                messages.ERROR,
                _("Vous ne pouvez pas voter, aucun scrutin n'est ouvert !"),
            )
            return HttpResponseRedirect(reverse("vote:scrutins"))
        return super().dispatch(request, *args, **kwargs)

    def setup(self, request, *args, **kwargs):
        super().setup(request, *args, **kwargs)
        self.scrutin = (
            Scrutin.objects.filter(ouvert=True)
            .exclude(type_scrutin=Scrutin.DELEGUE)
            .first()
        )

    def get_success_url(self):
        return reverse("vote:page-vote")

    def form_valid(self, form):
        self.request.session["syndicat_choisi_id"] = form.cleaned_data["syndicat"].id
        return HttpResponseRedirect(self.get_success_url())
