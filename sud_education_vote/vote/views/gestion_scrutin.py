import csv
import io
import zipfile
from wsgiref.util import FileWrapper

from django.contrib import messages
from django.http import HttpResponse, HttpResponseRedirect, StreamingHttpResponse
from django.shortcuts import get_object_or_404
from django.urls import reverse
from django.utils.translation import gettext_lazy as _
from django.views.generic import (
    CreateView,
    DeleteView,
    DetailView,
    ListView,
    TemplateView,
    View,
)

from sud_education_vote.log.models import Log
from sud_education_vote.mixins import AdminRequisMixin, SyndicatOuAdminRequisMixin
from sud_education_vote.utilisateur.models import Utilisateur

from ..forms import ScrutinCreationForm
from ..models import Proposition, Scrutin, Vote, VoteOpposition, VotePriorisation


class ScrutinListView(SyndicatOuAdminRequisMixin, ListView):
    template_name = "vote/liste_scrutins.html"
    model = Scrutin

    def get_queryset(self):
        queryset = super().get_queryset()
        queryset = queryset.order_by("-id")
        return queryset

    def get_context_data(self, **kwargs):
        context = super().get_context_data(**kwargs)
        nombre_utilisateurs = Utilisateur.objects.filter(
            type_utilisateur=Utilisateur.SYNDICAT
        ).count()
        if self.request.user.is_admin:
            context["scrutin_actuel"] = Scrutin.objects.filter(ouvert=True).first()
            if context["scrutin_actuel"]:
                if (
                    context["scrutin_actuel"].type_scrutin
                    == Scrutin.MAJORITE_SYNDICATS_OPPOSITION
                ):
                    context["nombre_syndicats_non_vote"] = (
                        nombre_utilisateurs
                        - VoteOpposition.objects.filter(
                            scrutin=context["scrutin_actuel"]
                        ).count()
                    )
                elif (
                    context["scrutin_actuel"].type_scrutin
                    == Scrutin.MAJORITE_SYNDICATS_PRIORISATION
                ):
                    context["nombre_syndicats_non_vote"] = (
                        nombre_utilisateurs
                        - VotePriorisation.objects.filter(
                            scrutin=context["scrutin_actuel"]
                        ).count()
                    )
                else:
                    context["nombre_syndicats_non_vote"] = (
                        nombre_utilisateurs
                        - Vote.objects.filter(scrutin=context["scrutin_actuel"]).count()
                    )
        return context


class ScrutinCreateView(AdminRequisMixin, CreateView):
    model = Scrutin
    template_name = "vote/creation_scrutin.html"
    form_class = ScrutinCreationForm

    def dispatch(self, request, *args, **kwargs):
        if Scrutin.objects.filter(ouvert=True).exists():
            messages.add_message(
                self.request,
                messages.ERROR,
                _("Vous ne pouvez ouvrir plus d'un scrutin à la fois!"),
            )
            return HttpResponseRedirect(reverse("vote:scrutins"))
        return super().dispatch(request, *args, **kwargs)

    def get_success_url(self):
        if self.object.type_scrutin in [
            Scrutin.MAJORITE_SYNDICATS_OPPOSITION,
            Scrutin.MAJORITE_SYNDICATS_PRIORISATION,
        ]:
            return reverse("vote:scrutin-propositions", kwargs={"pk": self.object.pk})
        if self.object.type_scrutin == Scrutin.DELEGUE:
            return reverse("vote:scrutin-delegue", kwargs={"pk": self.object.pk})
        return reverse("vote:scrutins")

    def form_valid(self, form):
        response = super().form_valid(form)
        self.object.createur = self.request.user
        self.object.save()
        if self.object.type_scrutin not in [
            Scrutin.MAJORITE_SYNDICATS_PRIORISATION,
            Scrutin.MAJORITE_SYNDICATS_OPPOSITION,
        ]:
            Log.objects.create(
                log=(
                    f"L'admin {self.request.user} a créé le scrutin {self.object} "
                    f"de type {self.object.type_scrutin_display}"
                )
            )
        return response


class ScrutinFermetureView(AdminRequisMixin, DeleteView):
    model = Scrutin
    template_name = "vote/fermeture_scrutin.html"

    def delete(self, request, *args, **kwargs):
        scrutin = self.get_object()
        scrutin.ouvert = False
        scrutin.save()
        Log.objects.create(
            log=f"L'admin {self.request.user} a fermé le scrutin {scrutin}"
        )
        if scrutin.type_scrutin not in [
            Scrutin.MAJORITE_SYNDICATS_OPPOSITION,
            Scrutin.MAJORITE_SYNDICATS_PRIORISATION,
        ]:
            scrutin.calcul_resultat()
            Log.objects.create(
                log=(
                    f"Le scrutin {scrutin} voit sa résolution "
                    f"{'adoptée' if scrutin.adoption else 'rejetée'}"
                )
            )
        else:
            Log.objects.create(
                log=(
                    f"Le scrutin {scrutin} obtient le "
                    f"résultat suivant : {scrutin.resultat}"
                )
            )
        return HttpResponseRedirect(self.get_success_url())

    def get_success_url(self):
        return reverse("vote:scrutins")


class ScrutinDetailView(SyndicatOuAdminRequisMixin, DetailView):
    model = Scrutin
    template_name = "vote/detail_scrutin.html"

    def get_context_data(self, **kwargs):
        context = super().get_context_data(**kwargs)
        if self.object.type_scrutin == Scrutin.MAJORITE_SYNDICATS:
            context["majorite_syndicats"] = True
        if self.object.type_scrutin == Scrutin.MAJORITE_SYNDICATS_OPPOSITION:
            context["votes"] = self.object.votes_opposition.all()
            context["majorite_syndicats"] = True
            context["opposition"] = True
        elif self.object.type_scrutin == Scrutin.MAJORITE_SYNDICATS_PRIORISATION:
            context["votes"] = self.object.votes_priorisation.all()
            context["majorite_syndicats"] = True
            context["priorisation"] = True
        else:
            context["votes"] = self.object.votes.all()
        return context


class PropositionCreateView(AdminRequisMixin, TemplateView):
    model = Proposition
    template_name = "vote/add_propositions.html"

    def get_success_url(self):
        return reverse("vote:scrutins")

    def post(self, *args, **kwargs):
        scrutin = get_object_or_404(Scrutin, pk=self.kwargs["pk"])
        propositions = self.request.POST.getlist("proposition")
        for proposition in propositions:
            Proposition.objects.create(text=proposition, scrutin=scrutin)
        scrutin.createur = self.request.user
        scrutin.save()
        Log.objects.create(
            log=(
                f"L'admin {self.request.user} a créé le scrutin {scrutin} "
                f"de type {scrutin.type_scrutin_display}"
            )
        )
        return HttpResponseRedirect(self.get_success_url())


def get_writer_scrutin_simple(self, scrutin, writer):
    votes = scrutin.votes.all()
    if scrutin.type_scrutin == Scrutin.MAJORITE_SYNDICATS:
        writer.writerow(
            [
                "Syndicat",
                "Vote du syndicat",
            ]
        )
        for vote in votes:
            writer.writerow(
                [
                    vote.utilisateur,
                    vote.vote_utilisateur,
                ]
            )
        writer.writerow([f"Résultat du scrutin : {scrutin.resultat}"])
    else:
        writer.writerow(
            [
                "Syndicat",
                "Vote du syndicat",
                "Mandats pour",
                "Mandats contre",
                "Mandats abstention",
                "Mandats NPPV",
            ]
        )
        for vote in votes:
            writer.writerow(
                [
                    vote.utilisateur,
                    vote.vote_utilisateur,
                    vote.mandats_pour,
                    vote.mandats_contre,
                    vote.mandats_abstention,
                    vote.mandats_nppv,
                ]
            )
        writer.writerow([f"Résolution {'adoptée' if scrutin.adoption else 'rejetée'}"])
    return writer


def get_writer_scrutin_opposition_priorisation(self, scrutin, writer):
    if scrutin.type_scrutin == Scrutin.MAJORITE_SYNDICATS_OPPOSITION:
        votes = scrutin.votes_opposition.all()
    else:
        votes = scrutin.votes_priorisation.all()
    writer.writerow(
        [
            "Syndicat",
            "Vote du syndicat",
        ]
    )
    for vote in votes:
        writer.writerow(
            [
                vote.utilisateur,
                vote.resultat,
            ]
        )
    writer.writerow([f"Résultat du scrutin : {scrutin.resultat}"])
    return writer


class ExportCSVResultatView(SyndicatOuAdminRequisMixin, View):
    def get(self, request, *args, **kwargs):
        scrutin = get_object_or_404(Scrutin, pk=kwargs.get("pk"))
        response = HttpResponse(
            content_type="text/csv",
            headers={
                "Content-Disposition": f"attachment; filename={scrutin.intitule}.csv"
            },
        )
        writer = csv.writer(response)
        if scrutin.type_scrutin in [
            scrutin.MAJORITE_SYNDICATS_OPPOSITION,
            Scrutin.MAJORITE_SYNDICATS_PRIORISATION,
        ]:
            writer = get_writer_scrutin_opposition_priorisation(self, scrutin, writer)
        else:
            writer = get_writer_scrutin_simple(self, scrutin, writer)
        writer = csv.writer(response)
        return response


class ExportAllCSVResultatView(SyndicatOuAdminRequisMixin, View):
    def get_fichiers_csv(self):
        csv_files = []
        scrutins = Scrutin.objects.all()
        for scrutin in scrutins:
            mem_file = io.StringIO()
            writer = csv.writer(mem_file)
            if scrutin.type_scrutin in [
                scrutin.MAJORITE_SYNDICATS_OPPOSITION,
                Scrutin.MAJORITE_SYNDICATS_PRIORISATION,
            ]:
                writer = get_writer_scrutin_opposition_priorisation(
                    self, scrutin, writer
                )
            else:
                writer = get_writer_scrutin_simple(self, scrutin, writer)
            mem_file.seek(0)
            csv_files.append(
                {
                    "scrutin_pk": scrutin.pk,
                    "scrutin_intitule": scrutin.intitule,
                    "csv_file": mem_file,
                }
            )
        return csv_files

    def get(self, request, *args, **kwargs):
        csv_datas = self.get_fichiers_csv()
        temp_file = io.BytesIO()
        with zipfile.ZipFile(temp_file, "w", zipfile.ZIP_DEFLATED) as temp_file_opened:
            # add csv files each scrutin
            for data in csv_datas:
                data["csv_file"].seek(0)
                temp_file_opened.writestr(
                    f"scrutin_{data['scrutin_intitule']}_{data['scrutin_pk']}.csv",
                    data["csv_file"].getvalue(),
                )
        temp_file.seek(0)
        # put them to streaming content response
        # within zip content_type
        response = StreamingHttpResponse(
            FileWrapper(temp_file),
            content_type="application/zip",
        )
        response["Content-Disposition"] = "attachment;filename=scrutins_csv.zip"
        return response
