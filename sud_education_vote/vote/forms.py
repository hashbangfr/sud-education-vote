from django import forms
from django.core.exceptions import ValidationError
from django.utils.translation import gettext_lazy as _

from sud_education_vote.utilisateur.models import Utilisateur

from .models import Proposition, Scrutin, Vote, VoteOpposition, VotePriorisation


class ScrutinCreationForm(forms.ModelForm):
    class Meta:
        model = Scrutin
        fields = [
            "intitule",
            "type_scrutin",
        ]


class PropositionCreationForm(forms.ModelForm):
    class Meta:
        model = Proposition
        fields = [
            "text",
        ]
        labels = {"text": _("Proposition")}


class VoteCreationForm(forms.ModelForm):
    def clean(self):
        cleaned_data = super().clean()
        utilisateur = cleaned_data.get("utilisateur")
        if utilisateur.type_utilisateur == Utilisateur.SYNDICAT:
            pour = cleaned_data.get("mandats_pour", 0)
            contre = cleaned_data.get("mandats_contre", 0)
            abst = cleaned_data.get("mandats_abstention", 0)
            nppv = cleaned_data.get("mandats_nppv", 0)
            somme_votes = pour + contre + abst + nppv
            if somme_votes < utilisateur.nombre_mandat:
                raise ValidationError(
                    _("Vous devez attribuer l'ensemble de vos mandats")
                )
            if somme_votes > utilisateur.nombre_mandat:
                raise ValidationError(
                    _("Vous ne pouvez attribuer plus de mandats que vous n'en possedez")
                )
        scrutin = self.cleaned_data.get("scrutin")
        scrutin.refresh_from_db()
        if Vote.objects.filter(scrutin=scrutin, utilisateur=utilisateur).exists():
            self.add_error("scrutin", _("Vous avez déjà voté pour ce scrutin"))
        if not scrutin.ouvert:
            self.add_error(
                "scrutin",
                _("Ce scrutin est fermé, votre vote n'a pas été pris en compte"),
            )
        return cleaned_data

    class Meta:
        model = Vote
        fields = [
            "vote_utilisateur",
            "mandats_pour",
            "mandats_contre",
            "mandats_abstention",
            "mandats_nppv",
            "scrutin",
            "utilisateur",
        ]
        widgets = {
            "scrutin": forms.HiddenInput(),
            "utilisateur": forms.HiddenInput(),
        }
        labels = {"vote_utilisateur": _("Vote du syndicat")}


class VoteUpdateForm(forms.ModelForm):
    def clean(self):
        cleaned_data = super().clean()
        utilisateur = cleaned_data.get("utilisateur")
        if utilisateur.type_utilisateur == Utilisateur.SYNDICAT:
            pour = cleaned_data.get("mandats_pour", 0)
            contre = cleaned_data.get("mandats_contre", 0)
            abst = cleaned_data.get("mandats_abstention", 0)
            nppv = cleaned_data.get("mandats_nppv", 0)
            somme_votes = pour + contre + abst + nppv
            if somme_votes < utilisateur.nombre_mandat:
                raise ValidationError(
                    _("Vous devez attribuer l'ensemble de vos mandats")
                )
            if somme_votes > utilisateur.nombre_mandat:
                raise ValidationError(
                    _("Vous ne pouvez attribuer plus de mandats que vous n'en possedez")
                )
        return cleaned_data

    class Meta:
        model = Vote
        fields = [
            "vote_utilisateur",
            "mandats_pour",
            "mandats_contre",
            "mandats_abstention",
            "mandats_nppv",
            "scrutin",
            "utilisateur",
        ]
        widgets = {
            "scrutin": forms.HiddenInput(),
            "utilisateur": forms.HiddenInput(),
        }
        labels = {"vote_utilisateur": _("Vote du syndicat")}


class VoteDelegueCreationForm(forms.ModelForm):
    class Meta:
        model = Vote
        fields = [
            "mandats_pour",
            "mandats_contre",
            "mandats_abstention",
            "mandats_nppv",
            "scrutin",
            "utilisateur",
        ]
        widgets = {
            "scrutin": forms.HiddenInput(),
            "utilisateur": forms.HiddenInput(),
        }


class VoteMajoriteSyndicatsCreationForm(forms.ModelForm):
    def clean(self):
        cleaned_data = super().clean()
        utilisateur = cleaned_data.get("utilisateur")
        scrutin = self.cleaned_data.get("scrutin")
        scrutin.refresh_from_db()
        if Vote.objects.filter(scrutin=scrutin, utilisateur=utilisateur).exists():
            self.add_error("scrutin", _("Vous avez déjà voté pour ce scrutin"))
        return cleaned_data

    class Meta:
        model = Vote
        fields = [
            "vote_utilisateur",
            "scrutin",
            "utilisateur",
        ]
        widgets = {
            "scrutin": forms.HiddenInput(),
            "utilisateur": forms.HiddenInput(),
        }
        labels = {"vote_utilisateur": _("Vote du syndicat")}


class VoteMajoriteSyndicatsUpdateForm(forms.ModelForm):
    def clean(self):
        cleaned_data = super().clean()
        scrutin = self.cleaned_data.get("scrutin")
        scrutin.refresh_from_db()
        return cleaned_data

    class Meta:
        model = Vote
        fields = [
            "vote_utilisateur",
            "scrutin",
            "utilisateur",
        ]
        widgets = {
            "scrutin": forms.HiddenInput(),
            "utilisateur": forms.HiddenInput(),
        }
        labels = {"vote_utilisateur": _("Vote du syndicat")}


class VoteMajoriteSyndicatsOppositionCreationForm(forms.ModelForm):
    def __init__(self, *args, **kwargs):
        super(VoteMajoriteSyndicatsOppositionCreationForm, self).__init__(
            *args, **kwargs
        )
        self.fields["selected_proposition"].required = False

    def clean(self):
        cleaned_data = super().clean()
        utilisateur = cleaned_data.get("utilisateur")
        scrutin = self.cleaned_data.get("scrutin")
        scrutin.refresh_from_db()
        if VoteOpposition.objects.filter(
            scrutin=scrutin, utilisateur=utilisateur
        ).exists():
            self.add_error("scrutin", _("Vous avez déjà voté pour ce scrutin"))
        if not scrutin.ouvert:
            self.add_error(
                "scrutin",
                _("Ce scrutin est fermé, votre vote n'a pas été pris en compte"),
            )
        return cleaned_data

    class Meta:
        model = VoteOpposition
        fields = [
            "scrutin",
            "utilisateur",
            "abstention",
            "nppv",
            "selected_proposition",
        ]
        widgets = {
            "scrutin": forms.HiddenInput(),
            "utilisateur": forms.HiddenInput(),
        }
        labels = {
            "abstention": "Abstention",
            "nppv": "Ne prend pas part au vote",
            "selected_proposition": "Proposition choisie",
        }


class VoteMajoriteSyndicatsPriorisationCreationForm(forms.ModelForm):
    def __init__(self, *args, **kwargs):
        super(VoteMajoriteSyndicatsPriorisationCreationForm, self).__init__(
            *args, **kwargs
        )

    def clean(self):
        cleaned_data = super().clean()
        utilisateur = cleaned_data.get("utilisateur")
        scrutin = self.cleaned_data.get("scrutin")
        scrutin.refresh_from_db()
        if VotePriorisation.objects.filter(
            scrutin=scrutin, utilisateur=utilisateur
        ).exists():
            self.add_error("scrutin", _("Vous avez déjà voté pour ce scrutin"))
        if not scrutin.ouvert:
            self.add_error(
                "scrutin",
                _("Ce scrutin est fermé, votre vote n'a pas été pris en compte"),
            )
        return cleaned_data

    class Meta:
        model = VotePriorisation
        fields = ["scrutin", "utilisateur", "abstention", "nppv"]
        widgets = {
            "scrutin": forms.HiddenInput(),
            "utilisateur": forms.HiddenInput(),
        }
        labels = {
            "abstention": "Abstention",
            "nppv": "Ne prend pas part au vote",
        }


class VoteMajoriteSyndicatsOppositionUpdateForm(forms.ModelForm):
    def __init__(self, *args, **kwargs):
        super(VoteMajoriteSyndicatsOppositionUpdateForm, self).__init__(*args, **kwargs)
        self.fields["selected_proposition"].required = False

    def clean(self):
        cleaned_data = super().clean()
        scrutin = self.cleaned_data.get("scrutin")
        scrutin.refresh_from_db()
        return cleaned_data

    class Meta:
        model = VoteOpposition
        fields = [
            "scrutin",
            "utilisateur",
            "abstention",
            "nppv",
            "selected_proposition",
        ]
        widgets = {
            "scrutin": forms.HiddenInput(),
            "utilisateur": forms.HiddenInput(),
        }
        labels = {
            "abstention": "Abstention",
            "nppv": "Ne prend pas part au vote",
            "selected_proposition": "Proposition choisie",
        }


class VoteMajoriteSyndicatsPriorisationUpdateForm(forms.ModelForm):
    def __init__(self, *args, **kwargs):
        super(VoteMajoriteSyndicatsPriorisationUpdateForm, self).__init__(
            *args, **kwargs
        )

    def clean(self):
        cleaned_data = super().clean()
        scrutin = self.cleaned_data.get("scrutin")
        scrutin.refresh_from_db()
        return cleaned_data

    class Meta:
        model = VotePriorisation
        fields = ["scrutin", "utilisateur", "abstention", "nppv"]
        widgets = {
            "scrutin": forms.HiddenInput(),
            "utilisateur": forms.HiddenInput(),
        }
        labels = {
            "abstention": "Abstention",
            "nppv": "Ne prend pas part au vote",
        }


class AdminCreateVoteForm(forms.Form):
    syndicat = forms.ModelChoiceField(
        queryset=Utilisateur.objects.filter(type_utilisateur=Utilisateur.SYNDICAT)
    )


class ResetForm(forms.Form):
    archive_souhaitee = forms.BooleanField(
        initial=True,
        label=(
            "Je souhaite qu'une archive des scrutins"
            " et des votes associés soit créée."
        ),
        required=False,
    )
