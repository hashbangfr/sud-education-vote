from factory import Faker, Sequence, SubFactory
from factory.django import DjangoModelFactory
from factory.fuzzy import FuzzyChoice

from sud_education_vote.utilisateur.factories import UtilisateurFactory

from .models import (
    Proposition,
    Scrutin,
    Vote,
    VoteOpposition,
    VotePriorisation,
    VoteProposition,
)


class ScrutinFactory(DjangoModelFactory):
    createur = SubFactory(UtilisateurFactory)
    intitule = Sequence(lambda n: "intitule_{0}".format(n))
    type_scrutin = FuzzyChoice([x[0] for x in Scrutin.TYPES])

    class Meta:
        model = Scrutin


class VoteFactory(DjangoModelFactory):
    scrutin = SubFactory(ScrutinFactory)
    utilisateur = SubFactory(UtilisateurFactory)
    vote_utilisateur = FuzzyChoice([x[0] for x in Vote.VOTES])

    class Meta:
        model = Vote


class PropositionFactory(DjangoModelFactory):
    scrutin = SubFactory(ScrutinFactory)
    text = Sequence(lambda n: "prop_{0}".format(n))

    class Meta:
        model = Proposition


class VoteOppositionFactory(DjangoModelFactory):
    scrutin = SubFactory(ScrutinFactory)
    utilisateur = SubFactory(UtilisateurFactory)
    selected_proposition = SubFactory(PropositionFactory)

    class Meta:
        model = VoteOpposition


class VotePriorisationFactory(DjangoModelFactory):
    scrutin = SubFactory(ScrutinFactory)
    utilisateur = SubFactory(UtilisateurFactory)

    class Meta:
        model = VotePriorisation


class VotePropositionFactory(DjangoModelFactory):
    vote = SubFactory(VotePriorisationFactory)
    proposition = SubFactory(PropositionFactory)
    score = Faker("pyint", min_value=0, max_value=3)

    class Meta:
        model = VoteProposition
