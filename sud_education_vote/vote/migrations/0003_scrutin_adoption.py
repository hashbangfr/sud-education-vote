# Generated by Django 3.2.10 on 2022-03-03 10:29

from django.db import migrations, models


class Migration(migrations.Migration):

    dependencies = [
        ('vote', '0002_scrutin_ouvert'),
    ]

    operations = [
        migrations.AddField(
            model_name='scrutin',
            name='adoption',
            field=models.BooleanField(default=False),
        ),
    ]
