from django.db import models
from django.db.models import Count, Sum
from django.utils.translation import gettext_lazy as _

from sud_education_vote.utilisateur.models import Utilisateur


class Scrutin(models.Model):
    """
    Scrutin pouvant être créé par un admin.
    - Contient un intitulé et un type
    - Si le type est Majorité ou majorité a deux tiers, les syndicats
    votes et attribuent leurs mandats sur l'application
    - Si le type est Délégué, les délégués votent a main levé et un
    admin rentre le resultat dans l'application, sur la page du scrutin
    - Possede une clef vers l'admin l'ayant ouvert
    """

    MAJORITE = "MAJORITE"
    MAJORITE_DEUX_TIERS = "MAJORITE_DEUX_TIERS"
    DELEGUE = "DELEGUE"
    MAJORITE_SYNDICATS = "MAJORITE_SYNDICATS"
    MAJORITE_SYNDICATS_OPPOSITION = "MAJORITE_SYNDICATS_OPPOSITION"
    MAJORITE_SYNDICATS_PRIORISATION = "MAJORITE_SYNDICATS_PRIORISATION"

    TYPES = [
        (MAJORITE, _("Majorité des syndicats et mandats")),
        (MAJORITE_DEUX_TIERS, _("Majorité des 2/3 des syndicats et mandats")),
        (DELEGUE, _("Majorité simple des délégué⋅es")),
        (MAJORITE_SYNDICATS, _("Majorité simple des syndicats")),
        (MAJORITE_SYNDICATS_OPPOSITION, _("Majorité des syndicats - Opposition")),
        (MAJORITE_SYNDICATS_PRIORISATION, _("Majorité des syndicats - Priorisation")),
    ]
    type_scrutin = models.CharField(
        choices=TYPES, max_length=max([len(e[0]) for e in TYPES])
    )
    intitule = models.CharField(max_length=255)

    createur = models.ForeignKey(
        Utilisateur,
        on_delete=models.SET_NULL,
        blank=True,
        null=True,
        related_name="scrutins",
    )

    ouvert = models.BooleanField(default=True)
    adoption = models.BooleanField(default=False)

    def __str__(self):
        return self.intitule

    @property
    def resultat(self):
        if self.ouvert:
            return "En cours"
        if self.type_scrutin in [
            Scrutin.MAJORITE_SYNDICATS_OPPOSITION,
            Scrutin.MAJORITE_SYNDICATS_PRIORISATION,
        ]:
            propositions = self.propositions.all()
            if self.type_scrutin == Scrutin.MAJORITE_SYNDICATS_OPPOSITION:
                if self.votes_opposition.count() == 0:
                    return _("Pas de votes")
                propositions = propositions.annotate(
                    sum_votes=Count("votes_opposition")
                ).order_by("-sum_votes", "text")
            elif self.type_scrutin == Scrutin.MAJORITE_SYNDICATS_PRIORISATION:
                if self.votes_priorisation.count() == 0:
                    return _("Pas de votes")
                propositions = propositions.annotate(
                    sum_votes=Sum("votes_propositions__score")
                ).order_by("-sum_votes", "text")
            winning_propositions = []
            max_votes = propositions[0].sum_votes
            for proposition in propositions:
                if proposition.sum_votes == max_votes:
                    winning_propositions.append(proposition.text)
            if len(winning_propositions) >= 2:
                return "Egalité entre plusieurs propositions : " + ", ".join(
                    winning_propositions
                )
            return str(winning_propositions[0])
        else:
            if self.votes.count() == 0:
                return _("Pas de votes")
            elif self.adoption:
                return _("Adopté")
            else:
                return _("Rejeté")

    @property
    def resultat_syndicats(self):
        if self.ouvert:
            return "En cours"
        pour = 0
        contre = 0
        abstention = 0
        nppv = 0
        for vote in self.votes.all():
            if vote.vote_utilisateur == Vote.POUR:
                pour = pour + 1
            elif vote.vote_utilisateur == Vote.CONTRE:
                contre = contre + 1
            elif vote.vote_utilisateur == Vote.ABSTENTION:
                abstention = abstention + 1
            elif vote.vote_utilisateur == Vote.NPPV:
                nppv = nppv + 1
        resultats = f"P: {pour}\n" f"C: {contre}\n" f"A: {abstention}\n" f"N: {nppv}"
        return resultats

    @property
    def resultat_syndicats_opposition(self):
        if self.ouvert:
            return "En cours"
        resultats = ""
        propositions = (
            self.propositions.all()
            .annotate(count_votes=Count("votes_opposition"))
            .order_by("-count_votes")
        )
        for proposition in propositions:
            resultats += f"{proposition.text} : {proposition.count_votes or 0}\n"
        nb_abstention = VoteOpposition.objects.filter(
            scrutin=self, abstention=True
        ).count()
        nb_nppv = VoteOpposition.objects.filter(scrutin=self, nppv=True).count()
        resultats += f"Abstention : {nb_abstention}\n"
        resultats += f"Nppv : {nb_nppv}\n"
        return resultats

    @property
    def resultat_syndicats_priorisation(self):
        if self.ouvert:
            return "En cours"
        resultats = ""
        propositions = (
            self.propositions.all()
            .annotate(sum_scores=Sum("votes_propositions__score"))
            .order_by("-sum_scores")
        )
        for proposition in propositions:
            resultats += f"{proposition.text} : {proposition.sum_scores or 0}\n"
        nb_abstention = VotePriorisation.objects.filter(
            scrutin=self, abstention=True
        ).count()
        nb_nppv = VotePriorisation.objects.filter(scrutin=self, nppv=True).count()
        resultats += f"Abstention : {nb_abstention}\n"
        resultats += f"Nppv : {nb_nppv}\n"
        return resultats

    @property
    def resultat_mandats(self):
        if self.ouvert:
            return "En cours"
        pour = 0
        contre = 0
        abstention = 0
        nppv = 0
        for vote in self.votes.all():
            pour = pour + vote.mandats_pour
            contre = contre + vote.mandats_contre
            abstention = abstention + vote.mandats_abstention
            nppv = nppv + vote.mandats_nppv
        resultats = f"P: {pour}\n" f"C: {contre}\n" f"A: {abstention}\n" f"N: {nppv}"
        return resultats

    @property
    def type_scrutin_display(self):
        if self.type_scrutin == self.MAJORITE:
            return _("Majorité des syndicats et mandats")
        elif self.type_scrutin == self.MAJORITE_DEUX_TIERS:
            return _("Majorité des 2/3 des syndicats et mandats")
        elif self.type_scrutin == self.MAJORITE_SYNDICATS:
            return _("Majorité simple des syndicats")
        elif self.type_scrutin == self.MAJORITE_SYNDICATS_OPPOSITION:
            return _("Majorité des syndicats - Opposition")
        elif self.type_scrutin == self.MAJORITE_SYNDICATS_PRIORISATION:
            return _("Majorité des syndicats - Priorisation")
        else:
            return _("Majorité simple des délégué⋅es")

    """
        Cette méthode est utilisée uniquement pour les votes simples
        visant l'adoption ou non d'une mesure.
        Cela exclut les scrutins de type
        "Majorité des syndicats - Opposition"
        et "Majorité des syndicats - Priorisation"
    """

    def calcul_resultat(self):
        votes = self.votes.all()
        syndicat_pour = votes.filter(vote_utilisateur="POUR").count()
        syndicat_contre = votes.filter(vote_utilisateur="CONTRE").count()
        syndicat_abst = votes.filter(vote_utilisateur="ABSTENTION").count()
        mandats_pour = sum([vote.mandats_pour for vote in votes])
        mandats_contre = sum([vote.mandats_contre for vote in votes])
        mandats_abstention = sum([vote.mandats_abstention for vote in votes])

        adoption_syndicats = False
        adoption_mandats = False

        if votes.count() > 0:
            if self.type_scrutin == Scrutin.MAJORITE:
                if syndicat_pour > syndicat_contre + syndicat_abst:
                    adoption_syndicats = True
                if mandats_pour > mandats_contre + mandats_abstention:
                    adoption_mandats = True
            elif self.type_scrutin == Scrutin.MAJORITE_DEUX_TIERS:
                if (
                    syndicat_pour
                    >= (syndicat_pour + syndicat_contre + syndicat_abst) * 2 / 3
                ):
                    adoption_syndicats = True
                if (
                    mandats_pour
                    >= (mandats_pour + mandats_contre + mandats_abstention) * 2 / 3
                ):
                    adoption_mandats = True
            elif self.type_scrutin == Scrutin.MAJORITE_SYNDICATS:
                if syndicat_pour > syndicat_contre + syndicat_abst:
                    adoption_syndicats = True
                    adoption_mandats = True
            else:
                if mandats_pour > mandats_contre + mandats_abstention:
                    adoption_syndicats = True
                    adoption_mandats = True

        self.adoption = adoption_syndicats and adoption_mandats
        self.save()

    class Meta:
        verbose_name = _("Scrutin")
        verbose_name_plural = _("Scrutins")


class Proposition(models.Model):
    """
    Proposition  pour les scrutins à majorité par syndicats uniquement.
    Le type d'adoption est lié au type de scrutin choisi :
    - Type MAJORITE_SYNDICALES_OPPOSITION :
    l'utilisateur choisit une unique proposition parmi celles proposées
    - Type MAJORITE_SYNDICALES_PRIORISATION:
    l'utilisateur classe les propositions
    """

    text = models.CharField(max_length=255)
    scrutin = models.ForeignKey(
        Scrutin, on_delete=models.CASCADE, related_name="propositions"
    )

    def __str__(self):
        return self.text


class Vote(models.Model):
    """
    Vote pouvant être créé par un admin, ou un syndicat.
    - Lié à un scrutin, et à un utilisateur
    - Si l'utilisateur est un syndicat, il répartit ses mandats entre Pour,
    Contre, Abstention, NPPV.
    - Si L'utilisateur est un admin (dans le cas d'un scrutin à majorité
    des delegués), il répartit les votes des délégués présents comme pour un syndicat
    - Le syndicat peut également donner le vote du syndicat (vote_utilisateur)
    - En cas de vote delegué, le vote_utilisateur sera remplie
    automatiquement en fonction de la répartition des votes délégués
    """

    scrutin = models.ForeignKey(
        Scrutin,
        on_delete=models.CASCADE,
        related_name="votes",
    )
    utilisateur = models.ForeignKey(
        Utilisateur,
        on_delete=models.CASCADE,
        related_name="votes",
    )

    POUR = "POUR"
    CONTRE = "CONTRE"
    ABSTENTION = "ABSTENTION"
    NPPV = "NPPV"
    VOTES = [
        (POUR, _("Pour")),
        (CONTRE, _("Contre")),
        (ABSTENTION, _("Abstention")),
        (NPPV, _("Ne prend pas part au vote")),
    ]
    vote_utilisateur = models.CharField(
        choices=VOTES, max_length=max([len(e[0]) for e in VOTES])
    )

    mandats_pour = models.PositiveIntegerField(default=0)
    mandats_contre = models.PositiveIntegerField(default=0)
    mandats_abstention = models.PositiveIntegerField(default=0)
    mandats_nppv = models.PositiveIntegerField(default=0)


class VoteOpposition(models.Model):
    """
    Vote pouvant être créé par un syndicat
    pour les scrutins de type Majorité de syndicats - Opposition
    - Lié à un scrutin et à un utilisateur
    - L'utilisateur vote pour une seule proposition,
    s'abstient ou ne prend pas part au vote
    """

    scrutin = models.ForeignKey(
        Scrutin,
        on_delete=models.CASCADE,
        related_name="votes_opposition",
    )
    utilisateur = models.ForeignKey(
        Utilisateur,
        on_delete=models.CASCADE,
        related_name="votes_opposition",
    )
    abstention = models.BooleanField(default=False)
    nppv = models.BooleanField(default=False)
    selected_proposition = models.ForeignKey(
        Proposition,
        on_delete=models.CASCADE,
        related_name="votes_opposition",
        null=True,
        blank=True,
    )

    @property
    def resultat(self):
        if self.abstention:
            return "Abstention"
        elif self.nppv:
            return "Ne prend pas part au vote"
        else:
            return f"{self.selected_proposition}"


class VotePriorisation(models.Model):
    """
    Vote pouvant être créé par un syndicat
    pour les scrutins de type Majorité de syndicats - Priorisation
    - Lié à un scrutin et à un utilisateur
    - L'utilisateur priorise les propositions du scrutin en leur attribuant un score
    (création d'instances du modèle VoteProposition).
    Il peut aussi s'abstenir ou ne pas prendre par au vote.
    """

    scrutin = models.ForeignKey(
        Scrutin,
        on_delete=models.CASCADE,
        related_name="votes_priorisation",
    )
    utilisateur = models.ForeignKey(
        Utilisateur,
        on_delete=models.CASCADE,
        related_name="votes_priorisation",
    )
    abstention = models.BooleanField(default=False)
    nppv = models.BooleanField(default=False)

    @property
    def resultat(self):
        if self.abstention:
            return "Abstention"
        elif self.nppv:
            return "Ne prend pas part au vote"
        else:
            resultats = ""
            votes_propositions = self.votes_propositions.all().order_by("-score")
            for index, vote in enumerate(votes_propositions):
                resultats += f"{vote.proposition.text}"
                if index != len(votes_propositions) - 1:
                    resultats += ", "
            return resultats


class VoteProposition(models.Model):
    """
    Lie un vote et une proposition pour les scrutins
    de type Majorité de syndicats - Priorisation.
    """

    vote = models.ForeignKey(
        VotePriorisation, on_delete=models.CASCADE, related_name="votes_propositions"
    )
    proposition = models.ForeignKey(
        Proposition, on_delete=models.CASCADE, related_name="votes_propositions"
    )
    score = models.PositiveIntegerField(default=0)
