import pytest
from django.urls import reverse

from sud_education_vote.utilisateur.factories import UTILISATEUR_PASSWORD

pytestmark = pytest.mark.django_db


def test_redirection_connexion_si_superadmin(client, utilisateur_superadmin):
    response = client.post(
        reverse("connexion"),
        {"username": utilisateur_superadmin.email, "password": UTILISATEUR_PASSWORD},
    )
    assert response.status_code == 302
    assert response.url == reverse("utilisateur:admins")


def test_redirection_connexion_si_admin(client, utilisateur_admin):
    response = client.post(
        reverse("connexion"),
        {"username": utilisateur_admin.email, "password": UTILISATEUR_PASSWORD},
    )
    assert response.status_code == 302
    assert response.url == reverse("accueil")


def test_redirection_connexion_si_syndicat(client, utilisateur_syndicat):
    response = client.post(
        reverse("connexion"),
        {"username": utilisateur_syndicat.email, "password": UTILISATEUR_PASSWORD},
    )
    assert response.status_code == 302
    assert response.url == reverse("vote:page-vote")
