import pytest
from django.urls import reverse

pytestmark = pytest.mark.django_db


def test_redirection_si_non_connecte(client):
    response = client.get(reverse("accueil"))
    assert response.status_code == 302
    assert response.url == f"{reverse('connexion')}?next={reverse('accueil')}"


def test_200_si_connecte(client_log):
    response = client_log.get(reverse("accueil"))
    assert response.status_code == 200
