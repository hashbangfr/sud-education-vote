import pytest
from django.urls import reverse

pytestmark = pytest.mark.django_db


def test_export_txt_journal_redirection_si_non_connecte(client, log):
    url_scrutin = reverse("log:journal-export")
    response = client.get(url_scrutin)
    assert response.status_code == 302
    assert response.url == f"{reverse('connexion')}?next={url_scrutin}"


def test_export_txt_journal_ok_superadmin(client_superadmin, log):
    response = client_superadmin.get(reverse("log:journal-export"))
    assert response.status_code == 200


def test_export_txt_journal_ok_syndicat(client_syndicat, log):
    response = client_syndicat.get(reverse("log:journal-export"))
    assert response.status_code == 200


def test_export_txt_journal_ok_admin(client_admin, log):
    response = client_admin.get(reverse("log:journal-export"))
    assert response.status_code == 200
