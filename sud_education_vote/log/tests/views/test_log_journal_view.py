import pytest
from django.urls import reverse
from pytest_django.asserts import assertQuerysetEqual

pytestmark = pytest.mark.django_db


def test_journal_log_redirection_si_non_connecte(client):
    url_journal = reverse("log:journal")
    response = client.get(url_journal)
    assert response.status_code == 302
    assert response.url == f"{reverse('connexion')}?next={url_journal}"


def test_journal_log_ok_pour_syndicat(client_syndicat):
    response = client_syndicat.get(reverse("log:journal"))
    assert response.status_code == 200


def test_journal_log_ok_pour_admin(client_admin):
    response = client_admin.get(reverse("log:journal"))
    assert response.status_code == 200


def test_journal_log_ok_pour_superadmin(client_superadmin):
    response = client_superadmin.get(reverse("log:journal"))
    assert response.status_code == 200


def test_journal_log_ok(client_syndicat, log_factory):
    log_1 = log_factory()
    log_2 = log_factory()
    log_3 = log_factory()
    response = client_syndicat.get(reverse("log:journal"))
    assert response.status_code == 200
    assertQuerysetEqual(
        response.context.get("object_list"),
        [log_3, log_2, log_1],
    )


def test_journal_log_ok_pas_de_log(client_syndicat, utilisateur_syndicat):
    response = client_syndicat.get(reverse("log:journal"))
    assert response.status_code == 200
    assert not response.context.get("object_list")


def test_journal_log_not_ok_pour_syndicat_scrutin_ouvert(
    scrutin_factory, client_syndicat
):
    scrutin_factory(ouvert=True)
    response = client_syndicat.get(reverse("log:journal"))
    assert response.status_code == 302
    assert response.url == reverse("vote:scrutins")
