from django.db import models


class Log(models.Model):
    """
    Modele log : Chaque fois qu'un utilisateur fera une action,
    un log sera créé pour garder une trace
    """

    log = models.CharField(max_length=255)
    date_log = models.DateTimeField(auto_now_add=True)
