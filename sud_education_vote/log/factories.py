from factory import Sequence
from factory.django import DjangoModelFactory

from .models import Log


class LogFactory(DjangoModelFactory):
    log = Sequence(lambda n: "log_{0}".format(n))

    class Meta:
        model = Log
