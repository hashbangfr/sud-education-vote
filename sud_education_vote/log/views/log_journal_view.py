from django.contrib import messages
from django.contrib.auth.mixins import LoginRequiredMixin
from django.http import HttpResponse, HttpResponseRedirect
from django.urls import reverse
from django.views.generic import ListView, View

from sud_education_vote.vote.models import Scrutin

from ..models import Log


class JournalLogListView(LoginRequiredMixin, ListView):
    template_name = "log/journal_logs.html"
    model = Log

    def dispatch(self, request, *args, **kwargs):
        scrutin_en_cours = Scrutin.objects.filter(ouvert=True).exists()
        if scrutin_en_cours:
            if not self.request.user.is_admin:
                messages.add_message(
                    self.request,
                    messages.ERROR,
                    (
                        "Un scrutin est en cours, vous ne pouvez"
                        " pas consulter les logs !"
                    ),
                )
                return HttpResponseRedirect(reverse("vote:scrutins"))
        return super().dispatch(request, *args, **kwargs)

    # On affiche les 200 derniers Logs
    def get_queryset(self):
        queryset = super().get_queryset()
        queryset = queryset.order_by("-date_log")[:200]
        return queryset


class ExportTxtJournalView(LoginRequiredMixin, View):
    def get(self, request, *args, **kwargs):
        content = ""
        for log in Log.objects.all():
            date_str = log.date_log.strftime("%m/%d/%Y %H:%M:%S")
            content = content + f"{date_str} {log.log}\n"
        response = HttpResponse(
            content=content,
            content_type="text/plain",
            headers={"Content-Disposition": "attachment; filename=journal.txt"},
        )
        return response
