from django.urls import path

from .views import ExportTxtJournalView, JournalLogListView

app_name = "log"

urlpatterns = [
    path("journal/", JournalLogListView.as_view(), name="journal"),
    path("journal/export/", ExportTxtJournalView.as_view(), name="journal-export"),
]
